

## Instruções 

1. composer install
2. credenciais do banco no arquivo .env
3. php artisan key:generate
4. php artisan passport:install ( rodar em produção somente uma vez)
5. php artisan serve
6. npm run watch 

## Importante
o projeto está dividido em 2 partes, o admin com o front feito em laravel e o site feito em vuejs


--------> RedirectIfAuthenticated


## Log de atividades
deverá ser utilizado em todos os metodos onde o usuário tiver alguma iteracao com o banco de dados
```php
    use App\Traits\LogatividadeTrait;

	class LocalRepository
	{
    	use LogatividadeTrait;

    	public function saveLocal($request)
    	{
            $atividade = Atividade::create($request);
        	$this->LogSalvarAtividade(
				'users_id' // id do usuario, ex: auth('api')->user()->id
            	'tabela'        // nome da tabela onde teve alguma atividade, ex: Local::class
            	'tabela_id'   // primary key da tabela onde teve alguma atividade
            	'atividade'   // mensagem do que aconteceu, ex: salvou um local 
			);

		um exemplo real:
		$this->LogSalvarAtividade(auth('api')->user()->id, User::class, $atividade->id,  "Inativou o usuário $user->name");

    }
```
## protegendo informacoes

Para proteger as informacoes do sistema, podemos utilizar alguns filtros:

| # | Onde usar | Como usar |
| ------------- | ------------- | ------------- |
| Admin | HTML com Vuejs | v-if="$gate.isAdmin()"  |
| Admin | Laravel  | \Gate::allows('isAdmin')  |
| Cliente | HTML com Vuejs | v-if="$gate.isCliente()"  |
| Cliente | Laravel  | \Gate::allows('isCliente')  |

## Validacoes em formularios
##### No Back utilizar a funcao de validacao:
$request->validate

```php
public function save() {
    $request->validate([
        'tipo' => 'required',
        'lugares' => 'required',
        'espacoM2' => 'required',
        'cep' => 'required',
        'logradouro' => 'required',
        'numero' => 'required',
        'bairro' => 'required',
        'cidade' => 'required',
        'estado' => 'required',
        'preco_hora' => 'required|numeric',
        'preco_dia' => 'required|numeric',
        'preco_semana' => 'required|numeric',
        'preco_mes' => 'required|numeric',
    ]);
}
````

##### No front utilizar algumas tags do vuejs:
:class="{ 'is-invalid': locaisForm.errors.has('logradouro') }"
'<has-error :form="locaisForm" field="logradouro"></has-error>', EXEMPLO:

```html
<input
	type="text" 
	class="form-control" 
	:class="{ 'is-invalid': locaisForm.errors.has('logradouro') }"
	v-model="locaisForm.logradouro"
>
<has-error :form="locaisForm" field="logradouro"></has-error>
```

#"Loading"

basta apenas utilizar o seguinte comando no inicio do metodo:
    this.$store.commit('PRELOADER', true)
e no final do metodo utilizar o mesmo comando mudando de true para false, exemplo:

```javascript
    listLocalsImages() {
        this.$store.commit('PRELOADER', true)
        axios.get('/api/locals/imagesbyid/' + user.id).then(res  => {
            this.localsImages = res.data 
        });
        .finally(() => this.$store.commit('PRELOADER', false))
    },
```


Pasta vendor/ spatie/ laravel-activitylog/src/activityLogger.php

cole este codigo no metodo log

if(!$activity->causer_id && auth('api')->user()){
            $activity->causer_id = auth('api')->user()->id;
            $activity->causer_type = 'App\User';
}



