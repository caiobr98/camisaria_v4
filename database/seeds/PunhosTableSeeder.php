<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Punho;

class PunhosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');
    
        Punho::create([
            'tipo' => 'Botão Simples',
            'imagem' => '/img/punhos/punhos.png',
        ]);
        Punho::create([
            'tipo' => 'Botão Duplo',
            'imagem' => '/img/punhos/punhos.png',
        ]);
        Punho::create([
            'tipo' => 'Duplo',
            'imagem' => '/img/punhos/punhos.png',
        ]);
    }
}
