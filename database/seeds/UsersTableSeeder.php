<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Fernando Alves',
            'email'     => 'fer@fer.com',
            'email_verified_at' => '2019-09-09 00:00:00',
            'password'  => bcrypt('123123'),
        ]);
    }
}
