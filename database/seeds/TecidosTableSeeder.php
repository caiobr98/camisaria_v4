<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Tecido;

class TecidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');

        for ($i=0; $i < 20; $i++) { 
            Tecido::create([
                'nome' => $faker->firstName,
                'marca' => $i < 12 ? 'marca 1' : 'marca 2',
                'imagem' => $i < 12 ? '/img/produtos/tecidos/tecido-3.png' : '/img/produtos/tecidos/tecido-2.png',
                'descricao' => $faker->realText(180),
                'preco' => $faker->buildingNumber,
                'cor' => $i < 12 ? 'azul' : 'pastel' ,
                'textura' => '',
                'padrao' => $i < 12 ? 'liso' : 'padrao',
            ]);
        }

        for ($j=0; $j < 20; $j++) { 
            Tecido::create([
                'nome' => $faker->firstName,
                'marca' => $j < 12 ? 'marca 3' : 'marca 2',
                'imagem' => $j < 12 ? '/img/produtos/tecidos/tecido-1.png' : '/img/produtos/tecidos/tecido-2.png', 
                'descricao' => $faker->realText(180),
                'preco' => $faker->buildingNumber,
                'cor' => $j < 12 ? 'branco' : 'pastel' ,
                'textura' => '',
                'padrao' => $j < 12 ? 'estampa' : 'padrao',
            ]);
        }
    }
}
