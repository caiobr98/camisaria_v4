<?php

use Illuminate\Database\Seeder; 

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TecidosTableSeeder::class);        
        $this->call(UsersTableSeeder::class);        
        $this->call(ColarinhosTableSeeder::class);        
        $this->call(PunhosTableSeeder::class);        
    }
}
