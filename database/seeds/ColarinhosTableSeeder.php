<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Colarinho;

class ColarinhosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');
    
        Colarinho::create([
            'tipo' => 'Inglês',
            'imagem' => '/img/colarinhos/colarinho.png',
        ]);
        Colarinho::create([
            'tipo' => 'Francês',
            'imagem' => '/img/colarinhos/colarinho.png',
        ]);
        Colarinho::create([
            'tipo' => 'Italiano',
            'imagem' => '/img/colarinhos/colarinho.png',
        ]);
        Colarinho::create([
            'tipo' => 'Americano',
            'imagem' => '/img/colarinhos/colarinho.png',
        ]);
        Colarinho::create([
            'tipo' => 'Curto',
            'imagem' => '/img/colarinhos/colarinho.png',
        ]);
        
    }
}
