export default class Gate{

    constructor(user){
        this.user = user;
    }

    isCliente(){
        return this.user.type === 'cliente';
    }

    isAdmin(){
        return this.user.type === 'admin';
    }

    isAdminOrCliente(){
        if(this.user.type === 'admin' || this.user.type === 'cliente'){
            return true;
        }

    }

}