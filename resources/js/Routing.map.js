import SistemaLayoutAdminComponent from './templates/SistemaLayoutAdminComponent.vue'
import SistemaLayoutClienteComponent from './templates/SistemaLayoutClienteComponent.vue'
import SiteLayoutComponent from './templates/SiteLayoutComponent.vue'

export default [

    {
        path: '/',
        component: SiteLayoutComponent,
        children: [
            { path: '/', component: require('./pages/site/index/index.vue').default },
            { path: '/confeccao', component: require('./pages/cliente/confeccao/index.vue').default,  meta: {auth: false},
                children: [
                    { path: '', component: require('./pages/cliente/confeccao/components/SuasmedidasComponent.vue').default,meta: {auth: false}},
                    { path: '/colarinho', component: require('./pages/cliente/confeccao/components/ColarinhoComponent.vue').default, meta: {auth: false} },
                    { path: '/punho', component: require('./pages/cliente/confeccao/components/PunhoComponent.vue').default, meta: {auth: false} },
                    { path: '/detalhes', component: require('./pages/cliente/confeccao/components/DetalhesComponent.vue').default, meta: {auth: false} },
                    { path: '/fios', component: require('./pages/cliente/confeccao/components/FioComponent.vue').default ,meta: {auth: false}},
                    { path: '/tecidos', component: require('./pages/cliente/confeccao/components/TecidoComponent.vue').default, meta: {auth: false} },
                    { path: '/checklist', component: require('./pages/cliente/confeccao/components/ChecklistComponent.vue').default, meta: {auth: false} },
                ]
            }, 
            { path: '/quemsomos', component: require('./pages/site/index/quemsomos.vue').default },
            { path: '/comprasegura', component: require('./pages/site/index/comprasegura.vue').default },
            { path: '/comocomprar', component: require('./pages/site/index/comocomprar.vue').default },
            { path: '/entrega', component: require('./pages/site/index/entrega.vue').default },
            { path: '/formasdepagamento', component: require('./pages/site/index/formasdepagamento.vue').default },
            { path: '/politicas-de-privacidade', component: require('./pages/site/index/politicaprivacidade.vue').default },
            { path: '/termosdecompra', component: require('./pages/site/index/termosdecompra.vue').default },
            { path: '/trocas-devolucoes', component: require('./pages/site/index/trocasedevolucoes.vue').default },
            { path: '/faq', component: require('./pages/site/index/faq.vue').default},
            { path: '/contato', component: require('./pages/cliente/contato/index.vue').default },
            { path: '/central', component: require('./pages/site/index/central.vue').default },
            { path: '/visita', component: require('./pages/cliente/visita/index.vue').default},  
            { path: '/minhaconta', component: require('./pages/cliente/minhaconta/index.vue').default, meta: {auth: false}}, 
            /* pagamento */
            { path: '/checkout-pagamento', component: require('./pages/cliente/pagamento/index.vue').default, meta: {auth: false}}, 
            { path: '/checkout-compras', component: require('./pages/cliente/pagamento/compras.vue').default, meta: {auth: false}}, 
            { path: '/checkout-dadoscompra/:id', component: require('./pages/cliente/pagamento/dadoscompra.vue').default, meta: {auth: true}}, 
            /*fim pagamento*/
            
            /* meus dados*/
            
            { path: '/meusdados', component: require('./pages/cliente/minhaconta/meusdados/index.vue').default, meta: {auth: true}}, 
            
            { path: '/meusdados-minhasmedidas', component: require('./pages/cliente/minhaconta/minhasmedidas/index.vue').default, meta: {auth: true}}, 
            
            { path: '/meusdados-enderecos', component: require('./pages/cliente/minhaconta/meusenderecos/index.vue').default, meta: {auth: true}}, 
            { path: '/meusdados-enderecos/edit/:id', component: require('./pages/cliente/minhaconta/meusenderecos/edit.vue').default, meta: {auth: true}}, 
            /* fim meus dados*/
            { path: '/perfil', component: require('./pages/cliente/perfil/index.vue').default, meta: {auth: false}}, 
            { path: '/desejados', component: require('./pages/cliente/desejados/index.vue').default, }, 
            { path: '/medidas', component: require('./pages/cliente/medidas/index.vue').default, }, 
            { path: '/minhasmedidas', component: require('./pages/cliente/minhasmedidas/index.vue').default, meta: {auth: true}}, 
        ],
        props: (route) => ({ query: route.query.posicao })
    },

    // error 404
    { path: '*', component: require('./components/NotFound.vue').default }
    
]