import Vue from 'vue'
import Vuex from 'vuex'

import preloader from './modules/preloader/preloader'
import confeccao from './modules/confeccao/confeccao'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        preloader,
        confeccao
    },
})