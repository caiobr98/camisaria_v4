
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment'
import { Form, HasError, AlertError } from 'vform'
import Gate from "./Gate";
import Routing from './Routing.map'
import VueTheMask from 'vue-the-mask'
moment.locale('pt-BR');
import store from './vuex/store'

Vue.prototype.$gate = new Gate(window.user);

window.Form = Form
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.use(VueTheMask)
import swal from 'sweetalert2'
window.swal = swal;

const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.toast = toast;

import Router from 'vue-router'
Vue.use(Router)

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '3px'
})

let routes = Routing

const router = new Router({
    mode: 'history',
    routes,
    scrollBehavior (to, from, savedPosition) {
        if(user === null && to.meta.auth) {
            window.location.href = "/login";
        }

        return new Promise((resolve, reject) => {
            if (to.hash == '#confeccao'){
                resolve({ x: 0, y: 280 })
            }

            if (to.path == 'home_camisaria'){
                resolve({ x: 0, y: 280 })
            }
            
            resolve({ x: 0, y: 0 })

        })
    }
})

Vue.filter('upText', function(text){
    return text.toUpperCase()
})

Vue.filter('myDate', function(created){
    return moment(created).format('DD/MM/YYYY')
})

Vue.filter('hoursMoment', function(created){
    return moment(created).fromNow()
})

Vue.filter('currency', function (value) { // filtro moeda Real BR
    return 'R$ ' + parseFloat(value).toFixed(2);
});

window.Fire = new Vue();


Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
    'not-found',
    require('./components/NotFound.vue').default
);

Vue.component(
    'pagination',
    require('laravel-vue-pagination')
);

Vue.component('preloader-component', require('./components/PreloaderComponent.vue').default)

const app = new Vue({
    el: '#app',
    router,
    store,
    data: {
        search: ''
    },

    methods: {
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        },1000)
    },
});

