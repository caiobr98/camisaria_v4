@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Novo Cupom</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('cupons-listar')}}">Cupons</a></li>
                        <li class="breadcrumb-item active">Novo Cupom</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('cupons-cadastrar')}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">

                                @include('components.input', [
                                    'name' => 'codigo_cupom',
                                    'id' => 'txtCodigoCupon',
                                    'label' => 'Código Cupom',
                                    'autocomplete' => 'off'
                                ])

                                <label for="frete">Frete grátis</label>
                                <select name="frete" class="form-control @error('frete') is-invalid @enderror" id="txtFrete">
                                    <option value="0" selected>Não</option>
                                    <option value="1">Sim</option>
                                </select>
                                <br>
                                
                                @include('components.input', [
                                    'name' => 'desconto_cupom',
                                    'id' => 'txtDescontoCupom',
                                    'label' => 'Desconto do Cupom',
                                    'info' => 'É aconselhavé valores entre 5% e 30% para percentual',
                                    'type' => 'number',
                                    'maxlenght' => 3,
                                    'autocomplete' => 'off'
                                ])

                                <select name="tipo" class="form-control @error('tipo') is-invalid @enderror" id="txtTipo">
                                    <option value="0" selected>Percentual</option>
                                    <option value="1">Fixo</option>
                                </select>
                                
                                <br>
                                
                                <label for="frete">Quantidade de uso</label>
                                <input type="number" name="qtd" id="txtQtd" class="form-control @error('qtd') is-invalid @enderror">
                                <br>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('cupons-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script>
$(function(){

    $('#txtDescontoCupom').keyup(function(){
        var qtdDigitos = $('#txtDescontoCupom').val()
        var tipo = $('#txtTipo').val()

        if(qtdDigitos.length > 2 && tipo == 0) {
            $('#txtDescontoCupom').val('')
        }

        if((qtdDigitos >= 100 || qtdDigitos == 0) && tipo == 0){
            $('#txtDescontoCupom').val('')
        }
    })
});

</script>
@endsection