@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalhar Cupom</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('cupons-listar')}}">Cupons</a></li>
                        <li class="breadcrumb-item active">Detalhar Cupom</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('cupons-frm-editar', $cupom->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">

                                @include('components.input', [
                                    'name' => 'codigo_cupom',
                                    'id' => 'txtCodigoCupon',
                                    'label' => 'Código Cupom',
                                    'autocomplete' => 'off',
                                    'value' => $cupom->codigo_cupom,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'frete',
                                    'id' => 'txtFrete',
                                    'label' => 'Frete grátis',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'value' => ($cupom->frete == 1) ? 'Sim' : 'Não',
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'desconto_cupom',
                                    'id' => 'txtDescontoCupom',
                                    'label' => 'Desconto do Cupom',
                                    'type' => 'text',
                                    'maxlenght' => 3,
                                    'autocomplete' => 'off',
                                    'value' => ($cupom->tipo == 1) ? 'R$ ' . number_format($cupom->desconto_cupom, 2, ",", ".") : ($cupom->desconto_cupom . '%'),
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'tipo',
                                    'id' => 'txtTipo',
                                    'label' => 'Tipo de Desconto',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'value' => ($cupom->tipo == 1) ? 'Fixo' : 'Percentual',
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'qtd',
                                    'id' => 'txtQtd',
                                    'label' => 'Quantidade',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'value' => $cupom->qtd,
                                    'readonly' => true
                                ])

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('cupons-listar')}}" class="btn btn-default">Voltar</a>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection