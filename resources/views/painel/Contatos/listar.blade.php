@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="box">
    <div class="box-header">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Mensagens</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline mb-2">
                        <div class="card-body table-responsive p-0">
                            <table id="lista" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Sobrenome</th>
                                        <th>E-mail</th>
                                        <th>Telefone</th>
                                        <th>Celular</th>
                                        <th>Assunto</th>
                                        <th>Data</th>
                                        <th>Visita</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($contato as $contato)
                                        <tr>
                                            <td>{{ $contato->nome }}</td>
                                            <td>{{ $contato->sobrenome }}</td>
                                            <td>{{ $contato->email }}</td>
                                            <td>{{ $contato->telefone }}</td>
                                            <td>{{ $contato->celular }}</td>
                                            <td>{{ $contato->assunto }}</td>
                                            <td>{{ date('d/m/Y', strtotime($contato->created_at)) }}</td>
                                            <td>
                                                @if($contato->visita <> NULL)
                                                    <i class="fa fa-check color-green" style="color:green; font-size:40px;"></i> 
                                                @else 

                                                @endif
                                            </td>
                                            <td>
                                            <a href="{{ route('contato-visualizar', $contato->id) }}" class="btn btn-warning btn-sm">Visualizar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- /.card-body -->
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@stop
        
@section('javascript')

    <script>   

        $(document).ready(function() {
            $('#lista').DataTable({
               "processing": true,
               "responsive": true,
               "colReorder": true,
               "searching": true,
               "lengthMenu": [25, 50, 100],
               "deferRender": true,
               "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar: ",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    },
                    "select": {
                        "rows": {
                            "_": "Selecionado %d linhas",
                            "0": "Nenhuma linha selecionada",
                            "1": "Selecionado 1 linha"
                        }
                    }
                }
            });
        
        });

        $('.btnDeletar').click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Você tem certeza?',
                text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim!'
            }).then((result) => {
                if (result.value) {
                    window.location = $(this).attr('href');
                }
            })
        });
    </script>
@stop