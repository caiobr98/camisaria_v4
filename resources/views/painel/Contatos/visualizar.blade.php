@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="box">
    <div class="box-header">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Mensagens</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class="card card-primary card-outline mb-2">
                        <div class="card-body table-responsive p-0 mt-3 ml-3 mr-3 mb-3">
                                <label for="Mensagem" class="ml-3">Nome</label>
                                <input type="text" class="form-control col-lg-9" value="{{ $contato->nome }}" disabled>
                            
                                <label for="Mensagem" class="ml-3">Sobrenome</label>
                                <input type="text" class="form-control col-lg-9" value="{{ $contato->sobrenome }}" disabled>

                                <label for="Mensagem" class="ml-3">E-mail</label>
                                <input type="text" class="form-control col-lg-9" value="{{ $contato->email }}" disabled>

                                <label for="Mensagem" class="ml-3">Telefone</label>
                                <input type="text" class="form-control col-lg-9" value="{{ $contato->telefone }}" disabled>


                                <label for="Mensagem" class="ml-3">Celular</label>
                                <input type="text" class="form-control col-lg-9" value="{{ $contato->celular }}" disabled>


                                <label for="Mensagem" class="ml-3">Assunto</label>
                                <input type="text" class="form-control col-lg-9" value="{{ $contato->assunto }}" disabled>

                                <label for="Mensagem" class="ml-3">Mensagem</label>
                                <textarea class="form-control" style="width: 650px; height: 150px;" name="mensagem" id="mensagem" cols="1" rows="10" disabled>{{ $contato->mensagem }}</textarea>

                            <div class="container" style="padding: 0;">
                                @if($contato->visita == NULL)
                                    <form action="{{ route('contato-marcar', $contato->id) }}" method="get">
                                        <div class="d-flex p-2">
                                            <a href="{{route('contato-lista')}}" class="btn btn-default" style="margin-right: 15px;">Voltar</a>
                                            <button type="submit" class="btn btn-success">Marcar como concluído</button>
                                        </div>
                                    </form>
                                @else 
                                    <form action="{{ route('contato-desmarcar', $contato->id) }}" method="get">
                                        <div class="d-flex p-2">
                                            <a href="{{route('contato-lista')}}" class="btn btn-default" style="margin-right: 15px;">Voltar</a>
                                            <button type="submit" class="btn btn-danger">Desmarcar como concluído</button>
                                        </div>
                                    </form>
                                @endif
                                
                            </div>
                        </div>
                        
                        <!-- /.card-body -->
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@stop
        
@section('javascript')

    <script>   

        $(document).ready(function() {
            $('#lista').DataTable({
               "processing": true,
               "responsive": true,
               "colReorder": true,
               "searching": true,
               "lengthMenu": [25, 50, 100],
               "deferRender": true,
               "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar: ",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    },
                    "select": {
                        "rows": {
                            "_": "Selecionado %d linhas",
                            "0": "Nenhuma linha selecionada",
                            "1": "Selecionado 1 linha"
                        }
                    }
                }
            });
            
            $('[name="sLengthMenu"]').css('margin', "5px")
        });

        $('.btnDeletar').click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Você tem certeza?',
                text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim!'
            }).then((result) => {
                if (result.value) {
                    window.location = $(this).attr('href');
                }
            })
        });
    </script>
@stop