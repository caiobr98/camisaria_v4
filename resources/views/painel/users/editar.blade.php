@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 offset-1">
                    
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-usuario-tab" data-toggle="tab" href="#nav-usuario" role="tab" aria-controls="nav-usuario" aria-selected="true">Usuário</a>    
                            <a class="nav-item nav-link" id="nav-senha-tab" data-toggle="tab" href="#nav-senha" role="tab" aria-controls="nav-senha" aria-selected="true">Senha</a>
                        </div>
                    </nav>
                   
                    @include('components.messages')
                    <div class="tab-content" id="nav-tabContent ">
                        <div class="tab-pane fade show active" id="nav-usuario" role="tabpanel" aria-labelledby="nav-usuario-tab">
                            <form action="{{ route('usuario.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    {{ csrf_field() }}

                                <div class="card card-primary card-outline">
                                    <div class="card-body">
                                        @include('components.input', [
                                            'name' => 'nome',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'placeholder' => 'nome',
                                            'value' => $user->name ?? NULL,
                                            'autocomplete' => 'off'
                                            ])
   
                                        @include('components.input', [
                                            'name' => 'email',
                                            'id' => 'txtEmail',
                                            'placeholder' => 'Ex: joaquim@gmail.com',
                                            'label' => 'E-mail',
                                            'type' => 'email',
                                            'value' => $user->email ?? NULL,
                                            'autocomplete' => 'off'
                                            ])

                        
                                        @if($user->foto)
                                            <b>Foto Atual</b>
                                            {{--<img src="{{  url($tecido->foto)}}" class="img-thumbnail">--}}
                                        @endif
                                        <input type='file' id="foto" name="foto" accept="image/*" />
                            
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <a href="{{ route('usuario.index') }}" class="btn btn-default">Cancelar</a>
                                        <button type="submit" class="btn btn-primary">Atualizar</button>
                                    </div>
                                </div><!-- /.card -->
                            </form>
                            
                        </div>
                    
                        <div class="tab-pane fade" id="nav-senha" role="tabpanel" aria-labelledby="nav-senha-tab">
                            <form action="{{ route('editar-senha', $user->id) }}" method="post">
                                <div class="card card-primary card-outline">
                                    <div class="card-body">

                                        @include('components.input', [
                                            'name' => 'password',
                                            'id' => 'txtPassword',
                                            'placeholder' => '******',
                                            'label' => 'Senha',
                                            'type' => 'password',
                                            'autocomplete' => 'off',
                                            ])

                                        @include('components.input', [
                                            'name' => 'password2',
                                            'id' => 'txtPassword2',
                                            'label' => 'Confirmar senha',
                                            'placeholder' => '******',
                                            'type' => 'password',
                                            'autocomplete' => 'off'
                                            ])

                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        @csrf
                                        <a href="exportarfuncionario" class="btn btn-default">Cancelar</a>
                                        <button type="submit" class="btn btn-primary">Atualizar</button>
                                    </div>
                                </div><!-- /.card -->
                            </form>
                            
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script type="text/javascript">       
    $(document).ready(function () {
        $(function(){
            $('.cel-mask').keypress(function(){
                var qtdPalavras = $('.cel-mask').val()    
                $('.cel-mask').mask('(00)000000000');
            })
        });
        
    });
</script>
@endsection
