@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar camiseiro de {{$user->name}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Editar Camiseiro</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('clientes-camiseiro', $user->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="form-group col-md-12">
                                    @include('components.select', [
                                        'name' => 'camiseiro_id',
                                        'id' => 'slcCamiseiro',
                                        'label' => 'Camiseiro',
                                        'selected' => $user->camiseiro ? $user->camiseiro->camiseiro_id : null,
                                        'data' => [
                                            'source' => $camiseiros,
                                            'value' => 'id',
                                            'label' => 'nome'
                                        ]
                                    ])
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <a href="{{route('clientes-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection