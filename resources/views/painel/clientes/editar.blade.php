@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Cliente</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Editar Cliente</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('clientes-editar')}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">

                                @include('components.input', [
                                    'name' => 'users.name',
                                    'id' => 'txtNome',
                                    'label' => 'Nome',
                                    'autocomplete' => 'off',
                                    'value' => $user->name
                                ])

                                @include('components.input', [
                                    'name' => 'clientes.cpf',
                                    'id' => 'txtCpf',
                                    'label' => 'CPF ou CNPJ',
                                    'class' => 'cpfedit',
                                    'autocomplete' => 'off',
                                    'value' => $user->cliente->cpf
                                ])

                                @include('components.input', [
                                    'name' => 'users.email',
                                    'id' => 'txtEmail',
                                    'label' => 'E-Mail',
                                    'autocomplete' => 'off',
                                    'value' => $user->email
                                ])

                                @include('components.select', [
                                    'name' => 'clientes.sexo',
                                    'id' => 'slctSexo',
                                    'label' => 'Sexo',
                                    'selected' => $user->cliente->sexo,
                                    'data' => [
                                        [
                                            'label'=> 'Masculino',
                                            'value'=> 'M'
                                        ],
                                        [
                                            'label' => 'Feminino',
                                            'value' => 'F'
                                        ]
                                    ],
                                ])

                                @include('components.input', [
                                    'name' => 'clientes.telefone',
                                    'id' => 'txtTelefone',
                                    'label' => 'Telefone',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $user->cliente->telefone
                                ])

                                @include('components.input', [
                                    'name' => 'clientes.celular',
                                    'id' => 'txtCelular',
                                    'label' => 'Celular',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $user->cliente->celular
                                ])
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <input type="hidden" name="id" value="{{$user->id}}">
                                <a href="{{route('clientes-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script>
$(function(){
    $('.cpfedit').keypress(function(){
        var qtdPalavras = $('.cpfedit').val()

        if(qtdPalavras.length < 14) {
            $('.cpfedit').mask('000.000.000-00');
        } else {
            $('.cpfedit').mask('00.000.000/0000-00');
        }
    })
});
</script>
@endsection