@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Medida</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item"><a href="{{route('medidas-listar', $user->id)}}">Medidas</a></li>
                        <li class="breadcrumb-item active">Editar Medida</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('medidas-editar', [$user->id, $medida->id])}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                @include('components.input', [
                                    'name' => 'user_id',
                                    'id' => 'txtUserId',
                                    'type' => 'hidden',
                                    'value' => $medida->user_id,
                                    'autocomplete' => 'off'
                                ])
                                @include('components.input', [
                                    'name' => 'id',
                                    'id' => 'txtId',
                                    'type' => 'hidden',
                                    'value' => $medida->id,
                                    'autocomplete' => 'off'
                                ])
                                <div class="form-row">
                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'colarinho',
                                            'id' => 'txtColarinho',
                                            'label' => 'Colarinho',
                                            'type' => 'text',
                                            'value' => $medida->colarinho,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'torax',
                                            'id' => 'txtTorax',
                                            'label' => 'Tórax',
                                            'type' => 'text',
                                            'value' => $medida->torax,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'cintura',
                                            'id' => 'txtCintura',
                                            'label' => 'Cintura',
                                            'type' => 'text',
                                            'value' => $medida->cintura,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'quadril',
                                            'id' => 'txtQuadril',
                                            'label' => 'Quadril',
                                            'type' => 'text',
                                            'value' => $medida->quadril,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'comp_total',
                                            'id' => 'txtComprimentoTotal',
                                            'label' => 'Comprimento Total',
                                            'type' => 'text',
                                            'value' => $medida->comp_total,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'largura_costas',
                                            'id' => 'txtLarguraCostas',
                                            'label' => 'Largura Costas',
                                            'type' => 'text',
                                            'value' => $medida->largura_costas,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'largura_costas_dois',
                                            'id' => 'txtLarguraCostasDois',
                                            'label' => 'Largura Costas Dois',
                                            'type' => 'text',
                                            'value' => $medida->largura_costas_dois,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'ombro',
                                            'id' => 'txtOmbro',
                                            'label' => 'Ombro',
                                            'type' => 'text',
                                            'value' => $medida->ombro,
                                            'autocomplete' => 'off'
                                        ])
                                    </div> 

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'cava',
                                            'id' => 'txtCava',
                                            'label' => 'Cava',
                                            'type' => 'text',
                                            'value' => $medida->cava,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'punho',
                                            'id' => 'txtPunho',
                                            'label' => 'Punho esquerdo',
                                            'type' => 'text',
                                            'value' => $medida->punho,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'comp_mana_d',
                                            'id' => 'txtCompManaD',
                                            'label' => 'Punho direito',
                                            'type' => 'text',
                                            'value' => $medida->comp_mana_d,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    {{-- <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'busto',
                                            'id' => 'txtBusto',
                                            'label' => 'Busto',
                                            'type' => 'text',
                                            'value' => $medida->busto,
                                            'autocomplete' => 'off'
                                        ])
                                    </div> --}}

                                    

                                    {{-- <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'altura_busto',
                                            'id' => 'txtAlturaBusto',
                                            'label' => 'Altura Busto',
                                            'type' => 'text',
                                            'value' => $medida->altura_busto,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'dist_busto',
                                            'id' => 'txtDistBusto',
                                            'label' => 'Distância Busto',
                                            'type' => 'text',
                                            'value' => $medida->dist_busto,
                                            'autocomplete' => 'off'
                                        ])
                                    </div> --}}
                                  
                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'biceps',
                                            'id' => 'txtBiceps',
                                            'label' => 'Bíceps',
                                            'type' => 'text',
                                            'value' => $medida->biceps,
                                            'autocomplete' => 'off'
                                        ])
                                    </div> 

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'comp_manga_e',
                                            'id' => 'txtComprimentoManga',
                                            'label' => 'Comprimento Manga',
                                            'type' => 'text',
                                            'value' => $medida->comp_manga_e,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('medidas-listar', $user->id)}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script>
$(function(){
    function formatarMedida(medida) {
        let newMedida = medida;

        if (!newMedida || newMedida === ',') return '';

        newMedida.replace('.', ',');

        let virgulaPos = newMedida.indexOf(',');

        if (virgulaPos === -1) {
            return newMedida + ',0';
        }

        if (virgulaPos === 0) {
            newMedida = '0' + newMedida;
        }

        //if (newMedida.slice(newMedida.indexOf(',')).length === 2) newMedida = newMedida + '0';
        if (newMedida.slice(newMedida.indexOf(',')).length === 1) newMedida = newMedida + '0';

        return newMedida;
    }

    $('.js-medida-wrapper input').on('blur', function(){
        $(this).val(formatarMedida($(this).val()));
    });

    $('.js-medida-wrapper input').each(function(index, element) {
        $(element).val($(element).val().replace('.',','));
        $(element).val(formatarMedida($(element).val()));
        $(element).mask('ZZZ,0', {
            translation: {
              'Z': {
                pattern: /[0-9]{1,2}/, optional: true
              }
            }
        });
    });            
});
</script>
@endsection