@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Nova Medida</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item"><a href="{{route('medidas-listar', $user->id)}}">Medidas: {{$user->name}}</a></li>
                        <li class="breadcrumb-item active">Nova Medida</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('medidas-cadastrar', $user->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                @include('components.input', [
                                    'name' => 'user_id',
                                    'id' => 'txtUserId',
                                    'type' => 'hidden',
                                    'value' => $user->id,
                                    'autocomplete' => 'off'
                                ])
                                <div class="form-row">
                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'colarinho',
                                            'id' => 'txtColarinho',
                                            'label' => 'Colarinho',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'largura_costas',
                                            'id' => 'txtLarguraCostas',
                                            'label' => 'Largura Costas',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'punho',
                                            'id' => 'txtPunho',
                                            'label' => 'Punho',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'torax',
                                            'id' => 'txtTorax',
                                            'label' => 'Tórax',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'largura_costas_dois',
                                            'id' => 'txtLarguraCostasDois',
                                            'label' => 'Largura Costas Dois',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'busto',
                                            'id' => 'txtBusto',
                                            'label' => 'Busto',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'cintura',
                                            'id' => 'txtCintura',
                                            'label' => 'Cintura',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'cava',
                                            'id' => 'txtCava',
                                            'label' => 'Cava',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'altura_busto',
                                            'id' => 'txtAlturaBusto',
                                            'label' => 'Altura Busto',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'quadril',
                                            'id' => 'txtQuadril',
                                            'label' => 'Quadril',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'comp_manga_e',
                                            'id' => 'txtComprimentoManga',
                                            'label' => 'Comprimento Manga',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'dist_busto',
                                            'id' => 'txtDistBusto',
                                            'label' => 'Distância Busto',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'comp_total',
                                            'id' => 'txtComprimentoTotal',
                                            'label' => 'Comprimento Total',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>
                                    
                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'comp_mana_d',
                                            'id' => 'txtCompManaD',
                                            'label' => 'Comprimento Manga',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'biceps',
                                            'id' => 'txtBiceps',
                                            'label' => 'Bíceps',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div> 

                                    <div class="form-group col-md-4 js-medida-wrapper">
                                        @include('components.input', [
                                            'name' => 'ombro',
                                            'id' => 'txtOmbro',
                                            'label' => 'Ombro',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>                               

                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('medidas-listar', $user->id)}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script>
$(function(){

    function formatarMedida(medida) {
        let newMedida = medida;

        if (!newMedida || newMedida === ',') return '';

        newMedida.replace('.', ',');

        let virgulaPos = newMedida.indexOf(',');

        if (virgulaPos === -1) {
            return newMedida + ',0';
        }

        if (virgulaPos === 0) {
            newMedida = '0' + newMedida;
        }

        //if (newMedida.slice(newMedida.indexOf(',')).length === 2) newMedida = newMedida + '0';
        if (newMedida.slice(newMedida.indexOf(',')).length === 1) newMedida = newMedida + '0';

        return newMedida;
    }

    $('.js-medida-wrapper input').on('blur', function(){
        $(this).val(formatarMedida($(this).val()));
    });

    $('.js-medida-wrapper input').each(function(index, element) {
        $(element).mask('ZZZ,0', {
            translation: {
              'Z': {
                pattern: /[0-9]{1,2}/, optional: true
              }
            }
        });
    });            
});
</script>
@endsection