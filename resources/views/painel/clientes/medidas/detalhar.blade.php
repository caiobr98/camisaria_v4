@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalhar Medida</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item"><a href="{{route('medidas-listar', $user->id)}}">Medidas</a></li>
                        <li class="breadcrumb-item active">Detalhar Medida</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('medidas-editar', [$user->id, $medida->id])}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                @include('components.input', [
                                    'name' => 'user_id',
                                    'id' => 'txtUserId',
                                    'type' => 'hidden',
                                    'value' => $medida->user_id,
                                    'autocomplete' => 'off',
                                    'readonly' => true
                                ])
                                @include('components.input', [
                                    'name' => 'id',
                                    'id' => 'txtId',
                                    'type' => 'hidden',
                                    'value' => $medida->id,
                                    'autocomplete' => 'off',
                                    'readonly' => true
                                ])
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'nome',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'value' => $medida->nome,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'colarinho',
                                            'id' => 'txtColarinho',
                                            'label' => 'Colarinho',
                                            'type' => 'number',
                                            'value' => $medida->colarinho,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'torax',
                                            'id' => 'txtTorax',
                                            'label' => 'Tórax',
                                            'type' => 'number',
                                            'value' => $medida->torax,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'cintura',
                                            'id' => 'txtCintura',
                                            'label' => 'Cintura',
                                            'type' => 'number',
                                            'value' => $medida->cintura,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'quadril',
                                            'id' => 'txtQuadril',
                                            'label' => 'Quadril',
                                            'type' => 'number',
                                            'value' => $medida->quadril,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'comprimento_total',
                                            'id' => 'txtComprimentoTotal',
                                            'label' => 'Comprimento Total',
                                            'type' => 'number',
                                            'value' => $medida->comprimento_total,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'largura_costas',
                                            'id' => 'txtLarguraCostas',
                                            'label' => 'Largura Costas',
                                            'type' => 'number',
                                            'value' => $medida->largura_costas,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'costas_1',
                                            'id' => 'txtCostas1',
                                            'label' => 'Costas 1',
                                            'type' => 'number',
                                            'value' => $medida->costas_1,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'ombro',
                                            'id' => 'txtOmbro',
                                            'label' => 'Ombro',
                                            'type' => 'number',
                                            'value' => $medida->ombro,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'cava',
                                            'id' => 'txtCava',
                                            'label' => 'Cava',
                                            'type' => 'number',
                                            'value' => $medida->cava,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'comprimento_manga',
                                            'id' => 'txtComprimentoManga',
                                            'label' => 'Comprimento Manga',
                                            'type' => 'number',
                                            'value' => $medida->comprimento_manga,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'biceps',
                                            'id' => 'txtBiceps',
                                            'label' => 'Bíceps',
                                            'type' => 'number',
                                            'value' => $medida->biceps,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'punho',
                                            'id' => 'txtPunho',
                                            'label' => 'Punho',
                                            'type' => 'number',
                                            'value' => $medida->punho,
                                            'autocomplete' => 'off',
                                            'readonly' => true
                                        ])
                                    </div>


                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('medidas-listar', $user->id)}}" class="btn btn-default">Voltar</a>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection