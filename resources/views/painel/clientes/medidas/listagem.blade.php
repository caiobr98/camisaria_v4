@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Medidas de {{$user->name}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Medidas</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <a href="{{route('medidas-cadastrar', $user->id)}}" class="btn btn-primary btn-sm">Nova Medida</a>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <!--<table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Nome</th>
                                    <th>Opções</th>
                                </tr>
                                @foreach($user->medidas as $medida)
                                    <tr>
                                        <td>{{$medida->id}}</td>
                                        <td>{{$medida->nome}}</td>
                                        <td>
                                            <a href="{{route('medidas-detalhar', [$user->id, $medida->id])}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Visualizar medidas">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>

                                            <a href="{{route('medidas-editar', [$user->id, $medida->id])}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>

                                            <a href="{{route('medidas-deletar', [$user->id, $medida->id])}}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">
                                                <i class="fa fa-remove" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>-->

                            @if(count($medidas) > 0)
                            <table class="table table-hover table-responsive">
                              <thead>
                                  <tr>
                                      <th scope="col">Colarinho</th>
                                      <th scope="col">Largura costas</th>
                                      <th scope="col">Punho direito</th>
                                      <th scope="col">Torax</th>
                                      <th scope="col">Largura costas 2</th>
                                      <th scope="col">Cintura</th>
                                      {{-- <th scope="col">Busto</th>
                                      <th scope="col">Altura Busto</th>
                                      <th scope="col">Distância Busto</th> --}}
                                      <th scope="col">Cava</th>
                                      <th scope="col">Quadril</th>
                                      <th scope="col">Manga</th>
                                      <th scope="col">Punho esquerdo</th>
                                      <th scope="col">Comp manga</th>
                                      <th scope="col">Biceps</th>
                                      <th scope="col">Ombro</th>
                                      <th scope="col">Ações</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($medidas as $medida)
                                <tr>
                                    <td><span>{{ $medida->colarinho }}</span></td>
                                    <td><span>{{ $medida->largura_costas }}</span></td>
                                    <td><span>{{ $medida->comp_mana_d }}</span></td>
                                    <td><span>{{ $medida->torax }}</span></td>
                                    <td><span>{{ $medida->largura_costas_dois }}</span></td>
                                    <td><span>{{ $medida->cintura }}</span></td>
                                    {{-- <td><span>{{ $medida->busto }}</span></td>
                                    <td><span>{{ $medida->altura_busto }}</span></td>
                                    <td><span>{{ $medida->dist_busto }}</span></td> --}}
                                    <td><span>{{ $medida->cava }}</span></td>
                                    <td><span>{{ $medida->quadril }}</span></td>
                                    <td><span>{{ $medida->punho }}</span></td>
                                    <td><span>{{ $medida->comp_total }}</span></td>
                                    <td><span>{{ $medida->comp_manga_e }}</span></td>
                                    <td><span>{{ $medida->biceps }}</span></td>
                                    <td><span>{{ $medida->ombro }}</span></td>
                                      <!-- <router-link :to="`/admin/usuarios/edit/${ 1 }`" class="ferramenta">
                                          <i class="fas fa-pencil-alt"></i>
                                      </router-link> -->
                                    <td>
                                        <a href="{{route('medidas-editar', [$user->id, $medida->id])}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>

                                        <a href="{{route('medidas-deletar', [$user->id, $medida->id])}}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">
                                            <i class="fa fa-remove" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach                                
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <td scope="col">Colarinho</td>
                                      <td scope="col">Largura costas</td>
                                      <td scope="col">Punho direito</td>
                                      <td scope="col">Torax</td>
                                      <td scope="col">Largura costas 2</td>
                                      <td scope="col">Cintura</td>
                                      {{-- <td scope="col">Busto</td>
                                      <td scope="col">Altura Busto</td>
                                      <td scope="col">Distância Busto</td> --}}
                                      <td scope="col">Cava</td>
                                      <td scope="col">Quadril</td>
                                      <td scope="col">Manga</td>
                                      <td scope="col">Comp total</td>
                                      <td scope="col">Comp manga</td>
                                      <td scope="col">Biceps</td>
                                      <td scope="col">Ombro</td>
                                      <td scope="col">Ações</td>
                                  </tr>
                              </tfoot>
                          </table>
                          @else
                          <p class="col-sm-12"><br>Não há medidas para esse usuário.</p>
                          @endif
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            @csrf
                            <a href="{{route('clientes-listar')}}" class="btn btn-default">Voltar</a>
                        </div>
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $('.btnDeletar').click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Você tem certeza?',
                text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim!'
            }).then((result) => {
                if (result.value) {
                    window.location = $(this).attr('href');
                }
            })
        });
    </script>
@endsection