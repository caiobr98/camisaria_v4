@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalhar Endereço de {{$user->name}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Detalhar Endereço</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('clientes-endereco-editar')}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        @include('components.input', [
                                            'name' => 'cep',
                                            'id' => 'txtCep',
                                            'label' => 'CEP',
                                            'class' => 'cep',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->cep,
                                            'readonly' => true
                                        ])

                                        @include('components.input', [
                                            'name' => 'cidade',
                                            'id' => 'txtCidade',
                                            'label' => 'Cidade',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->cidade,
                                            'readonly' => true
                                        ])

                                        @include('components.input', [
                                            'name' => 'rua',
                                            'id' => 'txtRua',
                                            'label' => 'Rua',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->rua,
                                            'readonly' => true
                                        ])

                                        @include('components.input', [
                                            'name' => 'observacoes',
                                            'id' => 'txtObs',
                                            'label' => 'Observações',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->observacoes,
                                            'readonly' => true
                                        ])
                                    </div>

                                    <div class="col-md-6">
                                        @include('components.input', [
                                            'name' => 'uf',
                                            'id' => 'txtUf',
                                            'label' => 'UF',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->uf,
                                            'readonly' => true
                                        ])

                                        @include('components.input', [
                                           'name' => 'bairro',
                                           'id' => 'txtBairro',
                                           'label' => 'Bairro',
                                           'autocomplete' => 'off',
                                           'value' => $endereco->bairro,
                                           'readonly' => true
                                       ])

                                        @include('components.input', [
                                            'name' => 'numero',
                                            'id' => 'txtNumero',
                                            'label' => 'Número',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->numero,
                                            'readonly' => true
                                        ])

                                        @include('components.input', [
                                            'name' => 'numero',
                                            'id' => 'txtNumero',
                                            'label' => 'Número',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->tipo === 1 ? 'Principal' : 'Secundário',
                                            'readonly' => true
                                        ])
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <a href="{{route('clientes-listar')}}" class="btn btn-default">Voltar</a>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection