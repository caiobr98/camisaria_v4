@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Novo Endereço para {{$user->name}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Novo Endereço</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('clientes-endereco-editar')}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        @include('components.input', [
                                            'name' => 'cep',
                                            'id' => 'txtCep',
                                            'label' => 'CEP',
                                            'class' => 'cep',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->cep
                                        ])

                                        @include('components.input', [
                                            'name' => 'cidade',
                                            'id' => 'txtCidade',
                                            'label' => 'Cidade',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->cidade
                                        ])

                                        @include('components.input', [
                                            'name' => 'rua',
                                            'id' => 'txtRua',
                                            'label' => 'Rua',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->rua
                                        ])

                                        @include('components.input', [
                                            'name' => 'observacoes',
                                            'id' => 'txtObs',
                                            'label' => 'Observações',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->observacoes
                                        ])
                                    </div>

                                    <div class="col-md-6">
                                        @include('components.input', [
                                            'name' => 'uf',
                                            'id' => 'txtUf',
                                            'label' => 'UF',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->uf
                                        ])

                                        @include('components.input', [
                                           'name' => 'bairro',
                                           'id' => 'txtBairro',
                                           'label' => 'Bairro',
                                           'autocomplete' => 'off',
                                           'value' => $endereco->bairro
                                       ])

                                        @include('components.input', [
                                            'name' => 'numero',
                                            'id' => 'txtNumero',
                                            'label' => 'Número',
                                            'autocomplete' => 'off',
                                            'value' => $endereco->numero
                                        ])

                                        @include('components.select', [
                                            'name' => 'tipo',
                                            'id' => 'slctTipo',
                                            'label' => 'Tipo',
                                            'selected' => $endereco->tipo,
                                            'data' => [
                                                [
                                                    'label'=> 'Principal',
                                                    'value'=> 1
                                                ],
                                                [
                                                    'label' => 'Secundário',
                                                    'value' => 2
                                                ]
                                            ],
                                        ])
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <input type="hidden" name="id" value="{{$endereco->id}}">
                                <input type="hidden" name="user_id" value="{{$endereco->user_id}}">
                                <a href="{{route('clientes-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            $('#txtCep').blur(function(){
                $.ajax({
                    url: 'https://viacep.com.br/ws/' + $("#txtCep").val().replace('-', '') + '/json/',
                    success: function(result){
                        if(result.erro){
                            Swal.fire({
                                title: 'O CEP digitado não foi encontrado',
                                type: 'error'
                            })
                        }
                        else{
                            $("#txtUf").val(result.uf);
                            $("#txtCidade").val(result.localidade);
                            $("#txtBairro").val(result.bairro);
                            $("#txtRua").val(result.logradouro);
                        }
                    },
                    error: function(){
                        Swal.fire({
                            title: 'Erro ao buscar o CEP',
                            type: 'error'
                        })
                    }
                });
            });
        });
    </script>
@endsection