@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Processo de {{$user->name}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Editar Camiseiro</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-10 offset-1">
                    @include('components.messages')

                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link active" id="nav-usuario-tab" data-toggle="tab" href="#nav-usuario" role="tab" aria-controls="nav-usuario" aria-selected="true">Usuário</a>
                          <a class="nav-item nav-link" id="nav-camiseiro-tab" data-toggle="tab" href="#nav-camiseiro" role="tab" aria-controls="nav-camiseiro" aria-selected="true">Camiseiro</a>
                          <a class="nav-item nav-link" id="nav-endereco-tab" data-toggle="tab" href="#nav-endereco" role="tab" aria-controls="nav-endereco" aria-selected="false">Endereço</a>
                          <a class="nav-item nav-link" id="nav-medidas-tab" data-toggle="tab" href="#nav-medidas" role="tab" aria-controls="nav-medidas" aria-selected="false">Medidas</a>
                          <a class="nav-item nav-link" id="nav-historico-tab" data-toggle="tab" href="#nav-historico" role="tab" aria-controls="nav-historico" aria-selected="false">Histórico</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-usuario" role="tabpanel" aria-labelledby="nav-usuario-tab">
                            <form action="{{route('clientes-editar')}}" method="POST">
                                <div class="card card-primary card-outline">
                                    <div class="card-body">
        
                                        @include('components.input', [
                                            'name' => 'users.name',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'value' => $user->name,
                                            'autocomplete' => 'off'
                                        ])
        
                                        @include('components.input', [
                                            'name' => 'clientes.cpf',
                                            'id' => 'txtCpf',
                                            'label' => 'CPF ou CNPJ',
                                            'class' => 'processo_cpf',
                                            'value' => $user->cliente->cpf ?? $user->cpf,
                                            'autocomplete' => 'off'
                                        ])
        
                                        @include('components.input', [
                                            'name' => 'users.email',
                                            'id' => 'txtEmail',
                                            'label' => 'E-Mail',
                                            'value' => $user->email,
                                            'autocomplete' => 'off'
                                        ])
        
                                        @include('components.select', [
                                            'name' => 'clientes.sexo',
                                            'id' => 'slctSexo',
                                            'label' => 'Sexo',
                                            'selected' => $user->cliente->sexo,
                                            'data' => [
                                                [
                                                    'label'=> 'Masculino',
                                                    'value'=> 'M'
                                                ],
                                                [
                                                    'label' => 'Feminino',
                                                    'value' => 'F'
                                                ]
                                            ],
                                        ])
        
                                        @include('components.input', [
                                            'name' => 'clientes.telefone',
                                            'id' => 'txtTelefone',
                                            'label' => 'Telefone',
                                            'class' => 'telefone',
                                            'value' => $user->cliente->telefone,
                                            'autocomplete' => 'off'
                                        ])
        
                                        @include('components.input', [
                                            'name' => 'clientes.celular',
                                            'id' => 'txtCelular',
                                            'label' => 'Celular',
                                            'class' => 'telefone',
                                            'value' => $user->cliente->celular,
                                            'autocomplete' => 'off'
                                        ])
        
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <a href="{{route('clientes-listar')}}" class="btn btn-default">Voltar para clientes</a>
                                        <button type="submit" class="btn btn-primary">Editar</button>
                                    </div>
                                </div><!-- /.card -->
                            </form>
                        </div>
                        <div class="tab-pane fade show" id="nav-camiseiro" role="tabpanel" aria-labelledby="nav-camiseiro-tab">
                            <form action="{{route('clientes-camiseiro', $user->id)}}" method="POST">
                                <div class="card card-primary card-outline">
                                    <div class="card-body">
                                        <div class="form-group col-md-12">
                                            @include('components.select', [
                                                'name' => 'camiseiro_id',
                                                'id' => 'slcCamiseiro',
                                                'label' => 'Camiseiro',
                                                'selected' => $user->camiseiro ? $user->camiseiro->camiseiro_id : null,
                                                'data' => [
                                                    'source' => $camiseiros,
                                                    'value' => 'id',
                                                    'label' => 'nome'
                                                ]
                                            ])
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <a href="{{route('clientes-listar')}}" class="btn btn-default">Voltar para clientes</a>
                                        <button type="submit" class="btn btn-primary">Editar</button>
                                    </div>
                                </div><!-- /.card -->
                            </form>
                        </div>
                        <div class="tab-pane fade" id="nav-endereco" role="tabpanel" aria-labelledby="nav-endereco-tab">
                            <form action="{{route('clientes-endereco-cadastrar')}}" method="POST">
                                <div class="card card-primary card-outline">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                @include('components.input', [
                                                    'name' => 'cep',
                                                    'id' => 'processotxtCep',
                                                    'label' => 'CEP',
                                                    'class' => 'cep',
                                                    // 'value' => $endereco->cep ?? null,    
                                                    'autocomplete' => 'off'
                                                ])
        
                                                @include('components.input', [
                                                    'name' => 'cidade',
                                                    'id' => 'txtCidade',
                                                    'label' => 'Cidade',
                                                    // 'value' => $endereco->cidade ?? null,
                                                    'autocomplete' => 'off'
                                                ])
        
                                                @include('components.input', [
                                                    'name' => 'rua',
                                                    'id' => 'txtRua',
                                                    'label' => 'Rua',
                                                    // 'value' => $endereco->rua ?? null,
                                                    'autocomplete' => 'off'
                                                ])
        
                                                @include('components.input', [
                                                    'name' => 'observacoes',
                                                    'id' => 'txtObs',
                                                    'label' => 'Observações',
                                                    // 'value' => $endereco->observacoes ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>
        
                                            <div class="col-md-6">
                                                @include('components.input', [
                                                    'name' => 'uf',
                                                    'id' => 'txtUf',
                                                    'label' => 'UF',
                                                    // 'value' => $endereco->uf ?? null,
                                                    'autocomplete' => 'off'
                                                ])
        
                                                @include('components.input', [
                                                   'name' => 'bairro',
                                                   'id' => 'txtBairro',
                                                   'label' => 'Bairro',
                                                //    'value' => $endereco->bairro ?? null,
                                                   'autocomplete' => 'off'
                                               ])
        
                                                @include('components.input', [
                                                    'name' => 'numero',
                                                    'id' => 'txtNumero',
                                                    'label' => 'Número',
                                                    // 'value' => $endereco->numero ?? null,
                                                    'autocomplete' => 'off'
                                                ])
        
                                                @include('components.select', [
                                                    'name' => 'tipo',
                                                    'id' => 'slctTipo',
                                                    'label' => 'Tipo',
                                                    // 'selected' => $endereco->tipo ?? null,
                                                    'data' => [
                                                        [
                                                            'label'=> 'Principal',
                                                            'value'=> 1
                                                        ],
                                                        [
                                                            'label' => 'Secundário',
                                                            'value' => 2
                                                        ]
                                                    ],
                                                ])
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <a href="{{route('clientes-listar')}}" class="btn btn-default">Voltar para clientes</a>
                                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                                    </div>
                                </div><!-- /.card -->
                            </form>
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Tipo</th>
                                        <th>CEP</th>
                                        <th>UF</th>
                                        <th>Bairro</th>
                                        <th>Cidade</th>
                                        <th>Rua</th>
                                        <th>Numero</th>
                                        <th>Opções</th>
                                    </tr>
                                    @foreach($user->enderecos as $e)
                                        <tr>
                                            <td>@if($e->tipo === 1) Principal @else Secundário @endif</td>
                                            <td>{{$e->cep}}</td>
                                            <td>{{$e->uf}}</td>
                                            <td>{{$e->bairro}}</td>
                                            <td>{{$e->cidade}}</td>
                                            <td>{{$e->rua}}</td>
                                            <td>{{$e->numero}}</td>
                                            <td>
                                                <a href="{{route('clientes-endereco-form-detalhar', [$user->id, $e->id])}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Detalhar">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>
    
                                                <a href="{{route('clientes-endereco-form-editar', [$user->id, $e->id])}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
    
                                                <a href="{{route('clientes-endereco-deletar', $e->id)}}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">
                                                    <i class="fa fa-remove" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-medidas" role="tabpanel" aria-labelledby="nav-medidas-tab">
                            <form action="{{route('medidas-cadastrar', [$user->id, 'processoCamiseiroEnderecoMedidas'])}}" method="POST">
                                <div class="card card-primary card-outline">
                                    <div class="card-body">
                                        @include('components.input', [
                                            'name' => 'user_id',
                                            'id' => 'txtUserId',
                                            'type' => 'hidden',
                                            'value' => $user->id,
                                            'autocomplete' => 'off'
                                        ])

                                        @include('components.input', [
                                            'name' => 'id',
                                            'id' => 'txtId',
                                            'type' => 'hidden',
                                            'value' => $medida->id ?? null,
                                            'autocomplete' => 'off'
                                        ])
                                        <div class="form-row">
                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'colarinho',
                                                    'id' => 'txtColarinho',
                                                    'label' => 'Colarinho',
                                                    'type' => 'text',
                                                    'value' => $medida->colarinho ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'torax',
                                                    'id' => 'txtTorax',
                                                    'label' => 'Tórax',
                                                    'type' => 'text',
                                                    'value' => $medida->torax ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'cintura',
                                                    'id' => 'txtCintura',
                                                    'label' => 'Cintura',
                                                    'type' => 'text',
                                                    'value' => $medida->cintura ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'quadril',
                                                    'id' => 'txtQuadril',
                                                    'label' => 'Quadril',
                                                    'type' => 'text',
                                                    'value' => $medida->quadril ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'comp_total',
                                                    'id' => 'txtComprimentoTotal',
                                                    'label' => 'Comprimento Total',
                                                    'type' => 'text',
                                                    'value' => $medida->comp_total ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'largura_costas',
                                                    'id' => 'txtLarguraCostas',
                                                    'label' => 'Largura Costas',
                                                    'type' => 'text',
                                                    'value' => $medida->largura_costas ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>


                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'largura_costas_dois',
                                                    'id' => 'txtLarguraCostasDois',
                                                    'label' => 'Largura Costas Dois',
                                                    'type' => 'text',
                                                    'value' => $medida->largura_costas_dois ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            {{-- <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'busto',
                                                    'id' => 'txtBusto',
                                                    'label' => 'Busto',
                                                    'type' => 'text',
                                                    'value' => $medida->busto,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div> --}}
                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'ombro',
                                                    'id' => 'txtOmbro',
                                                    'label' => 'Ombro',
                                                    'type' => 'text',
                                                    'value' => $medida->ombro ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div> 
                                            

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'cava',
                                                    'id' => 'txtCava',
                                                    'label' => 'Cava',
                                                    'type' => 'text',
                                                    'value' => $medida->cava ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'punho',
                                                    'id' => 'txtPunho',
                                                    'label' => 'Punho esquerdo',
                                                    'type' => 'text',
                                                    'value' => $medida->punho ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'comp_mana_d',
                                                    'id' => 'txtCompManaD',
                                                    'label' => 'Punho direito',
                                                    'type' => 'text',
                                                    'value' => $medida->comp_mana_d ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'biceps',
                                                    'id' => 'txtBiceps',
                                                    'label' => 'Bíceps',
                                                    'type' => 'text',
                                                    'value' => $medida->biceps ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div> 

                                            {{-- <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'altura_busto',
                                                    'id' => 'txtAlturaBusto',
                                                    'label' => 'Altura Busto',
                                                    'type' => 'text',
                                                    'value' => $medida->altura_busto,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'dist_busto',
                                                    'id' => 'txtDistBusto',
                                                    'label' => 'Distância Busto',
                                                    'type' => 'text',
                                                    'value' => $medida->dist_busto,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div> --}}

                                            

                                            <div class="form-group col-md-4 js-medida-wrapper">
                                                @include('components.input', [
                                                    'name' => 'comp_manga_e',
                                                    'id' => 'txtComprimentoManga',
                                                    'label' => 'Comprimento Manga',
                                                    'type' => 'text',
                                                    'value' => $medida->comp_manga_e ?? null,
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        @csrf
                                        <a href="{{route('medidas-listar', $user->id)}}" class="btn btn-default">Voltar para clientes</a>
                                        <button type="submit" class="btn btn-primary js-btn-submit-medida">
                                            @if(isset($medida))
                                            Editar
                                            @else
                                            Cadastrar
                                            @endif
                                        </button>
                                    </div>
                                </div><!-- /.card -->
                            </form>
                        </div>

                        <div class="tab-pane fade" id="nav-historico" role="tabpanel" aria-labelledby="nav-historico-tab">
                            <form action="{{route('historico-cadastrar', [$user->id, 'processoCamiseiroEnderecoMedidas'])}}" method="POST">
                                <div class="card card-primary card-outline">
                                    <div class="card-body">
                                        @include('components.input', [
                                            'name' => 'user_id',
                                            'id' => 'txtUserId',
                                            'type' => 'hidden',
                                            'value' => $user->id,
                                            'autocomplete' => 'off'
                                        ])
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                @include('components.input', [
                                                    'name' => 'codigo_tecido',
                                                    'id' => 'txtCodigo_tecido',
                                                    'label' => 'Codigo do tecido',
                                                    'type' => 'text',
                                                    'autocomplete' => 'off'
                                                ])
                                            </div>
                                            
                                            <div class="form-group col-md-6">
                                                @include('components.select', [
                                                    'name' => 'fornecedor_id',
                                                    'id' => 'txtFornecedor',
                                                    'label' => 'Fornecedor',
                                                    'autocomplete' => 'off',
                                                    'data' => [
                                                        'source' => $fornecedores ?? array('id' => 1, 'nome' => ''),
                                                        'value' => 'id',
                                                        'label' => 'nome'
                                                    ]
                                                ])
                                            </div>

                                            <div class="form-group col-md-6">
                                                @include('components.input', [
                                                    'name' => 'data_compra',
                                                    'id' => 'txtData_compra',
                                                    'label' => 'Data da Compra',
                                                    'type' => 'text',
                                                    'autocomplete' => 'off',
                                                    'class' => 'date-mask datapicker'
                                                ])
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        @csrf
                                        <a href="{{route('clientes-listar', $user->id)}}" class="btn btn-default">Voltar para clientes</a>
                                        <button type="submit" class="btn btn-primary">
                                            Cadastrar
                                        </button>
                                    </div>
                                </div><!-- /.card -->
                            </form>

                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Código do Tecido</th>
                                        <th>Fornecedor</th>
                                        <th>Data da compra</th>
                                        <th>Opções</th>
                                    </tr>
                                    @foreach($user->historico as $e)
                                        <tr>
                                            <td>{{$e->codigo_tecido}}</td>
                                            <td>{{  $e->fornecedor->nome}}</td>
                                            <td>{{ \Carbon\Carbon::parse($e->data_compra)->format('d/m/Y')}}</td>
                                            <td>
                                                <a href="{{route('clientes-historico-form-editar', [$user->id, $e->id])}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>

                                                <a href="{{route('clientes-historico-deletar', $e->id)}}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">
                                                    <i class="fa fa-remove" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){

            $(function(){
                $('.date-mask').keypress(function(){
                var qtdPalavras = $('.date-mask').val()    
                $('.date-mask').mask('00/00/0000');
                })
            });

            $('.processo_cpf').keypress(function(){
                var qtdPalavras = $('.processo_cpf').val()

                if(qtdPalavras.length < 14) {
                    $('.processo_cpf').mask('000.000.000-00');
                } else {
                    $('.processo_cpf').mask('00.000.000/0000-00');
                }
            })

            function formatarMedida(medida) {
                let newMedida = medida;

                if (!newMedida || newMedida === ',') return '';

                newMedida.replace('.', ',');

                let virgulaPos = newMedida.indexOf(',');

                if (virgulaPos === -1) {
                    return newMedida + ',0';
                }

                if (virgulaPos === 0) {
                    newMedida = '0' + newMedida;
                }

                //if (newMedida.slice(newMedida.indexOf(',')).length === 2) newMedida = newMedida + '0';
                if (newMedida.slice(newMedida.indexOf(',')).length === 1) newMedida = newMedida + '0';

                return newMedida;
            }

            setInterval(function(){
                let bloqueia = false;
                $('.js-medida-wrapper input').each(function(index, element){
                    if (!$(element).val()) bloqueia = true;
                });

                if(bloqueia) {
                    $('.js-btn-submit-medida').addClass('disabled');
                    $('.js-btn-submit-medida').css('pointer-events','none');
                } else {
                    $('.js-btn-submit-medida').removeClass('disabled');
                    $('.js-btn-submit-medida').css('pointer-events','auto');
                }
            }, 100);

            $('.js-medida-wrapper input').on('change', function(){

            });

            $('.js-medida-wrapper input').on('blur', function(){
                $(this).val(formatarMedida($(this).val()));
            });


            $('.js-medida-wrapper input').each(function(index, element) {
                $(element).mask('ZZZ,0', {
                    translation: {
                      'Z': {
                        pattern: /[0-9]{1,2}/, optional: true
                      }
                    }
                });
            });    

            $('#processotxtCep').blur(function(){
                $.ajax({
                    url: 'https://viacep.com.br/ws/' + $("#processotxtCep").val().replace('-', '') + '/json/',
                    success: function(result){
                        if(result.erro){
                            Swal.fire({
                                title: 'O CEP digitado não foi encontrado',
                                type: 'error'
                            })
                        }
                        else{
                            $("#txtUf").val(result.uf);
                            $("#txtCidade").val(result.localidade);
                            $("#txtBairro").val(result.bairro);
                            $("#txtRua").val(result.logradouro);
                        }
                    },
                    error: function(){
                        Swal.fire({
                            title: 'Erro ao buscar o CEP',
                            type: 'error'
                        })
                    }
                });
            });
        });
    </script>
@endsection