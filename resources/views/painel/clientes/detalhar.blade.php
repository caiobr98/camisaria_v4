@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalhar Cliente</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Detalhar Cliente</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('clientes-editar')}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">

                                @include('components.input', [
                                    'name' => 'users.name',
                                    'id' => 'txtNome',
                                    'label' => 'Nome',
                                    'autocomplete' => 'off',
                                    'value' => $user->name,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'clientes.cpf',
                                    'id' => 'txtCpf',
                                    'label' => 'CPF',
                                    'class' => 'cpf',
                                    'autocomplete' => 'off',
                                    'value' => $user->cliente->cpf,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'users.email',
                                    'id' => 'txtEmail',
                                    'label' => 'E-Mail',
                                    'autocomplete' => 'off',
                                    'value' => $user->email,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'clientes.sexo',
                                    'id' => 'slctSexo',
                                    'label' => 'Sexo',
                                    'value' => $user->cliente->sexo === 'M' ? 'Masculino' : 'Feminino',
                                    'readonly' => true,
                                ])

                                @include('components.input', [
                                    'name' => 'clientes.telefone',
                                    'id' => 'txtTelefone',
                                    'label' => 'Telefone',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $user->cliente->telefone,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'clientes.celular',
                                    'id' => 'txtCelular',
                                    'label' => 'Celular',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $user->cliente->celular,
                                    'readonly' => true
                                ])
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <a href="{{route('clientes-listar')}}" class="btn btn-default">Voltar</a>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection