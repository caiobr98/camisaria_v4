@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Nova Medida</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item"><a href="{{route('medidas-listar', $user->id)}}">Medidas: {{$user->name}}</a></li>
                        <li class="breadcrumb-item active">Nova Medida</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('historico-cadastrar', [$user->id, 'processoCamiseiroEnderecoMedidas'])}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                @include('components.input', [
                                    'name' => 'user_id',
                                    'id' => 'txtUserId',
                                    'type' => 'hidden',
                                    'value' => $user->id,
                                    'autocomplete' => 'off'
                                ])
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'codigo_tecido',
                                            'id' => 'txtCodigo_tecido',
                                            'label' => 'Codigo do tecido',
                                            'type' => 'text',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.select', [
                                            'name' => 'fornecedor_id',
                                            'id' => 'txtFornecedor',
                                            'label' => 'Fornecedor',
                                            'autocomplete' => 'off',
                                            'selected' => $user->fornecedor ? $user->fornecedor->fornecedor_id : null,
                                            'data' => [
                                                'source' => $fornecedores,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'data_compra',
                                            'id' => 'txtData_compra',
                                            'label' => 'Data da Compra',
                                            'type' => 'text',
                                            'autocomplete' => 'off',
                                            'class' => 'datapicker'
                                        ])
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('historico-listar', $user->id)}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection