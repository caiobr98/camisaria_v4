@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Novo Histórico para {{$user->name}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Novo Histórico</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('clientes-historico-editar')}}" method="POST">
                            <div class="card card-primary card-outline">
                                <div class="card-body">
                                    @include('components.input', [
                                        'name' => 'user_id',
                                        'id' => 'txtUserId',
                                        'type' => 'hidden',
                                        'value' => $historico->user_id,
                                        'autocomplete' => 'off'
                                    ])
                                    @include('components.input', [
                                        'name' => 'id',
                                        'id' => 'txtId',
                                        'type' => 'hidden',
                                        'value' => $historico->id,
                                        'autocomplete' => 'off'
                                    ])
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            @include('components.input', [
                                                'name' => 'codigo_tecido',
                                                'id' => 'txtCodigo_tecido',
                                                'label' => 'Codigo do tecido',
                                                'type' => 'text',
                                                'value' => $historico->codigo_tecido,
                                                'autocomplete' => 'off'
                                            ])
                                        </div>
    
                                        <div class="form-group col-md-6">
                                            @include('components.select', [
                                                'name' => 'fornecedor_id',
                                                'id' => 'txtFornecedor',
                                                'label' => 'Fornecedor',
                                                'autocomplete' => 'off',
                                                'selected' => $historico->fornecedor_id,
                                                'data' => [
                                                    'source' => $fornecedores,
                                                    'value' => 'id',
                                                    'label' => 'nome'
                                                ]
                                            ])
                                        </div>
    
                                        <div class="form-group col-md-6">
                                            @include('components.input', [
                                                'name' => 'data_compra',
                                                'id' => 'txtData_compra',
                                                'label' => 'Data da Compra',
                                                'type' => 'text',
                                                'value' => \Carbon\Carbon::parse($historico->data_compra)->format('d/m/Y'),
                                                'autocomplete' => 'off',
                                                'class' => 'datapicker'
                                            ])
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    @csrf
                                    <a href="{{ route('historico-listar', $user->id)}}" class="btn btn-default">Cancelar</a>
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </div><!-- /.card -->
                        </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            $('#txtCep').blur(function(){
                $.ajax({
                    url: 'https://viacep.com.br/ws/' + $("#txtCep").val().replace('-', '') + '/json/',
                    success: function(result){
                        if(result.erro){
                            Swal.fire({
                                title: 'O CEP digitado não foi encontrado',
                                type: 'error'
                            })
                        }
                        else{
                            $("#txtUf").val(result.uf);
                            $("#txtCidade").val(result.localidade);
                            $("#txtBairro").val(result.bairro);
                            $("#txtRua").val(result.logradouro);
                        }
                    },
                    error: function(){
                        Swal.fire({
                            title: 'Erro ao buscar o CEP',
                            type: 'error'
                        })
                    }
                });
            });
        });
    </script>
@endsection