@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Novo Cliente</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('clientes-listar')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">Novo Cliente</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-10 offset-1">
                    @include('components.messages')
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link active" id="nav-usuario-tab" data-toggle="tab" href="#nav-usuario" role="tab" aria-controls="nav-usuario" aria-selected="true">Usuário</a>

                          <!-- cliente_pediu -->
                          <a class="nav-item nav-link disabled" id="nav-camiseiro-tab" data-toggle="tab" href="#nav-camiseiro" role="tab" aria-controls="nav-camiseiro" aria-selected="true">Camiseiro</a>
                          <a class="nav-item nav-link disabled" id="nav-endereco-tab" data-toggle="tab" href="#nav-endereco" role="tab" aria-controls="nav-endereco" aria-selected="false">Endereço</a>
                          <a class="nav-item nav-link disabled" id="nav-medidas-tab" data-toggle="tab" href="#nav-medidas" role="tab" aria-controls="nav-medidas" aria-selected="false">Medidas</a>
                          <a class="nav-item nav-link disabled" id="nav-historico-tab" data-toggle="tab" href="#nav-historico" role="tab" aria-controls="nav-historico" aria-selected="false">Histórico</a>
                          <!-- fim_cliente_pediu -->
                          
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent ">
                        <div class="tab-pane fade show active" id="nav-usuario" role="tabpanel" aria-labelledby="nav-usuario-tab">
                            <form action="{{route('clientes-cadastrar')}}" method="POST">
                                <div class="card card-primary card-outline">
                                    <div class="card-body">

                                        @include('components.input', [
                                            'name' => 'users.name',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'autocomplete' => 'off'
                                        ])

                                        @include('components.input', [
                                            'name' => 'clientes.cpf',
                                            'id' => 'txtCpf',
                                            'label' => 'CPF ou CNPJ',
                                            'class' => 'cpf',
                                            'autocomplete' => 'off'
                                        ])

                                        @include('components.input', [
                                            'name' => 'users.email',
                                            'id' => 'txtEmail',
                                            'label' => 'E-Mail',
                                            'autocomplete' => 'off'
                                        ])

                                        @include('components.select', [
                                            'name' => 'clientes.sexo',
                                            'id' => 'slctSexo',
                                            'label' => 'Sexo',
                                            'data' => [
                                                [
                                                    'label'=> 'Masculino',
                                                    'value'=> 'M'
                                                ],
                                                [
                                                    'label' => 'Feminino',
                                                    'value' => 'F'
                                                ]
                                            ],
                                        ])

                                        @include('components.input', [
                                            'name' => 'clientes.telefone',
                                            'id' => 'txtTelefone',
                                            'label' => 'Telefone',
                                            'class' => 'telefone',
                                            'autocomplete' => 'off'
                                        ])

                                        @include('components.input', [
                                            'name' => 'clientes.celular',
                                            'id' => 'txtCelular',
                                            'label' => 'Celular',
                                            'class' => 'telefone',
                                            'autocomplete' => 'off'
                                        ])

                                        @include('components.input', [
                                            'name' => 'users.password',
                                            'id' => 'txtSenha',
                                            'label' => 'Senha',
                                            'type' => 'password',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        @csrf
                                        <a href="{{route('clientes-listar')}}" class="btn btn-default">Cancelar</a>
                                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                                    </div>
                                </div><!-- /.card -->
                            </form>
                        </div>
                    
                    </div>
                    <!-- /.col-md-6 -->
                </div>
            </div>  <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script>
$(function(){
    $('.cpf').keypress(function(){
        var qtdPalavras = $('.cpf').val()

        if(qtdPalavras.length < 14) {
            $('.cpf').mask('000.000.000-00');
        } else {
            $('.cpf').mask('00.000.000/0000-00');
        }
    })
});

</script>
@endsection