@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Clientes</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Clientes</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <a href="{{route('clientes-cadastrar')}}" class="btn btn-primary btn-sm">Novo Cliente</a>
                        </div>

                        <div class="content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table table-condensed table-hover table-sm">
                                            <table id="tabelaClientes" class="">
                                                <thead>
                                                    <tr style="text-align: center"> 
                                                        <th>Nome</th>
                                                        <th>E-mail</th>
                                                        <th>Celular</th>
                                                        <th>Telefone</th>
                                                        <th>Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($clientes as $c)
                                                        <tr>
                                                            <td>{{ $c->name }}</td>
                                                            <td>{{ $c->email }}</td>
                                                            <td>{{ $c->celular }}</td>
                                                            <td>{{ $c->telefone }}</td>
                                                            <td>
                                                                <a href="{{route('clientes-camiseiro', $c->id)}}" class="btn btn-dark btn-sm" data-toggle="tooltip" data-placement="top" title="Camiseiro">
                                                                    <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                                                </a>

                                                                <a href="{{route('clientes-endereco', $c->id)}}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Endereço">
                                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                </a>

                                                                <a href="{{route('medidas-listar', $c->id)}}" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Medidas">
                                                                    <i class="fa fa-arrows-v" aria-hidden="true"></i>
                                                                </a>

                                                                <a href="{{route('historico-listar', $c->id)}}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Histórico">
                                                                    <i class="fa fa-book" aria-hidden="true"></i>
                                                                </a>

                                                                <a href="{{ route('clientes-processo-camiseiro-endereco-medidas', $c->id)}}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editar">
                                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                                </a>

                                                                <a href="{{ route('clientes-deletar', $c->id) }}" class="btn btn-danger btnDeletar btn-sm" data-toggle="tooltip" data-placement="top" title="Deletar">
                                                                    <i class="fa fa-remove" aria-hidden="true"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
      
            $('.btnDeletar').click(function(e){
                e.preventDefault();
                Swal.fire({
                    title: 'Você tem certeza?',
                    text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Sim!'
                }).then((result) => {
                    if (result.value) {
                        window.location = $(this).attr('href');
                    }
                })
            });


            $('#tabelaClientes').DataTable({
               "processing": true,
               "responsive": true,
               "colReorder": true,
               "searching": true,
               "lengthMenu": [25, 50, 100],
               "deferRender": true,
               "columnDefs": [
                    { "width": "26%", "targets": 0 },
                    { "width": "16%", "targets": 1 },
                    { "width": "16%", "targets": 2 },
                    { "width": "16%", "targets": 3 },
                    { "width": "26%", "targets": 4 },                    
                ],
               "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar: ",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    },
                    "select": {
                        "rows": {
                            "_": "Selecionado %d linhas",
                            "0": "Nenhuma linha selecionada",
                            "1": "Selecionado 1 linha"
                        }
                    }
                }
            });
        });
    </script>
@endsection
