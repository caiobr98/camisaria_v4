@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Fio</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-fios-listar')}}">Fios</a></li>
                        <li class="breadcrumb-item active">Editar Fio</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('produto-fios-editar', $fio->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'nome',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'autocomplete' => 'off',
                                            'value' => $fio->nome
                                        ])
                                    </div>
                                    <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'valor',
                                            'id' => 'txtValor',
                                            'label' => 'Valor',
                                            'class' => 'money',
                                            'autocomplete' => 'off',
                                            'value' => $fio->valor
                                        ])
                                    </div>
                                    <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'peso',
                                            'id' => 'txtPeso',
                                            'label' => 'Peso Total',
                                            'type' => 'number',
                                            'info' => 'Caso tenha dúvidas de como preencher este campo clique no ícone ',
                                            'link' => '#modalWeight',
                                            'autocomplete' => 'off',
                                            'value' => $fio->peso
                                        ])
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <input type="hidden" name="id" value="{{$fio->id}}">
                                <a href="{{route('produto-fios-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                            <div class="modal fade" id="modalWeight" tabindex="-1" role="dialog" aria-labelledby="modalWeightLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="modalWeightLabel">Como calcular o peso total?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>O campo peso será um padrão para o envio do pedido de acordo com o fio, isso por que os correios necessitam dessa informação. Para esse campo, pedimos que seja adicionado a média de peso de uma camisa + o peso da embalagem de envio, sendo todos os valores em gramas.</p>
                                        <p style="font-weight: bold"><i class="fa fa-eye"></i> Exemplo:</p>
                                        <p>Se o peso médio da camisa é de 2000 gramas e a embalagem pesa 130 gramas, o total a ser colocado no campo será de 2130 gramas.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Entendi</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection