@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalhar Fio</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-fios-listar')}}">Fios</a></li>
                        <li class="breadcrumb-item active">Detalhar Fio</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    @include('components.input', [
                                        'name' => 'nome',
                                        'id' => 'txtNome',
                                        'label' => 'Nome',
                                        'autocomplete' => 'off',
                                        'value' => $fio->nome,
                                        'readonly' => true
                                    ])
                                </div>
                                <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'valor',
                                            'id' => 'txtValor',
                                            'label' => 'Valor',
                                            'class' => 'money',
                                            'autocomplete' => 'off',
                                            'value' => $fio->valor,
                                            'readonly' => true
                                        ])
                                </div>
                                <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'peso',
                                            'id' => 'txtPeso',
                                            'label' => 'Peso Total',
                                            'type' => 'number',
                                            'autocomplete' => 'off',
                                            'value' => $fio->peso,
                                            'readonly' => true
                                        ])
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{route('produto-fios-listar')}}" class="btn btn-default">Voltar</a>
                        </div>
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection