@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Base Colarinho</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-base-colarinho-listagem')}}">Base Colarinho</a></li>
                        <li class="breadcrumb-item active">Editar Base Colarinho</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('produto-base-colarinho-editar', $base->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'nome',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'autocomplete' => 'off',
                                            'value' => $base->nome
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'descricao',
                                            'id' => 'txtDescricao',
                                            'label' => 'Descrição',
                                            'autocomplete' => 'off',
                                            'value' => $base->descricao
                                        ])
                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <input type="hidden" name="id" value="{{$base->id}}">
                                <a href="{{route('produto-base-colarinho-listagem')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection