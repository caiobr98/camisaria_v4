@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Pense</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-penses-listar')}}">Penses</a></li>
                        <li class="breadcrumb-item active">Editar Pense</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <form action="{{route('produto-penses-editar', $pense->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                @include('components.input', [
                                    'name' => 'id',
                                    'id' => 'txtId',
                                    'type' => 'hidden',
                                    'value' => $pense->id,
                                    'autocomplete' => 'off'
                                ])
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'nome',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'value' => $pense->nome,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>
                                    <div class="form-group col-md-6">
                                        @include('components.input', [
                                            'name' => 'descricao',
                                            'id' => 'txtDescricao',
                                            'label' => 'Descrição',
                                            'value' => $pense->descricao,
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('produto-penses-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection