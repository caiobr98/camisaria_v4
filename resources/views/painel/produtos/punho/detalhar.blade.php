@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Punho</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-punho-listagem')}}">Punho</a></li>
                        <li class="breadcrumb-item active">Editar Punho</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    @include('components.input', [
                                        'name' => 'nome',
                                        'id' => 'txtNome',
                                        'label' => 'Nome',
                                        'autocomplete' => 'off',
                                        'value' => $punho->nome,
                                        'readonly' => true
                                    ])
                                </div>
                                <div class="form-group col-md-6">
                                    @include('components.input', [
                                        'name' => 'descricao',
                                        'id' => 'txtDescricao',
                                        'label' => 'Descrição',
                                        'autocomplete' => 'off',
                                        'value' => $punho->descricao,
                                        'readonly' => true
                                    ])
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{route('produto-punho-listagem')}}" class="btn btn-default">Volar</a>
                        </div>
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection