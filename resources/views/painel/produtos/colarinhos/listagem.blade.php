@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Colarinhos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active">Colarinhos</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <a href="{{route('produto-colarinhos-cadastrar')}}" class="btn btn-primary btn-sm">Novo Colarinho</a>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th>Opções</th>
                                </tr>
                                @foreach($colarinhos as $c)
                                    <tr>
                                        <td>{{$c->id}}</td>
                                        <td>{{$c->nome}}</td>
                                        <td>{{$c->descricao}}</td>
                                        <td>
                                            <a href="{{route('produto-colarinhos-detalhar', $c->id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Detalhar">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>

                                            <a href="{{route('produto-colarinhos-editar', $c->id)}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>

                                            <a href="{{route('produto-colarinhos-deletar', $c->id)}}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">
                                                <i class="fa fa-remove" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $('.btnDeletar').click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Você tem certeza?',
                text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim!'
            }).then((result) => {
                if (result.value) {
                    window.location = $(this).attr('href');
                }
            })
        });
    </script>
@endsection