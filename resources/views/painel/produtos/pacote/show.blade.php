@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Medidas do Pacote de Envio <i class="fa fa-question-circle" style="cursor: pointer;" data-toggle="modal" data-target="#modalInformation"></i></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-pacote-show')}}">Medidas para envio</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    @foreach($pacote as $p)

                        <form action="{{route('produto-pacote-editar', $p->id)}}" method="POST">
                            <div class="card card-primary card-outline">
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            @include('components.input', [
                                                'name' => 'comprimento',
                                                'id' => 'txtNome',
                                                'label' => 'Comprimento',
                                                'autocomplete' => 'off',
                                                'value' => $p->comprimento,
                                                'info' => 'O comprimento deve ser no mínimo 16 cm'
                                            ])
                                        </div>
                                        <div class="form-group col-md-12">
                                            @include('components.input', [
                                                'name' => 'largura',
                                                'id' => 'txtLargura',
                                                'label' => 'Largura',
                                                'autocomplete' => 'off',
                                                'value' => $p->largura,
                                                'info' => 'O comprimento deve ser no mínimo 11 cm'
                                            ])
                                        </div>
                                        <div class="form-group col-md-12">
                                            @include('components.input', [
                                                'name' => 'altura',
                                                'id' => 'txtAltura',
                                                'label' => 'Altura',
                                                'autocomplete' => 'off',
                                                'value' => $p->altura,
                                                'info' => 'O comprimento deve ser no mínimo 1 cm'
                                            ])
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$p->id}}">
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </div>
                                <div class="modal fade" id="modalInformation" tabindex="-1" role="dialog" aria-labelledby="modalInformationLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalInformationLabel">Como medir o pacote de envio?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>São campos informados para o calculo dos correios.</p>
                                            <p style="font-weight: bold"><i class="fa fa-eye"></i> Ilustração das medidas:</p>
                                            <img src="{{ url('media/images/galeria/produtos/caixa_envio.png') }}" alt="" srcset="" style="width: 100%">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Entendi</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.card -->
                        </form>

                    @endforeach
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection