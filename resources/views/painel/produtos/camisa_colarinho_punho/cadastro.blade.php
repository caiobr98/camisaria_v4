@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Nova Camisa colarinho</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('camisa-colarinho.index')}}">Camisa Colarinho</a></li>
                        <li class="breadcrumb-item active">Nova Camisa colarinho</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 offset-3">
                    @include('components.messages')
                    <form action="{{route('camisa-colarinho.store')}}" method="POST" enctype="multipart/form-data">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nome punho</label>
                                        <select type="text" id="punho" name="punho" class="form-control" required>
                                            <option value="">Selecione</option>
                                            @foreach ($punhos as $p)
                                                <option value="{{ $p->id }}">{{ $p->nome }}</option>
                                            @endforeach
                                        </select>

                                        <label for="">Nome colarinho</label>
                                        <select type="text" id="colarinho" name="colarinho" class="form-control" required>
                                            <option value="">Selecione</option>
                                            @foreach ($colarinhos as $c)
                                                <option value="{{ $c->id }}">{{ $c->nome }}</option>
                                            @endforeach
                                        </select>

                                        <label for="">Foto punho</label>
                                        <input type="file" name="fotoPunho" id="fotoColarinho" class="form-control" accept="image/*">

                                        <label for="">Foto colarinho</label>
                                        <input type="file" name="fotoColarinho" id="fotoColarinho" class="form-control" accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('camisa-colarinho.index')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection