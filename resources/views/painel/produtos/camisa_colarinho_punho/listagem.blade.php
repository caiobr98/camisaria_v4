@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Camisa Colarinho</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active">Camisa Colarinho</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <a href="{{route('camisa-colarinho.create')}}" class="btn btn-primary btn-sm">Nova Camisa Colarinho</a>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Punho</th>
                                    <th>Colarinho</th>
                                    <th>Opções</th>
                                </tr>
                                @foreach ($camisas as $c)
                                    <tr>
                                        <td>{{ $c->id }}</td>
                                        <td>{{ $c->produto_punho['nome'] }}</td>
                                        <td>{{ $c->produto_colarinho['nome'] }}</td>
                                        <td>
                                            <a href="{{ route('camisa-colarinho.edit', $c->id) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>

                                            <form action="{{ route('camisa-colarinho.destroy',$c->id) }}" method="POST" style="display: inline;">
                                                @csrf
                                                @method('DELETE')

                                                <button type="submit" id="btnDeletar" class="btn btn-danger btnDeletar">
                                                    <i class="fa fa-remove" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script type="text/javascript">
    document.querySelector('.btnDeletar').addEventListener('submit', function(e) {
var form = this;
e.preventDefault();
swal({
   title: "¿Estas seguro que deseas eliminar este producto?",
   text: "Si eliminas este producto, no podras recuperarlo",
   type: "warning",
   showCancelButton: true,
   confirmButtonColor: '#DD6B55',
   confirmButtonText: '¡Si, estoy seguro!',
   cancelButtonText: "Cancelar",
   closeOnConfirm: false,
   closeOnCancel: false
},
function(isConfirm) {
   if (isConfirm) {
       swal({
           title: '¡Eliminado!',
           text: 'El producto ha sido eliminado con exito!',
           type: 'success'
       }, function() {
           form.submit();
       });
   } else {
       swal("Cancelado", "Tu producto no ha sido eliminado", "error");
   }
});
});
</script>  
@endsection