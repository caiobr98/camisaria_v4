@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Camisa colarinho</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('camisa-colarinho.index')}}">Camisa colarinho</a></li>
                        <li class="breadcrumb-item active">Editar Camisa colarinho</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 offset-1">
                    @include('components.messages')
                    <form action="{{ route('camisa-colarinho.update', $camisa->id) }}" method="POST" enctype="multipart/form-data">

                        @method('PUT')

                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nome punho</label>
                                        <select type="text" id="punho" name="punho" class="form-control" required>    
                                            <option value="{{ $punho->id }}">{{ $punho->nome }}</option>
                                            @foreach ($punhoList as $p)
                                                @if($punho->id == $p->id)
                                                    <?php continue; ?> 
                                                @endif
                                                <option value="{{ $p->id }}">{{ $p->nome }}</option>
                                            @endforeach
                                        </select>

                                        <label for="">Nome colarinho</label>
                                        <select type="text" id="colarinho" name="colarinho" class="form-control" required>    
                                            <option value="{{ $colarinho->id }}">{{ $colarinho->nome }}</option>
                                            @foreach ($colarinhoList as $c)
                                                @if($colarinho->id == $c->id)
                                                    <?php continue; ?> 
                                                @endif
                                                <option value="{{ $c->id }}">{{ $c->nome }}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                        
                                        @if($camisa->punho_colarinho_foto)
                                            <b>Foto Atual</b> <br>
                                            <img src="{{ url($camisa->punho_colarinho_foto) }}" style="width:300px;" class="img-thumbnail">
                                        @endif
                                        <br><br>
                                        <label for="">Foto punho</label>
                                        <input type="file" name="fotoPunho" id="fotoPunho" class="form-control" accept="image/*">

                                        
                                        @if($camisa->punho_colarinho_foto)
                                            <b>Foto Atual</b> <br>
                                            <img src="{{ url($camisa->punho_colarinho_foto_bolso) }}" style="width:300px;" class="img-thumbnail">
                                        @endif
                                        <br><br>
                                        <label for="">Foto colarinho</label>
                                        <input type="file" name="fotoColarinho" id="fotoColarinho" class="form-control" accept="image/*">
                                        <br>

                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('camisa-colarinho.index')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection