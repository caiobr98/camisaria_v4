@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Novo Tecido</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-tecidos-listar')}}">Tecido</a></li>
                        <li class="breadcrumb-item active">Novo Tecido</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('produto-tecidos-cadastrar-post')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'nome',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'codigo',
                                            'id' => 'txtCodigo',
                                            'label' => 'Código',
                                            'autocomplete' => 'off'
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'id_produto_cor',
                                            'id' => 'txtCor',
                                            'label' => 'Cor',
                                            'autocomplete' => 'off',
                                            'data' => [
                                                'source' => $cores,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'id_produto_classificacao',
                                            'id' => 'txtClassificacao',
                                            'label' => 'Classificação',
                                            'autocomplete' => 'off',
                                            'data' => [
                                                'source' => $classificacoes,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.textarea', [
                                            'name' => 'descricao',
                                            'id' => 'txtDescricao',
                                            'label'=> 'Descrição',
                                            'rows' => 5,
                                            'value' => ''
                                        ])
                                    </div>

                                    <!--div class="form-group col-md-12">
                                        {{-- @include('components.input', [
                                            'name' => 'valor',
                                            'id' => 'txtValor',
                                            'label' => 'Valor',
                                            'class' => 'money',
                                            'autocomplete' => 'off'
                                        ]) --}}
                                    </div-->

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'produto_fio_id',
                                            'id' => 'txtFio',
                                            'label' => 'Fio',
                                            'data' => [
                                                'source' => $fios,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'fornecedor_id',
                                            'id' => 'slcFornecedor',
                                            'label' => 'Fornecedor',
                                            'data' => [
                                                'source' => $fornecedores,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        <input type="file" name="foto">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                {{-- @csrf --}}
                                <a href="{{route('produto-tecidos-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection