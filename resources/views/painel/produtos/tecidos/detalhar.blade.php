@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalhar Tecido</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-tecidos-listar')}}">Tecido</a></li>
                        <li class="breadcrumb-item active">Detalhar Tecido</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    @include('components.input', [
                                        'name' => 'nome',
                                        'id' => 'txtNome',
                                        'label' => 'Nome',
                                        'autocomplete' => 'off',
                                        'value' => $tecido->nome,
                                        'readonly' => true
                                    ])
                                </div>

                                <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'id_produto_cor',
                                            'id' => 'txtCor',
                                            'label' => 'Cor',
                                            'autocomplete' => 'off',
                                            'disabled' => 'disabled',
                                            'selected' => $tecido->id_produto_cor,
                                            'data' => [
                                                'source' => $cores,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                <div class="form-group col-md-12">
                                    @include('components.input', [
                                        'name' => 'descricao',
                                        'id' => 'txtDescricao',
                                        'label' => 'Descricao',
                                        'autocomplete' => 'off',
                                        'value' => $tecido->descricao,
                                        'readonly' => true
                                    ])
                                </div>

                                <!--div class="form-group col-md-12">
                                    @include('components.input', [
                                        'name' => 'valor',
                                        'id' => 'txtValor',
                                        'label' => 'Valor',
                                        'class' => 'money',
                                        'value' => $tecido->valor,
                                        'readonly' => true
                                    ])
                                </div-->

                                <div class="form-group col-md-12">
                                    @include('components.input', [
                                        'name' => 'fio',
                                        'id' => 'txtFio',
                                        'label' => 'Fio',
                                        'autocomplete' => 'off',
                                        'value' => $tecido->fio->nome,
                                        'readonly' => true
                                    ])
                                </div>

                                <div class="form-group col-md-12">
                                    <b>Foto Atual</b>
                                    <img src="{{asset($tecido->foto)}}" class="img-thumbnail">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{route('produto-tecidos-listar')}}" class="btn btn-default">Voltar</a>
                        </div>
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection