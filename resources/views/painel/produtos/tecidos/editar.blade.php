@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Tecido</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active"><a href="{{route('produto-tecidos-listar')}}">Tecido</a></li>
                        <li class="breadcrumb-item active">Editar Tecido</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('produto-tecidos-editar', $tecido->id)}}" method="POST" enctype="multipart/form-data">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'nome',
                                            'id' => 'txtNome',
                                            'label' => 'Nome',
                                            'autocomplete' => 'off',
                                            'value' => $tecido->nome
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'codigo',
                                            'id' => 'txtCodigo',
                                            'label' => 'Código',
                                            'autocomplete' => 'off',
                                            'value' => $tecido->codigo
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'id_produto_cor',
                                            'id' => 'txtCor',
                                            'label' => 'Cor',
                                            'autocomplete' => 'off',
                                            'selected' => $tecido->id_produto_cor,
                                            'data' => [
                                                'source' => $cores,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>
                                    
                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'id_produto_classificacao',
                                            'id' => 'txtClassificacao',
                                            'label' => 'Classificação',
                                            'autocomplete' => 'off',
                                            'selected' => $tecido->id_produto_classificacao,
                                            'data' => [
                                                'source' => $classificacoes,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.textarea', [
                                            'name' => 'descricao',
                                            'id' => 'txtDescricao',
                                            'label'=> 'Descrição',
                                            'rows' => 5,
                                            'value' => $tecido->descricao
                                        ])
                                    </div>

                                    <!--div class="form-group col-md-12">
                                        @include('components.input', [
                                            'name' => 'valor',
                                            'id' => 'txtValor',
                                            'label' => 'Valor',
                                            'class' => 'money',
                                            'value' => $tecido->valor,
                                            'autocomplete' => 'off'
                                        ])
                                    </div-->

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'produto_fio_id',
                                            'id' => 'txtFio',
                                            'label' => 'Fio',
                                            'selected' => $tecido->produto_fio_id,
                                            'data' => [
                                                'source' => $fios,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'id_produto_textura',
                                            'id' => 'txtTextura',
                                            'label' => 'Textura',
                                            'selected' => $tecido->id_produto_textura,
                                            'data' => [
                                                'source' => $texturas,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'fornecedor_id',
                                            'id' => 'slcFornecedor',
                                            'label' => 'Fornecedor',
                                            'selected' => $tecido->fornecedor_id,
                                            'data' => [
                                                'source' => $fornecedores,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @if($tecido->foto)
                                            <b>Foto Atual</b>
                                            <img src="{{ url($tecido->foto) }}" class="img-thumbnail">
                                        @endif
                                        @include('components.inputFile', [
                                            'type' => 'file',
                                            'name' => 'foto',
                                            'id' => 'inputFoto',
                                            'label' => 'Nova Foto'
                                        ])
                                    </div>

                                     <div class="form-group col-md-12">
                                        <input
                                            type="checkbox"
                                            name="status"
                                            id="status"
                                            value="1"
                                            @if (isset($tecido->status) && $tecido->status == 1)
                                                checked
                                            @endif
                                        >
                                        <label for="status">Ativo?</label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('produto-tecidos-listar')}}" class="btn btn-default">Cancelar</a>
                                <input type="hidden" name="id" value="{{$tecido->id}}" />
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection