@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Tecidos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item">Produtos</li>
                        <li class="breadcrumb-item active">Tecidos</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <a href="{{route('produto-tecidos-cadastrar')}}" class="btn btn-primary btn-sm">Novo Tecido</a>
                        </div>

                        <div class="content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div>
                                            <table id="tabelaTecidos" class="table table-condensed table-hover table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Código</th>
                                                        <th>Fio</th>
                                                        <th>Nome</th>
                                                        <th>Status</th>
                                                        <th>Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($tecidos as $t)
                                                        <tr>
                                                            <td>{{ $t->codigo }}</td>
                                                            <td>{{ $t->fio->nome }}</td>
                                                            <td>{{$t->nome}}</td>
                                                            <td>
                                                                @if($t->status == 1)
                                                                Ativo
                                                                @else
                                                                Inativo
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <a href="{{route('produto-tecidos-editar', $t->id)}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">
                                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                                </a>
                                                                <a href="{{route('produto-tecidos-deletar', $t->id)}}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">
                                                                    <i class="fa fa-remove" aria-hidden="true"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>    
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
      
            $('.btnDeletar').click(function(e){
                e.preventDefault();
                Swal.fire({
                    title: 'Você tem certeza?',
                    text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Sim!'
                }).then((result) => {
                    if (result.value) {
                        window.location = $(this).attr('href');
                    }
                })
            });


            $('#tabelaTecidos').DataTable({
               "processing": true,
               "responsive": true,
               "colReorder": true,
               "searching": true,
               "lengthMenu": [25, 50, 100],
               "deferRender": true,
               "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar: ",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    },
                    "select": {
                        "rows": {
                            "_": "Selecionado %d linhas",
                            "0": "Nenhuma linha selecionada",
                            "1": "Selecionado 1 linha"
                        }
                    }
                }
            });
        });
    </script>
@endsection