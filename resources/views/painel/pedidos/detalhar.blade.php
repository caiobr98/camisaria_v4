@extends('layouts.app')

@section('content')

    <div class="invoice p-3 mt-3 container">
        <!-- title row -->
        <div class="row">
            <div class="col-12">
                <h4>
                    <i class="fa fa-globe"></i> <b class=" h2">Pedido #{{ $compra[0]->pedido_id }}</b>
                </h4>
            </div>
            <!-- /.col -->
        </div>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="row invoice-info">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 invoice-col">
                        <br>
                        <br>
                        
                        <address>
                            <strong>Para: </strong>{{ $compra[0]->name }}
                            <br>
                            <strong>Rua: </strong> {{ $compra[0]->rua }}, {{ $compra[0]->numero }}
                            <br>   
                            <strong>cep: </strong> {{ $compra[0]->cep }}
                            <br>
                            <br>
                            <!-- Telefone: (11) 11111-1111<br>
                            Celular: (11) 99999-9999<br> -->
                            <strong>Email: </strong>{{ $compra[0]->email }}
                        </address>
                    </div>
                    <!-- /.col -->
                </div>
            
                <div class="row">
                    <div class="col-8">
                        <div class="table-responsive" style="margin-top: 50px;">
                            <p class="lead">Valores</p>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Subtotal:</th>
                                        @if($cupom == null)
                                            @if($compra[0]->amostra == 'sim')
                                                <td>R$ {{ number_format(($compra[0]->valor_total - ($compra[0]->frete * 3)), 2, ',', '.') }}</td>
                                            @else
                                                <td>R$ {{ number_format(($compra[0]->valor_total - $compra[0]->frete), 2, ',', '.') }}</td>
                                            @endif
                                        @else
                                            @if($compra[0]->amostra == 'sim')
                                                <td>R$ {{ number_format((($compra[0]->valor_total - ($compra[0]->valor_total * ($cupom->desconto_cupom / 100))) - ($compra[0]->frete * 3)), 2, ',', '.') }}</td>
                                            @else
                                                <td>R$ {{ number_format((($compra[0]->valor_total - ($compra[0]->valor_total * ($cupom->desconto_cupom / 100))) - $compra[0]->frete), 2, ',', '.') }}</td>
                                            @endif
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Cupom Utilizado:</th>
                                        @if($cupom == null)
                                            <td>Sem Cupom Utilizado</td>
                                        @else
                                            <td>{{ $cupom->codigo_cupom }}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Amostra:</th>
                                        @if($compra[0]->amostra == 'sim')
                                            <td>{{ number_format(($compra[0]->frete * 2), 2, ',', '.') }}</td>
                                        @else
                                            <td>Não Solicitada</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Desconto:</th>
                                        @if($cupom == null)
                                            <td>0%</td>
                                        @else
                                            <td>{{ $cupom->desconto_cupom }}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Tipo de Entrega:</th>
                                        <td>{{$compra[0]->tipo_frete}}</td>
                                    </tr>
                                    <tr>
                                        <th>Entrega:</th>
                                        <td>R$ {{ number_format($compra[0]->frete, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status Pagamento:</th>
                                        <td>{{ $compra[0]->status_pagamento }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td>R$ {{ number_format($compra[0]->valor_total, 2, ',', '.') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- <div class="row no-print">
                    <div class="col-12">
                        <a onclick="window.print()" target="_blank" class="btn btn-default">
                            <i class="fa fa-print"></i> Imprimir
                        </a>
                    </div>
                </div> -->
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                <div class="row invoice-info">
                    @foreach ($compra as $c) 
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 invoice-col border mb-2 shadow-sm bg-white rounded">
                            <div class="row">
                                <div class="col-1">
                                </div>
                                <div class="col-6 col-sm-6 col-md-5 col-lg-5 col-xl-5 mt-5">
                                    <span><strong class="tamanho-texto-strong">Tecido:</strong> {{ $c->codigo }}</span>
                                    <br>
                                    <br>
                                    <span><strong class="tamanho-texto-strong">Punho:</strong> {{ $c->punhos_nome }}</span>
                                    <br>
                                    <br>
                                    <span><strong class="tamanho-texto-strong">Bolso:</strong> {{ $c->bolso }}</span>
                                </div>
                                <div class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 mt-5 mt-5">
                                    <span><strong class="tamanho-texto-strong">Valor:</strong>R$ {{ number_format($c->fio_valor, 2, ',', '.') }}</span>
                                    <br>
                                    <br>
                                    <span><strong class="tamanho-texto-strong">Colarinho:</strong> {{ $c->colarinhos_nome }}</span>
                                    <br>
                                    <br>
                                    <span>
                                        <strong class="tamanho-texto-strong">Monograma:</strong> {{ $c->monograma }}
                                        <br>
                                        <strong class="tamanho-texto-strong">Local monograma: </strong> {{ $c->local_monograma }}
                                        <br>
                                        <strong class="tamanho-texto-strong">Texto monograma: </strong> {{ $c->texto_monograma }}
                                    </span>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    
                                       
                                    {{-- @if($c->bolso == 'sim') --}}
                                    <img src="{{ asset('/media/images/galeria/moldes/mockup.png') }}" style="width: 220px; height: 220px;" class="img-confeccao divlupa"></span>
                                        
                                    {{-- @else
                                        <img src="{{ asset($c->punho_colarinho_foto) }}" style="width: 220px; height: 220px;" class="img-confeccao divlupa"></span>
                                    @endif --}}

                                </div>
                                <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-5">
                                    <ul>
                                        <li><strong class="tamanho-texto-strong">colarinho: </strong> {{ number_format(intval($c->colarinho),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">costas: </strong> {{ number_format(intval($c->largura_costas),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">punho direito: </strong> {{ number_format(intval($c->punho),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">torax: </strong> {{ number_format(intval($c->torax),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">costas dois: </strong> {{ number_format(intval($c->largura_costas_dois),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">cintura</strong> {{ number_format(intval($c->cintura),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">cava: </strong> {{ number_format(intval($c->cava),1) }}</li>
                                    </ul>
                                </div>
                                <div class="col-4 mt-5">
                                    <ul>
                                        <li><strong class="tamanho-texto-strong">quadril: </strong> {{ number_format(intval($c->quadril),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">manga e: </strong> {{ number_format(intval($c->comp_manga_e),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">punho esquerdo: </strong> {{ number_format(intval($c->comp_total),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">comp. manga: </strong> {{ number_format(intval($c->comp_mana_d),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">biceps: </strong> {{ number_format(intval($c->biceps),1) }}</li>
                                        <li><strong class="tamanho-texto-strong">ombro: </strong> {{ number_format(intval($c->ombro),1) }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row no-print">
            <div class="col-12">
                <a href="{{ route('pedidos-listar') }}" class="btn btn-default" style="margin: 0 15px;">
                    <i class="fa fa-arrow-left"></i> Voltar
                </a>
                <a onclick="window.print()" target="_blank" class="btn btn-primary" style="margin: 0 15px;">
                    <i class="fa fa-print"></i> Imprimir
                </a>
                
                @if($compra[0]->status_pagamento <> 'Cancelado')
                    <a href="{{route('pedidos-processar', $compra[0]->pedido_id)}}" class="btn btn-success" style="margin: 0 15px;">
                        <i class="fa fa-step-forward" aria-hidden="true"></i> Processar Pedido
                    </a>  
                @else 

                @endif
            </div>
        </div>
    </div>

@endsection 
