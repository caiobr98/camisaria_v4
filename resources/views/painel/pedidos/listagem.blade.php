@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Pedidos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Pedidos</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover" id='tabelaPedido'>
                                <thead>
                                    <tr>
                                        <th>Número pedido</th>
                                        <th>Cliente</th>
                                        <th>CPF</th>
                                        <th>Data</th>
                                        <th>Valor total</th>
                                        <th>Status</th>
                                        <th>Amostra</th>
                                        <th>Status pagamento</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pedidos as $p)
                                        <tr>
                                            <td>{{ $p->id }}</td>
                                            <td>{{ $p->user->name }}</td>
                                            <td>{{ $p->cpf }} </td>
                                            <td>{{ $p->created_at->format('d/m/Y') }}</td>
                                            {{--  <td>{{ 'R$ ' . number_format($p->produto_tecido->valor, 2, ',', '.') }}</td>--}}
                                            <td>{{ $p->valor_total != 0 ? 'R$'.number_format($p->valor_total, 2, ',', '.') : NULL }}</td>
                                            <td>{{ $p->status['nome'] }}</td>
                                            <td>{{ $p->amostra }}</td>
                                            <td>{{ $p->status_pagamento }}</td>
                                            @if($p->status_pagamento == 'Confirmado' ) 
                                                <td>
                                                    <a href="{{ route('pedidos-detalhar', $p->id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Detalhar">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>

                                                    <a href="{{route('pedidos-processar', $p->id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Processar Pedido">
                                                        <i class="fa fa-step-forward" aria-hidden="true"></i>
                                                    </a>

                                                    <a href="{{ route('cancelar-pagamento', $p->id) }}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">
                                                        Cancelar compra
                                                    </a>

                                                    {{--<a href="{{route('produto-tecidos-editar', $p->id)}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Editar">--}}
                                                        {{--<i class="fa fa-pencil" aria-hidden="true"></i>--}}
                                                    {{--</a>--}}

                                                    {{--<a href="{{route('produto-tecidos-deletar', $p->id)}}" class="btn btn-danger btnDeletar" data-toggle="tooltip" data-placement="top" title="Deletar">--}}
                                                        {{--<i class="fa fa-remove" aria-hidden="true"></i>--}}
                                                    {{--</a>--}}
                                                </td>   
                                                @elseif($p->status_pagamento == 'Cancelado')
                                                <td>
                                                    <a href="{{ route('pedidos-detalhar', $p->id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Detalhar">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
        
        $('.btnDeletar').click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Você tem certeza?',
                text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim!'
            }).then((result) => {
                if (result.value) {
                    window.location = $(this).attr('href');
                }
            })
        });


        $('#tabelaPedido').DataTable({
            "processing": true,
            "responsive": true,
            "colReorder": true,
            "searching": true,
            "lengthMenu": [25, 50, 100],
            "deferRender": true,
            "columnDefs": [
                { "width": "14,2%", "targets": 0 },
                { "width": "14,2%", "targets": 1 },
                { "width": "14,2%", "targets": 2 },
                { "width": "14,2%", "targets": 3 },
                { "width": "14,2%", "targets": 4 },                    
                { "width": "14,2%", "targets": 5 },
                { "width": "14,2%", "targets": 6 }                 
            ],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar: ",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                }
            }
        });
    });
</script>
@endsection