@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Processar Pedido</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Processar Pedido</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('pedidos-processar', $pedido->id)}}" method="POST" enctype="multipart/form-data">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'camiseiro_id',
                                            'id' => 'slcCamiseiro',
                                            'label' => 'Camiseiro',
                                            'selected' => $pedido->camiseiro_id,
                                            'data' => [
                                                'source' => $camiseiros,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>

                                    <div class="form-group col-md-12">
                                        @include('components.select', [
                                            'name' => 'pedido_status_id',
                                            'id' => 'slcqStatus',
                                            'label' => 'Status',
                                            'selected' => $pedido->pedido_status_id,
                                            'data' => [
                                                'source' => $status,
                                                'value' => 'id',
                                                'label' => 'nome'
                                            ]
                                        ])
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('pedidos-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Processar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection