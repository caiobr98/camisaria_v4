@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Camiseiro</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('camiseiros-listar')}}">Camiseiros</a></li>
                        <li class="breadcrumb-item active">Editar Camiseiro</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('camiseiros-editar', $camiseiro->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                @include('components.input', [
                                    'name' => 'id',
                                    'id' => 'txtId',
                                    'type' => 'hidden',
                                    'value' => $camiseiro->id,
                                    'autocomplete' => 'off'
                                ])

                                @include('components.input', [
                                    'name' => 'nome',
                                    'id' => 'txtNome',
                                    'label' => 'Nome',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->nome
                                ])

                                @include('components.input', [
                                    'name' => 'cpf',
                                    'id' => 'txtCpf',
                                    'label' => 'CPF ou CNPJ',
                                    'class' => 'cpfedit',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->cpf
                                ])

                                @include('components.input', [
                                    'name' => 'rg',
                                    'id' => 'txtRg',
                                    'label' => 'RG',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->rg
                                ])

                                @include('components.input', [
                                    'name' => 'email',
                                    'id' => 'txtEmail',
                                    'label' => 'E-Mail',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->email
                                ])

                                @include('components.input', [
                                    'name' => 'telefone',
                                    'id' => 'txtTelefone',
                                    'label' => 'Telefone',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->telefone
                                ])

                                @include('components.input', [
                                    'name' => 'celular',
                                    'id' => 'txtCelular',
                                    'label' => 'Celular',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->celular
                                ])

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('camiseiros-listar')}}" class="btn btn-default">Cancelar</a>
                                <button type="submit" class="btn btn-primary">Editar</button>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
<script>
$(function(){
    $('.cpfedit').keypress(function(){
        var qtdPalavras = $('.cpfedit').val()

        if(qtdPalavras.length < 14) {
            $('.cpfedit').mask('000.000.000-00');
        } else {
            $('.cpfedit').mask('00.000.000/0000-00');
        }
    })
});
</script>
@endsection