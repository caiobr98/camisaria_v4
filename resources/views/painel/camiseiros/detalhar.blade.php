@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalhar Camiseiro</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('camiseiros-listar')}}">Camiseiros</a></li>
                        <li class="breadcrumb-item active">Detalhar Camiseiro</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    @include('components.messages')
                    <form action="{{route('camiseiros-editar', $camiseiro->id)}}" method="POST">
                        <div class="card card-primary card-outline">
                            <div class="card-body">

                                @include('components.input', [
                                    'name' => 'nome',
                                    'id' => 'txtNome',
                                    'label' => 'Nome',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->nome,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'cpf',
                                    'id' => 'txtCpf',
                                    'label' => 'CPF',
                                    'class' => 'cpf',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->cpf,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'rg',
                                    'id' => 'txtRg',
                                    'label' => 'RG',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->rg,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'email',
                                    'id' => 'txtEmail',
                                    'label' => 'E-Mail',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->email,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'telefone',
                                    'id' => 'txtTelefone',
                                    'label' => 'Telefone',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->telefone,
                                    'readonly' => true
                                ])

                                @include('components.input', [
                                    'name' => 'celular',
                                    'id' => 'txtCelular',
                                    'label' => 'Celular',
                                    'class' => 'telefone',
                                    'autocomplete' => 'off',
                                    'value' => $camiseiro->celular,
                                    'readonly' => true
                                ])

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @csrf
                                <a href="{{route('camiseiros-listar')}}" class="btn btn-default">Voltar</a>
                            </div>
                        </div><!-- /.card -->
                    </form>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection