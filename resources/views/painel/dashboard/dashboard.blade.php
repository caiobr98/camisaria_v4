@extends('layouts.app')

@section('content')

<section class="content">
<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-6 mt-2">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3>{{ $pedidos }}</h3>

          <p>Pedidos</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="{{ route('pedidos-listar') }}" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    
    <!-- ./col -->
    <div class="col-lg-3 col-6 mt-2">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>{{ $user }}</h3>

          <p>Usuários Registrados</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="{{ route('usuario.index') }}" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6 mt-2">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <p style="font-size:25px;">{{ 'R$'.number_format($valor, 2, ',', '.') }}</p>

          <p>Total vendas</p>
        </div>
        <div class="icon">
          <i class="ion ion-cash"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
  </div>

  <div class="row">
    <div class="col-6">
        <div class="card">
          <div class="card-header">
          <div id="columnchart_values_ano" style="min-height: 450px;"></div>
        </div>
      </div>
    </div>
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <div id="columnchart_values_mes" style="min-height: 450px;"></div>
        </div>
      </div>
    </div>
    <div class="col-6">
        <div class="card">
          <div class="card-header">
            <div id="columnchart_values_dias" style="min-height: 450px;"></div>
          </div>
      </div>
    </div>
    <div class="col-6">
        <div class="card">
          <div class="card-header">
            <div id="columnchart_values_semanas" style="min-height: 450px;"></div>
          </div>
        </div>
    </div>
  </div>
</div>





  <!-- /.row -->
  <!-- Main row -->
</section>
@endsection

@section('javascript')
<script src="{{ asset('plugins/daterangepicker/moment.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
  moment.locale('pt-BR');

  graficoAno()
  graficoMes()
  graficoSemanas()
  graficoDias()
  
// grafico Ano
  function graficoAno() {
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Element', 'Quantidade de vendas', { role: 'style' }, { role: 'annotation' } ],
          <?php foreach($vendasAnos as $value) : ?>
            [ <?php echo "'" . $value['ano'] . "'"; ?>, <?= $value['quantidade'] ?>,  "#b87333", <?php echo "'R$ " . $value['lucro'] . "'"; ?>],
          <?php endforeach; ?>
        ]);

        var view = new google.visualization.DataView(data);

        var options = {
          title: "Vendas por ano",
          bar: {groupWidth: "70%"},
          legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values_ano"));
        chart.draw(view, options);
    }
  }
// fim grafico Ano

// grafico Mes
  function graficoMes() {
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Element', 'Quantidade de vendas', { role: 'style' }, { role: 'annotation' } ],
          <?php foreach($vendasMeses as $value) : ?>
            [ moment(<?php echo "'" . $value['mes'] . "'"; ?>).format('MMMM'), <?= $value['quantidade'] ?>,  "green", <?php echo "'R$ " . $value['lucro'] . "'"; ?>],
          <?php endforeach; ?>
        ]);

        var view = new google.visualization.DataView(data);

        var options = {
          title: "Vendas por mês",
          bar: {groupWidth: "70%"},
          legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values_mes"));
        chart.draw(view, options);
    }
  }
// fim grafico Mes

// grafico semanas
  function graficoSemanas() {
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Element', 'Quantidade de vendas', { role: 'style' }, { role: 'annotation' } ],
          <?php foreach($vendasSemanas as $key => $value) : ?>
            [ <?php echo "'" . $value['semana'] . "'"; ?>, <?php echo $value['quantidade']; ?>,  "orange", <?php echo "'R$ " . $value['lucro'] . "'"; ?>],
          <?php endforeach; ?>
        ]);

        var view = new google.visualization.DataView(data);

        var options = {
          title: "Vendas por semanas",
          bar: {groupWidth: "70%"},
          legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values_semanas"));
        chart.draw(view, options);
    }
  }
// fim grafico semanas

// grafico Dias
  function graficoDias() {
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Element', 'Quantidade de vendas', { role: 'style' }, { role: 'annotation' } ],
          <?php foreach($vendasDias as $key => $value) : ?>
            [ moment(<?php echo '\'' . $value->dia . '\'';  ?>).format('dddd'), <?php echo $value->quantidade; ?>,  "orange", <?php echo '\'R$ '. $value->lucro . '\'';  ?>],
          <?php endforeach; ?>
        ]);

        var view = new google.visualization.DataView(data);

        var options = {
          title: "Vendas por Dias",
          bar: {groupWidth: "70%"},
          legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values_dias"));
        chart.draw(view, options);
    }
  }
// fim grafico Dias


</script>
    
@endsection