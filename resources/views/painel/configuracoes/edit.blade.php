@extends('layouts.app')
@section('pageTitle', 'Atividades')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li><a href="{{ url('/admin/painel/configuracoes-log') }}"><i class="fa fa-dashboard"></i> Listagem </a></li>
        <li class="active">Editar</li>
    </ol>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Atividades
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div>

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/admin/configuracoes/log-activity/' . $logactivity->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('painel/configuracoes.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
