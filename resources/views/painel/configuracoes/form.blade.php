<table class="table table-bordered">
    <tr>
        <th>Campos</th>
        @isset($properties['old']) <th>Old</th> @endisset
        <th>New</th>
    </tr>
    @foreach ($properties['attributes'] as $key => $value)
        <tr>
            <th>{{ $key }}</th>

            @isset($properties['old'])
                <td>
                    @if ($key == 'created_at' OR $key == 'updated_at')
                        {{ \App\Utils::formatDateTimeToView($properties['old'][$key]) }}
                    @else
                        {{ $properties['old'][$key] }}
                    @endif
                </td>
            @endisset

            <td
                @isset($properties['old'])
                    @if($properties['old'][$key] != $value)
                        style="background-color: lightblue;"
                    @endif
                @endisset
            >
                @if ($key == 'created_at' OR $key == 'updated_at')
                    {{ \App\Utils::formatDateTimeToView($value) }}
                @else
                    {{ $value }}
                @endif
            </td>
        </tr>
    @endforeach
</table>

<div class="form-group">
    <a class="btn btn-default" href="{{ url()->previous() }}">Voltar </a>
</div>
