@extends('layouts.app')
@section('pageTitle', 'Listagem de Atividades no sistema')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active">Atividades no sistema</li>
    </ol>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Listagem de Atividades do(a) {{ $user }}
                    </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div>
                        <table id="datatable-buttons" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ação</th>
                                    <th>Onde</th>
                                    {{-- <th>Quem</th> --}}
                                    <th>Data</th>
                                    <th>Ferramentas</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($logactivitys as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            @if ($item->description == 'updated') EDITOU @endif
                                            @if ($item->description == 'visualized') VISUALIZOU @endif
                                            @if ($item->description == 'created') CADASTROU @endif
                                            @if ($item->description == 'deleted') DELETOU @endif
                                        </td>
                                        <td>{{ strtoupper(explode("\\", $item->subject_type)[2]) }}</td>
                                        {{-- <td>
                                            
                                                {{$item->properties}}
                                            
                                        </td> --}}

                                        <td>{{ \App\Utils::formatDateTimeToView($item->created_at) }}</td>

                                        <td>
                                            <a href="{{ url('admin/painel/configuracoes-edit/' . $item->id ) }}" title="Editar LogActivity"><button class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Visualizar</button></a>

                                            <!--
                                                <form method="POST" action="{{ url('/admin/configuracoes/log-activity' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Deletar LogActivity" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Deletar</button>
                                                </form>
                                            -->
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <a class="btn btn-default" href="{{ url('admin/painel/configuracoes-log') }}">Voltar </a>
                </div>
            </div>
        </div>
    </div>

@endsection
