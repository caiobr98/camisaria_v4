@extends('layouts.app')
@section('pageTitle', 'Listagem de Logactivity')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home  </a></li>
        <li class="active"> | Atividades</li>
    </ol>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('components.messages')
                    <div class="card card-primary card-outline">
                        <div class="card-body table-responsive p-0">
        <h1>Log Activity Lists</h1>
        <table class="table table-bordered" id="tabelaPedido">
            <thead>
                <tr>
                    <th>No Modificação</th>
                    <th>Quem</th>
                    <th>URL</th>
                    <th>Método</th>
                    <th>Ip</th>
                    <th width="300px">Navegador</th>
                    <th>Usuário</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @if($logs->count())
                    @foreach($logs as $key => $log)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $log->subject }}</td>
                        <td class="text-success">{{ $log->url }}</td>
                        <td><label class="label label-info">{{ $log->method }}</label></td>
                        <td class="text-warning">{{ $log->ip }}</td>
                        <td class="text-danger">{{ $log->agent }}</td>
                        <td>{{ $log->name . ' - ' . $log->user_id  }}</td>
                        <td><button class="btn btn-danger btn-sm">Delete</button></td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div><!-- /.card -->
</div>
<!-- /.col-md-6 -->
</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
    
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
        
        $('.btnDeletar').click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Você tem certeza?',
                text: "Realmente deseja deletar isto? Esta ação não poderá ser desfeita",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim!'
            }).then((result) => {
                if (result.value) {
                    window.location = $(this).attr('href');
                }
            })
        });


        $('#tabelaPedido').DataTable({
            "processing": true,
            "responsive": true,
            "colReorder": true,
            "searching": true,
            "lengthMenu": [25, 50, 100],
            "deferRender": true,
            "columnDefs": [
                { "width": "14,2%", "targets": 0 },
                { "width": "14,2%", "targets": 1 },
                { "width": "14,2%", "targets": 2 },
                { "width": "14,2%", "targets": 3 },
                { "width": "14,2%", "targets": 4 },                    
                { "width": "14,2%", "targets": 5 },
                { "width": "14,2%", "targets": 6 }                 
            ],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar: ",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                }
            }
        });
    });
</script>
@endsection