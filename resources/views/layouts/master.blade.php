<!DOCTYPE html>
<html lang="pt-br">
<head>
  {{-- Google Analytics --}}


  <!-- Google Tag Manager -->
<!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5NWVMSC');</script>

  <!-- End Google Tag Manager -->
 
  {{-- Pixel do Facebook --}}

  

  <!-- Facebook Pixel Code -->

  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '913234575766398');
    fbq('track', 'PageView');
  </script>

  <noscript>
    <img height="1" width="1"
    src="https://www.facebook.com/tr?id=913234575766398&ev=PageView
    &noscript=1"/>
  </noscript>

  <!-- End Facebook Pixel Code -->

  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
  <meta name="author" content="Superteia Soluções para Internet" />
  <meta name="description" content="Camisas Siqueira" />
  <meta name="keywords" content="camisas, camisas siqueira, superteia, tecnologia, aplicativo, desenvolvimento" />
  <link rel="shortcut icon" href="./favicon.ico" />
  <title>Siqueira Camisas</title>

  <link rel="shortcut icon" href="{{ asset('media/images/icones/favicon.ico') }}" type="image/x-icon">
  <link rel="icon" href="{{ asset('media/images/icones/favicon.ico') }}" type="image/x-icon">
  <!-- CSS  -->
  <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.4.2/css/all.css" />
  <link href="{{ asset('media/css/reset.css') }}" rel="stylesheet">
  <link href="{{ asset('media/css/lib/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('media/css/lib/aos.css') }}" rel="stylesheet">
  <link href="{{ asset('media/css/lib/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('media/css/lib/component.css') }}" rel="stylesheet">
  <link href="{{ asset('media/css/lib/galeria.css') }}" rel="stylesheet">
  <link href="{{ asset('media/css/style.min.css') }}" type="text/css" rel="stylesheet" />
  <link href="{{ asset('media/css/crs.css') }}" type="text/css" rel="stylesheet" />

  {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"> --}}

  <noscript><link rel="stylesheet" type="text/css" href="{{ asset('media/css/lib/fallback.css') }}" /></noscript>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- OG Tags -->
  <meta itemprop="name" content="Siqueira Camisas">
  <meta itemprop="description" content="Camisas sob medida">
  <meta itemprop="image" content="//www.superteia.com.br/camisaria-siqueira/media/images/logo.png" >
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@siqueira.camisas">
  <meta name="twitter:title" content="Siqueira Camisas">
  <meta name="twitter:description" content="Siqueira Camisas">
  <meta name="twitter:creator" content="@siqueira.camisas">
  <meta name="twitter:image:src"  content="//www.superteia.com.br/camisaria-siqueira/media/images/logo.png">
  <meta property="og:title" content="Siqueira Camisas" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="http://www.camisariasiqueira.com.br/" />
  <meta property="og:image"  content="//www.superteia.com.br/camisaria-siqueira/media/images/logo.png" />
  <meta property="og:description" content="Camisas sob medida" />
  <meta property="og:site_name" content="Siqueira Camisas" />
  <meta property="article:section" content="Siqueira Camisas" />
  <meta property="article:tag" content="camisas.siqueira" />
  <meta property="fb:admins" content="SiqueiraCamisas-numConta" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body id="wrapper" class="pagina-busca">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NWVMSC"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <div id="app">

      <preloader-component></preloader-component>
      <router-view></router-view>

  </div>

  <!-- JS -->  
  
  <script>
    let user = @json(auth()->user()) !== null ? @json(auth()->user()) : null;
    
    if (typeof user === null) {
      window.user = 0
    } else {
      window.user = user
    }
    
    </script>
  
  
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('media/js/lib/modernizr.custom.26633.js') }}"></script>
  <script src="{{ asset('media/js/lib/jquery.min.js') }}"></script>
  <script src="{{ asset('media/js/lib/jquery.gridrotator.js') }}"></script>
  <script src="{{ asset('media/js/lib/popper.min.js') }}"></script>
  <script src="{{ asset('media/js/lib/bootstrap.min.js') }}"></script>
  <script src="{{ asset('media/js/lib/aos.js') }}"></script>
  <script src="{{ asset('media/js/lib/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('media/js/lib/validator.min.js') }}"></script>
  <script src="{{ asset('media/js/lib/waypoints.min.js') }}"></script>
  <script src="{{ asset('media/js/lib/jquery.counterup.min.js') }}"></script>
  <script src="{{ asset('media/js/lib/maskedinput.js') }}"></script>
  <script src="{{ asset('media/js/lib/jquery.isotope.min.js') }}"></script>
  <script src="{{ asset('media/js/scripts.js') }}"></script>

</body>
</html>
