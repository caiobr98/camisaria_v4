<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{ config('APP_NAME', 'Siqueira Camisas') }}</title>
    
    <link rel="shortcut icon" href="{{ asset('media/images/icones/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('media/images/icones/favicon.ico') }}" type="image/x-icon">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- Tag Input -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-tagsinput.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{ asset('css/bootstrapDataTable.css') }}">

    <link rel="stylesheet" href="{{ asset('css/dataTableBootstrap.min.css') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    
</head>
<body class="hold-transition sidebar-mini">
    
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
         <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            {{--<li class="nav-item d-none d-sm-inline-block">
                <a href="{{route('home')}}" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Contato</a>
            </li>--}}
        </ul> 

        <!-- SEARCH FORM -->
        {{-- <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form> --}}

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            
            <li class="nav-item">
                <a class="nav-link" href="{{route('logout')}}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        Sair <i class="fa fa-sign-out"></i>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                            class="fa fa-th-large"></i></a>
            </li> --}}
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('dashboard')}}" class="brand-link">
            <img src="{{asset('media/images/logo_fake_simbolo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">{{ config('APP_NAME', 'Camisaria') }}</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ Auth::user()->foto ? Auth::user()->foto : asset('media/images/users.png') }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block" title="perfil">{{ Auth::user()->name }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="{{route('dashboard')}}" class="nav-link @if(Route::currentRouteName() === 'dashboard') active @endif">
                            <i class="nav-icon fa fa-th-large"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('clientes-listar')}}" class="nav-link @if(Route::currentRouteName() === 'clientes-listar') active @endif">
                            <i class="nav-icon fa fa-users"></i>
                            <p>
                                Clientes
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('pedidos-listar')}}" class="nav-link @if(Route::currentRouteName() === 'pedidos-listar') active @endif">
                            <i class="nav-icon fa fa-shopping-bag"></i>
                            <p>
                                Pedidos
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('usuario.index')}}" class="nav-link @if(Route::currentRouteName() === 'usuario.index') active @endif">
                            <i class="nav-icon fa fa-user"></i>
                            <p>
                                Usuários
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('usuarios-deletados')}}" class="nav-link @if(Route::currentRouteName() === 'usuarios-deletados') active @endif">
                            <i class="nav-icon fa fa-window-close"></i>
                            <p>
                                Usuários deletados
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('contato-lista')}}" class="nav-link @if(Route::currentRouteName() === 'contato-index') active @endif">
                            <i class="nav-icon fa fa-credit-card"></i>
                            <p>
                                Mensagens
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('camiseiros-listar')}}" class="nav-link @if(Route::currentRouteName() === 'camiseiros-listar') active @endif">
                            <i class="nav-icon fa fa-handshake-o"></i>
                            <p>
                                Camiseiros
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('fornecedores-listar')}}" class="nav-link @if(Route::currentRouteName() === 'fornecedores-listar') active @endif">
                            <i class="nav-icon fa fa-building"></i>
                            <p>
                                Fornecedores
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('produto-pacote-show')}}" class="nav-link @if(Route::currentRouteName() === 'produto-pacote-show') active @endif">
                            <i class="nav-icon fa fa-archive"></i>
                            <p>
                                Medidas de Envio
                            </p>
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="{{route('cupons-listar')}}" class="nav-link @if(Route::currentRouteName() === 'cupons-listar') active @endif">
                            <i class="nav-icon fa fa-ticket"></i>
                            <p>
                                Cupons
                            </p>
                        </a>
                    </li>
                    
                    {{-- @if () --}}
                        <li class="nav-item">
                            <a href="{{url('admin/painel/configuracoes-log')}}" class="nav-link">
                                <i class="nav-icon fa fa-cog"></i>
                                <p>
                                    Configurações
                                </p>
                            </a>
                        </li>
                    {{-- @endif --}}

                    {{-- <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-briefcase"></i>
                            <p>
                                Faturamento
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('produto-caracteristicas-listar')}}" class="nav-link @if(Route::currentRouteName() === 'produto-caracteristicas-listar') active @endif">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Caracteríiticas</p>
                                </a>
                            </li>
                        </ul>
                    </li> --}}


                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-briefcase"></i>
                            <p>
                                Produtos
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('produto-caracteristicas-listar')}}" class="nav-link @if(Route::currentRouteName() === 'produto-caracteristicas-listar') active @endif">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Caracteríiticas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-monogramas-listar')}}" class="nav-link @if(Route::currentRouteName() === 'produto-monogramas-listar') active @endif">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Monograma</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-base-colarinho-listagem')}}" class="nav-link @if(Route::currentRouteName() === 'produto-base-colarinho-listagem') active @endif">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Base Colarinho</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-bolsos-listar')}}" class="nav-link @if(Route::currentRouteName() === 'produto-bolsos-listar') active @endif">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Bolso</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-penses-listar')}}" class="nav-link @if(Route::currentRouteName() === 'produto-penses-listar') active @endif">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Pense</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-punho-listagem')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Punho</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-colarinhos-listar')}}" class="nav-link @if(Route::currentRouteName() === 'produto-colarinhos-listar') active @endif">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Colarinho</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-fios-listar')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Fios</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('camisa-colarinho.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Camisa colarinho</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-cor-listar')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Cor</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-classificacao-listar')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Classificacões</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('produto-tecidos-listar')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Tecidos</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            Tecnologia e Marketing Digital
        </div>
        <!-- Default to the left -->
        <strong>Desenvolvido por <a href="https://superteia.com.br" target="_blank">superteia.com.br</a></strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<script src="{{ asset('js/app.js') }}"></script>
<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- JQuery Mask -->
<script src="{{asset('js/jquery.mask.js')}}"></script>
<!-- Tags Input -->
<script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
<!-- Máscaras prontas -->
<script src="{{asset('js/mascaras.js')}}" defer></script>
<!-- Sweet Alert -->
<script src="{{ asset('js/dataTableBootstrap.min.js') }}"></script>


<script src="{{ asset('js/datatable.min.js') }}"></script>

<script src="{{ asset('dist/js/plugins/sweet-alert2/sweet-alert2.min.js') }}"></script>

<script src="{{ asset('plugins/sparkline/jquery.sparkline.js') }}"></script>

<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('dist/js/plugins/chartjs2/Chart.min.js') }}"></script>





@yield('javascript')
</body>
</html>
