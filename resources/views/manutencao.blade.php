<!-- Esta é apenas uma página temporária que segue a estrutura do site -->
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
  <meta name="author" content="Superteia Soluções para Internet" />
  <meta name="description" content="ScaleWork - Seu novo espaço para coworking" />
  <meta name="keywords" content="scalework, acomodações, superteia, tecnologia, websites, app" />
  <link rel="shortcut icon" href="favicon.ico" />
  <title>ScaleWork - Página Temporária</title>

  <!-- CSS  -->
  <link href="//use.fontawesome.com/releases/v5.4.2/css/all.css" rel="stylesheet" />
  <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
  <link href="https://use.typekit.net/slp4ihh.css" rel="stylesheet" >
  <link href="../media/css/reset.css" rel="stylesheet">
  <link href="../media/css/lib/bootstrap.min.css" rel="stylesheet">
  <link href="../media/css/lib/bootstrap.datetimepicker.css" rel="stylesheet">
  <link href="../media/css/lib/aos.css" rel="stylesheet">
  <link href="../media/css/lib/owl.carousel.min.css" rel="stylesheet">
  <link href="../media/css/lib/scrollbar.css" rel="stylesheet">
  <link href="../media/css/crs.css" type="text/css" rel="stylesheet" />

  <!-- Individual -->
  <link href="../media/css/admin.min.css" type="text/css" rel="stylesheet" />

  <noscript>
    <link rel="stylesheet" type="text/css" href="./media/css/lib/fallback.css" />
  </noscript>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body id="wrapper" style="position:relative">
  <header class="header">
    <div class="container"> </div>
  </header>

  <!-- Conteúdo -->
  <section id="miolo" class="capa miolo v-center" style="background-image:url({{ asset('media/images/internas/capa.jpg') }};">
    <div class="container">
      <div class="hidden-sm-up col-12">
        <a href="../index.html" class="logo" style="width: 420px;max-width:100%;height: 60px;background-position-x:center;margin:auto;"></a>
        <figure class="figura-destaque" style="background-image:url({{ asset('media/images/internas/capa.jpg') }});"></figure>
      </div>
      <div class="row v-100">
        <div class="v-100 col-xl-4 col-lg-4 col-md-5 col-sm-12 offset-xl-8 offset-lg-7 offset-md-6 offset-sm-0 container-login">
          <div class="row">
            <div class="row col-lg-12 col-sm-12">
              <a href="../index.html" class="logo hidden-sm-down" style="width:100%;max-width: 420px;height: 60px;background-position-x:center;margin:auto;"></a>
              <div class="titulo text-center w-100">
                <h2 class="upper" style="font-size:32px;font-weight:bolder;margin-bottom:1rem;">Aguarde</h2>
                <h3 style="font-size:18px;">Em breve traremos novidades!</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- Footer -->
  <footer class="footer-capa">
    <div class="container-fluid">
      <div class="faixa row">
        <div class="col-lg-5 col-md-5 col-sm-12 offset-xl-7 offset-lg-7 offset-md-7 offset-sm-0">
          <span class="copy pull-right">&copy; 2019 Scale Work.</span>
        </div>
      </div>
    </div>
  </footer>

  <!-- JS -->
  <script src="../media/js/lib/modernizr.custom.26633.js"></script>
  <script src="../media/js/lib/jquery.min.js"></script>
  <script src="../media/js/lib/popper.min.js"></script>
  <script src="../media/js/lib/bootstrap.min.js"></script>
  <script src="../media/js/lib/aos.js"></script>
  <script src="../media/js/lib/owl.carousel.min.js"></script>
  <script src="../media/js/lib/validator.min.js"></script>
  <script src="../media/js/lib/maskedinput.js"></script>
  <script src="../media/js/scripts.js"></script>

  <style>
    .titulo { margin:5rem -3rem 5rem 5rem }
    @media screen and (max-width:579px) {
      .figura-destaque { min-height: 180px; }
    }
    @media screen and (max-width:767px) {
      .capa { background-image:none!important; min-height:100%; padding:0!important }
      .titulo { margin: 2rem 15px 2rem 4rem!important; width: 80%; text-align: right!important;}
      section { padding: 2rem 0; }
      .logo { height: 50px!important; margin: 2rem auto!important; width: 100%!important; }
      .copy { display: block; width:100%; text-align: right; margin: 1rem -1rem 1rem auto!important }
      .v-center { align-items: flex-start; }
      .faixa { margin:0!important }
    }
  </style>

  </body>
</html>
