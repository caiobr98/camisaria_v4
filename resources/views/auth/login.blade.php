@extends('layouts.auth')

@section('content')
    <div class="card-body login-card-body">
        <p class="login-box-msg">Faça login para iniciar sua sessão</p>

        <form action="{{route('login')}}" method="post">
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="checkbox icheck">
                        <label>
                            <input name="remember" id="checkbox" type="checkbox" {{ old('remember') ? 'checked' : '' }}> Lembrar-me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-6">
                    @csrf
                    <button type="submit" class="btn btn-outline-secondary btn-block btn-flat">Entrar</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        {{--<div class="social-auth-links text-center mb-3">
            <p>- OU -</p>
            <a href="#" class="btn btn-block btn-primary">
                <i class="fa fa-facebook mr-2"></i> Entrar com Facebook
            </a>
            <a href="#" class="btn btn-block btn-danger">
                <i class="fa fa-google-plus mr-2"></i> Entrar com o Google
            </a>
        </div>
        <!-- /.social-auth-links -->--}}

        <div id="btns-access" class="row">
            <div class="col-12">
                <p class="text-center">
                    <a href="{{ route('password.request') }}"><i class="fa fa-key key-refeito"></i> Esqueci minha senha</a>
                </p>
            </div>
            <div class="col-12">
                <p class="text-center">
                    Você não tem conta? <a href="{{route('register')}}" class="text-center">Registre-se agora!</a>
                </p>
            </div>
        </div>
    </div>
    <!-- /.login-card-body -->
@endsection
