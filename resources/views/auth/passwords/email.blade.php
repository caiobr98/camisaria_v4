@extends('layouts.auth')

@section('content')
    <div class="card-body login-card-body">
        <p class="login-box-msg">Esqueci minha senha</p>

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ url('password/email') }}">
            @csrf

            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="row mb-1">
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-secondary btn-block btn-flat">
                        Solicitar Redefinição de Senha
                    </button>
                </div>
            </div>
        </form>
        <div id="btns-access" class="row">
            <div class="col-12">
                <p class="text-center">
                    <a href="{{route('login')}}" class="text-center">Voltar</a>
                </p>
            </div>
        </div>
    </div>

<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- JQuery Mask -->
<script src="{{asset('js/jquery.mask.js')}}"></script>
<!-- Tags Input -->
<script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
<!-- Máscaras prontas -->
<script src="{{asset('js/mascaras.js')}}" defer></script>
<!-- Sweet Alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
@yield('javascript')
@endsection
