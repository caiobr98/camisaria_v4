<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-cell" align="center">
                    
                    <span class="copy">© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')</span>
                </td>
            </tr>
        </table>
    </td>
</tr>
