<div class="form-group">
    <label for="{{$id}}">{{$label}}</label>

    @php
        $exploded = explode('.', $name);

        if(isset($exploded[1])){
            $inputName = $exploded[0] . '[' . $exploded[1] . ']';
        } else {
            $inputName = $exploded[0];
        }
    @endphp

    <select name="{{$inputName}}" class="form-control @error($name) is-invalid @enderror" id="{{$id}}" required {{ $disabled ?? ''}}>
        <option value="" selected disabled>Selecione</option>
        @if(isset($data['source']) && isset($data['value'])  && isset($data['label']))
            @foreach($data['source'] as $d)
               <option value="{{$d[$data['value']]}}" @isset($selected) @if($selected === $d[$data['value']])) selected @endif @endif>
                    {{$d[$data['label']]}}
                </option>
            @endforeach
        @else
            @foreach($data as $d)
                <option value="{{$d['value']}}" @isset($selected) @if($selected === $d['value']) selected @endif @endif>{{$d['label']}}</option>
            @endforeach
        @endif
    </select>

    @error($name)
        <div class="invalid-feedback">
            <strong>{{ $message }}</strong>
        </div>
    @enderror
</div>