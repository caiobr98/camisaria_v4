<div class="form-group">
    @isset($label)
    <label for="{{$id}}">{{$label}}</label>
    @endisset

    @php
        $exploded = explode('.', $name);

        if(isset($exploded[1])){
            $inputName = $exploded[0] . '[' . $exploded[1] . ']';
        } else {
            $inputName = $exploded[0];
        }
    @endphp

    <input name="{{$inputName}}" {{ $disabled ?? '' }} type="@isset($type){{$type}}@else{{'text'}}@endisset" class="form-control @isset($class) {{$class}} @endisset @error($name) is-invalid @enderror" @isset($value) value="{{$value}}" @else value="{{old($name)}}" @endisset id="{{$id}}" @isset($label) placeholder="{{$label}}" @endisset @isset($autocomplete) autocomplete="{{$autocomplete}}" @endisset @isset($readonly) readonly @endisset @isset($step) step="{{$step}}" @endisset @isset($attributes) {{$attributes}} @endisset>

    @isset($info)
        <small class="form-text text-muted">{{ $info }} @isset($link)
            <i class="fa fa-question-circle" style="cursor: pointer;" data-toggle="modal" data-target="{{ $link }}"></i>
        @endisset</small>
    @endisset
    @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>