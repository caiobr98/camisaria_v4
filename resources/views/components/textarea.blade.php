<div class="form-group">
    @isset($label)
    <label for="{{$id}}">{{$label}}</label>
    @endisset

    @php
        $exploded = explode('.', $name);

        if(isset($exploded[1])){
            $inputName = $exploded[0] . '[' . $exploded[1] . ']';
        } else {
            $inputName = $exploded[0];
        }
    @endphp

    <div class="form-group purple-border">
        <textarea name="{{$inputName}}"  class="form-control @isset($class) {{$class}} @endisset @error($name) is-invalid @enderror" id="{{ $id }}" rows="{{ $rows }}">@isset($value){{ $value }} @else {{ old($value) }}" @endisset</textarea>
      </div>
    
    
    @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>