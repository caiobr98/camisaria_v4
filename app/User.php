<?php

namespace App;

use App\Models\Pagamentos\Pagamento;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Notifications\VerifyEmail;
use App\Notifications\VerifyEmail as AppVerifyEmail;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Notifications\meuResetDeSenha;

class User extends Authenticatable /*implements MustVerifyEmail*/
{
    use HasApiTokens, Notifiable, SoftDeletes, LogsActivity;

    //log mitoso
    protected static $logUnguarded = true;

    protected static $recordEvents = ['deleted', 'updated', 'created'];
    /////////////////////////////
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'bio', 'foto', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function SendEmailVerificationNotification()
    {
        $this->notify(new AppVerifyEmail());
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function cliente(){
        return $this->hasOne('App\Models\Cliente\Cliente');
    }

    public function enderecos(){
        return $this->hasMany('App\Models\Cliente\Endereco');
    }

    public function enderecosOne(){
        return $this->hasOne('App\Models\Cliente\Endereco');
    }

    public function medidas(){
        return $this->hasMany('App\Models\Cliente\Medida');
    }

    public function medidasOne(){
        return $this->hasOne('App\Models\Cliente\Medida');
    }

    public function camiseiro(){
        return $this->hasOne('App\Models\Cliente\ClienteCamiseiro');
    }

    public function historico(){
        return $this->hasMany('App\Models\Cliente\Historico');
    }

    public function historicoOne(){
        return $this->hasOne('App\Models\Cliente\Historico');
    }

    public function pagamento()
    {
        return $this->hasMany(Pagamento::class);
    }
}
