<?php

namespace App;

use App\Models\LogActivity;
use Illuminate\Support\Facades\Auth;

class Utils
{
    static function random($tamanho = 10, $simbolos = RANDOM_ALFANUM) {

        if (!$simbolos) {
            trigger_error('Simbolos invalidos', E_USER_ERROR);
            return false;
        }
        $str = '';
        if ($simbolos) {
            $str .=  'ABCDEFGHJKMNPQRSTUVWXYZ';
        }

        $str = str_shuffle($str);
        $ultimo = strlen($str) - 1;

        $saida = '';
        for ($i = 0; $i < 100000; $i++){
            for ($i = 0; $i < $tamanho; $i++) {
                $saida .= $str[mt_rand(0, $ultimo)];
            }

            $eleitor = Eleitor::select('id')->where('codigo', "$saida")->whereYear('created_at', date('Y'))->first();
            if (!$eleitor)
                break;
        }

        return $saida;
    }

    static function formatDateTimeToBD($datetime, $format = "Y-m-d H:i")
    {

        $data_entrada = $datetime;
        $formato_entrada = "d/m/Y H:i";
        $formato_saida = "Y-m-d H:i";

        try {
            $data = \DateTime::createFromFormat($formato_entrada, $data_entrada);

            $erros = \DateTime::getLastErrors();

            if ($erros['warning_count'] or $erros['error_count']) {
                throw new \Exception("Data inválida: '{$data_entrada}'; a data precisa estar no formato '{$formato_entrada}'."
                    . " Avisos ({$erros['warning_count']}): " . implode(". ", $erros['warnings'])
                    . " Erros ({$erros['error_count']}): " . implode(". ", $erros['errors'])
                );
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }

        $data_saida = $data->format($formato_saida);

        return $data_saida; // yyyy-mm-dd H:i
    }

    static function formatDateTimeToView($datetime, $format = "d/m/Y H:i:s")
    {
        date_default_timezone_set('America/Sao_Paulo');

        if ($datetime != null) {
            $tmp = strtotime($datetime);
            return date($format, $tmp);
        }
    }

    static function formataRF($rf)
    {
        return str_replace('-', '.', $rf);
    }

    static function desformataRF($rf)
    {
        return str_replace('.', '', str_replace('-', '.',$rf));;
    }

    public static function pegaDataNascimentoNucleo($rf, $cpf) {
        //$url = 'https://nucleo.sinpeem.com.br/api/';
        $url = 'http://localhost:8001/api/';
        $request  = $url . 'ws/associado?cpf='.$cpf.'&rf='.$rf;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $request);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($curl);
        curl_close($curl);

        return json_encode($output);

    }

    public static function LogModelo($id, $description, $subjectType, $properties, $authID = null)
    {
        $log = array (
            'attributes' => $properties,
        );

        LogActivity::create([
            'log_name' => 'default',
            'description' => $description,
            'subject_id' => $id,
            'subject_type' => $subjectType,
            'causer_id' => $authID == null ? Auth::user()->id : $authID,
            'causer_type' => User::class,
            'properties' => json_encode($log)
        ]);
    }

}
