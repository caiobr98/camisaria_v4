<?php

namespace App\Repositories\admin;

use App\Models\Cliente\Cliente;
use App\Models\Cliente\Endereco;
use App\User;
use Hash;

class UserRepository
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->authorize('isAdmin');
        if( \Gate::allows('isAdmin'))
            return User::where(['status' => 'ativo'])->latest()->paginate(15);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191|unique:users',
            'type' => 'required|string|max:191',
            'password' => 'required|string|min:6',
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->type,
            'bio' => $request->bio,
            'password' => Hash::make($request->password),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        return User::find($id);
    }

    public function updateProfile( $request, $id)
    {
        $user = auth('api')->user();
        $id = $user->id;

        $validateFields = [
            'cpf' => 'required|string|max:20',
            'name' => 'required|string|max:191',
            'celular' => 'required|string|max:20',
            'email' => 'unique:users,email,'. $user->id
        ];

        /*if($request->has('nova_senha')) {
            $validateFields['senha_atual'] = 'required|string';
            $validateFields['nova_senha'] = 'required|string|confirmed';
        }*/

        $request->validate($validateFields);
        
        $user->name = $request->name;
        $user->email = $request->email;
        // if( $request->password != null )
        //     $user->password = bcrypt($request->password);
        // $user->celular = $request->celular;
        // $user->cpf = $request->cpf;
        $user->save();

        $this->atualizaDadosEndereco($request, $id);
        $this->atualizaDadosCliente($request, $id);

        if( $request->has('nova_senha') ) {
            $this->atualizaSenha($request, $id);
        }

    }

    public function atualizaDadosCliente($request, $id)
    {  
        $dataToUpdate = [
            'user_id' => $id,
            'cpf' => $request->cpf,
            // 'sexo' => '',
            'telefone' => $request->telefone,
            'celular' => $request->celular,
        ];

        $cli = Cliente::updateOrCreate(
            ['user_id' => $id],
            $dataToUpdate
        );

        return $cli;
    }

    public function atualizaSenha($request, $id) {
        if ( $request->nova_senha == $request->nova_senha_confirmation && $request->senha_atual != null) {
            $userPassword = User::where('id', $id)->value('password');
            /*echo $request->senha_atual;
            echo '<br>';
            var_dump($userPassword);
            echo '<br>';*/
            //var_dump(Hash::check($request->senha_atual, $userPassword));
            //die();
            if(Hash::check($request->senha_atual, $userPassword)) {
                $cli = User::updateOrCreate(
                    ['id' => $id],
                    ['password' => Hash::make($request->nova_senha)]
                );

                return $cli;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function atualizaDadosEndereco($request, $id)
    {
        
        $end = Endereco::updateOrCreate(
            ['user_id' => $id],
            [
                'user_id' => $id,
                'cep' => $request->cep,
                'uf' => $request->uf,
                'cidade' => $request->cidade,
                'bairro' => $request->bairro,
                'complemento' => $request->complemento,
                'rua' => $request->rua,
                'numero' => $request->numero,
                'observacoes' => $request->observacoes
            ]
        );

        return $end;
    }

    public function profile()
    {
        $userAuth = auth('api')->user();

        $endereco = Endereco::where('user_id', $userAuth->id)->count();
        $cliente = Cliente::where('user_id', $userAuth->id)->count();
        $users = '';

        if ($endereco > 0 && $cliente > 0) {
            $users = User::join('enderecos', 'enderecos.user_id', 'users.id')->join('clientes', 'clientes.user_id', 'users.id')->where('users.id', $userAuth->id)->first();
        }
        
        elseif ($endereco > 0 ) {
            $users = User::join('enderecos', 'enderecos.user_id', 'users.id')->where('users.id', $userAuth->id)->first();
        }
        
        elseif ($cliente > 0 ) {
            $users = User::join('clientes', 'clientes.user_id', 'users.id')->where('users.id', $userAuth->id)->first();
        }

        else {
            $users = $userAuth;
        }
        
        return $users;   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $request, $id)
    {
        $user = User::findOrFail($id);
        
        $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191|unique:users,email,' . $user->id,
            'type' => 'required|string|max:191',
            'password' => 'sometimes|min:6',
        ]);

        $user->update($request->all());
        return ['message' => 'update'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return ['message' => 'User Deleted'];
    }
    
    public function search()
    {
        if($search = \Request::get('q')){
            $users = User::where(function($query) use($search){
                $query->where('name', 'LIKE', "%$search%")
                ->orWhere('email', 'LIKE', "%$search%")
                ->orWhere('type', 'LIKE', "%$search%");
            })->paginate(20);
        }else {
            $users = User::latest()->paginate(2);
        }

        return $users;
    }

    public function updateProfileImage($photo, $id)
    {   
        define('UPLOAD_DIR', 'img/usuarios/');
        
        $image_parts = explode(";base64,", $photo);
        $image_base64 = base64_decode($image_parts[1]);
        $file_name = UPLOAD_DIR . uniqid() . '.jpeg';
        file_put_contents($file_name, $image_base64);

        User::where('id', $id)->update(['photo' => "/$file_name"]);
        
        //log
        $this->LogSalvarAtividade($id, User::class, $id, "atualizou sua foto");

        return "/$file_name";
    }

    public function inactiveUsers()
    {
        return User::where(['status' => 'inativo'])->latest()->paginate(15);
    }

    public function inactiveUser($id)
    {
        User::where('id', $id)->update(['status' => 'inativo']);

        $user = User::find($id);
        
        //log
        $this->LogSalvarAtividade(auth('api')->user()->id, User::class, $user->id,  "Inativou o usuário $user->name");
    }

    public function showInactiveUser($id)
    {
        return User::find($id);
    }

    public function activeUser($request, $id)
    {
        User::where('id', $id)->update(['status' => 'ativo']);
        
        $user = User::find($id);
        
        //log
        $this->LogSalvarAtividade(auth('api')->user()->id, User::class, $user->id, "Ativou o usuário $user->name");
        
    }
}