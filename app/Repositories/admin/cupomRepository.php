<?php

namespace App\Repositories\admin;

use App\Models\Cupons\Cupom;

class cupomRepository
{

    public function consultar($data)
    {

        $cupom = Cupom::where('codigo_cupom', $data->code)->where('status', '<>', '1')->first();

        if($cupom){
            return array(
                'desconto' => $cupom->desconto_cupom,
                'tipo' => $cupom->tipo,
                'frete' => $cupom->frete,
                'id' => $cupom->id,
                'status' => $cupom->status
            );
        } else {
            return array(
                'desconto' => 0
            );
        }

    }

}