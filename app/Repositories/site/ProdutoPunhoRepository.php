<?php

namespace App\Repositories\site;

use App\Models\Produto\ProdutoPunho;

class ProdutoPunhoRepository
{
    public function index($request)
    {
        $produtoPunho = ProdutoPunho::latest()->paginate(25);

        return $produtoPunho;
    }

    public function store($request)
    {
        $produtoPunho = ProdutoPunho::create($request->all());

        return response()->json($produtoPunho, 201);
    }

    public function show($id)
    {
        $produtoPunho = ProdutoPunho::findOrFail($id);

        return $produtoPunho;
    }

    public function update($request, $id)
    {
        $produtoPunho = ProdutoPunho::findOrFail($id);
        $produtoPunho->update($request->all());

        return response()->json($produtoPunho, 200);
    }

    public function destroy($id)
    {
        ProdutoPunho::destroy($id);

        return response()->json(null, 204);
    }

    public function listaPunhos()
    {
        return ProdutoPunho::all();
    }
}
