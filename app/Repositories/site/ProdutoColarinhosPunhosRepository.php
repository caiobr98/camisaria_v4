<?php

namespace App\Repositories\site;

use App\Models\Produto\ProdutoColarinhosPunhos;

class ProdutoColarinhosPunhosRepository
{
    public function index($request)
    {
        $produtoColarinhosPunhos = ProdutoColarinhosPunhos::latest()->paginate(25);

        return $produtoColarinhosPunhos;
    }

    public function colarinhoPunhoFoto($produto_punhos_id, $produto_colarinhos_id)
    {
        $produtoColarinhosPunhos = ProdutoColarinhosPunhos::where('produto_punhos_id', $produto_punhos_id)
            ->where('produto_colarinhos_id', $produto_colarinhos_id)
            ->first();

        return $produtoColarinhosPunhos;
    }
}