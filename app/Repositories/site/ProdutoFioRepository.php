<?php

namespace App\Repositories\site;

use App\Models\Produto\ProdutoFio;

class ProdutoFioRepository
{
    public function index($request)
    {
        $produtoFio = ProdutoFio::latest()->paginate(25);

        return $produtoFio;
    }

    public function store($request)
    {
        $produtoFio = ProdutoFio::create($request->all());

        return response()->json($produtoFio, 201);
    }

    public function show($id)
    {
        $produtoFio = ProdutoFio::findOrFail($id);

        return $produtoFio;
    }

    public function update($request, $id)
    {
        $produtoFio = ProdutoFio::findOrFail($id);
        $produtoFio->update($request->all());

        return response()->json($produtoFio, 200);
    }

    public function destroy($id)
    {
        ProdutoFio::destroy($id);

        return response()->json(null, 204);
    }

    public function listaFios()
    {
        return ProdutoFio::all();
    }
}
