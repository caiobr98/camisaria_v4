<?php

namespace App\Repositories\site\Pedidos;

use App\Models\ConfeccaosPagamentos;
use App\Models\Pedido\Pedido;
use App\Models\Produto\ProdutoPacote;

class PedidosRepository
{
    public function pedidoByUserId($id)
    {
        $pedido = Pedido::select(
                'pedidos.*',
                'produto_tecidos.nome',
                'confeccao_detalhes.bolso',
                'produto_colarinhos_punhos.punho_colarinho_foto',
                'produto_colarinhos_punhos.punho_colarinho_foto_bolso',
                'endereco_pedidos.rua',
                'endereco_pedidos.cep',
                'endereco_pedidos.numero',
                'pedidos.valor_total',
                'confeccaos_pagamentos.created_at'
            )
            ->leftJoin('confeccaos_pagamentos', 'confeccaos_pagamentos.pedido_id', 'pedidos.id')
            ->leftJoin('confeccaos', 'confeccaos.id', 'confeccaos_pagamentos.confeccaos_id')
            ->leftJoin('endereco_pedidos', 'endereco_pedidos.id', 'pedidos.endereco_pedido_id')
            ->leftJoin('produto_colarinhos_punhos', 'produto_colarinhos_punhos.id', 'confeccaos.produto_colarinhos_punhos_id')
            ->leftJoin('confeccao_detalhes', 'confeccao_detalhes.id', 'confeccaos.confeccao_detalhe_id')
            ->leftJoin('produto_tecidos', 'produto_tecidos.id', 'confeccaos.produto_tecido_id')
            ->where('pedidos.user_id', $id)
            ->groupBy('confeccaos_pagamentos.pedido_id')
            ->orderBy('confeccaos_pagamentos.created_at', 'DESC')
            ->get();

        return $pedido;
    }

    public function pedidoById($userid, $id)
    {
        $compra = ConfeccaosPagamentos::select(
            'users.name',
            'users.email',
            'confeccaos_pagamentos.id',
            'produto_tecidos.nome As tecidos_nome',
            'produto_fios.valor As tecidos_valor',
            'confeccao_detalhes.bolso',
            'confeccao_detalhes.monograma',
            'produto_colarinhos_punhos.punho_colarinho_foto',
            'produto_colarinhos_punhos.punho_colarinho_foto_bolso',
            'endereco_pedidos.rua',
            'endereco_pedidos.cep',
            'pedidos.id AS identificacao_pedido',
            'endereco_pedidos.numero',
            'pedidos.valor_total',
            'pedidos.tipo_frete',
            'pedidos.frete',
            'confeccaos.qtd',
            'produto_punhos.nome As punhos_nome',
            'produto_colarinhos.nome As colarinhos_nome',
            'confeccaomedidas.colarinho',
            'confeccaomedidas.largura_costas',
            'confeccaomedidas.punho',
            'confeccaomedidas.torax',
            'confeccaomedidas.largura_costas_dois',
            'confeccaomedidas.cintura',
            'confeccaomedidas.cava',
            'confeccaomedidas.quadril',
            'confeccaomedidas.comp_manga_e',
            'confeccaomedidas.comp_total',
            'confeccaomedidas.comp_mana_d',
            'confeccaomedidas.biceps',            
            'confeccaomedidas.ombro'
        )
        ->join('pedidos', 'pedidos.id', 'confeccaos_pagamentos.pedido_id')
        ->join('confeccaos', 'confeccaos.id', 'confeccaos_pagamentos.confeccaos_id')
        ->join('endereco_pedidos', 'endereco_pedidos.id', 'pedidos.endereco_pedido_id')
        ->join('produto_colarinhos_punhos', 'produto_colarinhos_punhos.id', 'confeccaos.produto_colarinhos_punhos_id')
        ->join('confeccao_detalhes', 'confeccao_detalhes.id', 'confeccaos.confeccao_detalhe_id')
        ->join('produto_tecidos', 'produto_tecidos.id', 'confeccaos.produto_tecido_id')
        ->join('produto_fios', 'produto_fios.id', 'produto_tecidos.produto_fio_id')
        ->join('produto_punhos', 'produto_punhos.id', 'confeccaos.produto_punho_id')
        ->join('produto_colarinhos', 'produto_colarinhos.id', 'confeccaos.produto_colarinho_id')
        ->join('confeccaomedidas', 'confeccaomedidas.id', 'confeccaos.medida_id')
        ->join('users', 'users.id', 'confeccaos.user_id')
        ->where('confeccaos_pagamentos.pedido_id', $id)
        ->where('users.id', $userid)
        ->get();

        return $compra;
    }

    public function calcFrete($cepDestino, $peso, $comprimento, $largura, $altura, $tipo){

        /*Tabela de codigos
        
        04510 - 'PAC'; 
        40045 - 'SEDEX a Cobrar'; 
        40215 - 'SEDEX 10'; 
        40290 - 'SEDEX Hoje'; 
        04014 - 'SEDEX'; 

        */
        $retornoFrete = array();
        $cepOrigem = 12314567;

        $pesoFormated = str_replace(",", ".", $peso/1000);

        $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=' . $cepOrigem . '&sCepDestino=' . $cepDestino . '&nVlPeso=' . $pesoFormated . '&nCdFormato=1&nVlComprimento=' . $comprimento . '&nVlAltura=' . $altura . '&nVlLargura=' . $largura . '&sCdMaoPropria=n&nVlValorDeclarado=0&sCdAvisoRecebimento=n&nCdServico=' . $tipo . '&nVlDiametro=0&StrRetorno=xml';

        $retornoFrete = simplexml_load_string(file_get_contents($url));
        
        return json_encode($retornoFrete->cServico->Valor);

    }

    public function showMedidas(){
    
        return ProdutoPacote::all()->first();
    }

}