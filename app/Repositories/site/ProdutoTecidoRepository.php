<?php

namespace App\Repositories\site;

use App\Models\Produto\ProdutoTecido;

class ProdutoTecidoRepository
{
    public function index($request)
    {

        $produtoTecido = ProdutoTecido::paginate(15);

        return $produtoTecido;
    }

    public function indexWithFio($id)
    {
        $produtoTecido = ProdutoTecido::where('produto_fio_id', $id)
                        ->where('status', '=', '1')
                        ->paginate(15);

        return $produtoTecido;

    }

    public function store($request)
    {
        $produtoTecido = ProdutoTecido::create($request->all());

        return response()->json($produtoTecido, 201);
    }

    public function show($id)
    {
        $produtoTecido = ProdutoTecido::findOrFail($id);

        return $produtoTecido;
    }

    public function update($request, $id)
    {
        $produtoTecido = ProdutoTecido::findOrFail($id);
        $produtoTecido->update($request->all());

        return response()->json($produtoTecido, 200);
    }

    public function destroy($id)
    {
        ProdutoTecido::destroy($id);

        return response()->json(null, 204);
    }

    public function produtoTecidoCorTexturaPadrao($request)
    {
        
        if ($request->cor != NULL) {
            return ProdutoTecido::where('id_produto_cor', $request->cor)
                                ->where('produto_fio_id', $request->fio)
                                ->where('status', '=', '1')->paginate(15);
        }
        elseif ($request->padrao != NULL) {
            return ProdutoTecido::where('id_produto_cor', $request->padrao)
                                ->where('produto_fio_id', $request->fio)
                                ->where('status', '=', '1')
                                ->paginate(15);
        }
        else {
            return ProdutoTecido::where('produto_fio_id', $request->fio)
                                ->where('status', '=', '1')
                                ->paginate(15);
        }

    }

    public function listaTecidos()
    {
        return ProdutoTecido::all();
    }
}
