<?php

namespace App\Repositories\site;

use App\Models\Confeccaomedida;
use App\Models\Confeccao;
use Illuminate\Support\Facades\DB;

class ConfeccaomedidaRepository
{
    public function index($request)
    {
        //$medidas = Confeccao::join('confeccaomedidas', 'confeccaos.medida_id','confeccaomedidas.id')
        //    ->where('confeccaos.user_id', auth('api')->user()->id)->get();
        $medidas = Confeccaomedida::where('confeccaomedidas.user_id', auth('api')->user()->id)->get();
        //$medidasOld = DB::table('medidas')->where('user_id',auth('api')->user()->id)->get();
        return $medidas;
    }

    public function store($request)
    {
        $medida = Confeccaomedida::create($request->all());
        
        Confeccao::create([
            'user_id' => auth('api')->user()->id,
            'confeccaomedida_id' => $medida->id
        ]);

        return response()->json($medida, 201);
    }

    public function show($id)
    {
        $medida = Confeccaomedida::findOrFail($id);

        return $medida;
    }

    public function update($request, $id)
    {
        
        $medida = Confeccaomedida::findOrFail($id);
        $medida->update($request->all());

        return response()->json($medida, 200);
    }

    public function destroy($id)
    {
        Confeccaomedida::destroy($id);

        return response()->json(null, 204);
    }
}
