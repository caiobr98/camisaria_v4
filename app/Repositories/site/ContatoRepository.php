<?php

namespace App\Repositories\site;

use App\Models\Contato;

class ContatoRepository
{
    public function index($request)
    {
        $colarinho = Contato::latest()->paginate(25);

        return $colarinho;
    }

    public function store($request)
    {
        $colarinho = Contato::create($request->all());

        return response()->json($colarinho, 201);
    }

    public function show($id)
    {
        $colarinho = Contato::findOrFail($id);

        return $colarinho;
    }

    public function update($request, $id)
    {
        $colarinho = Contato::findOrFail($id);
        $colarinho->update($request->all());

        return response()->json($colarinho, 200);
    }

    public function destroy($id)
    {
        Contato::destroy($id);

        return response()->json(null, 204);
    }
}
