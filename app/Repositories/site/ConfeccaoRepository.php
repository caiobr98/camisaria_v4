<?php

namespace App\Repositories\site;

use App\Models\Confeccao;
use App\Models\Confeccaomedida;
use App\Models\Confeccaodetalhe;
use App\Models\Produto\ProdutoFio;
use App\Models\Produto\ProdutoPunho;
use App\Models\Produto\ProdutoColarinho;
use Log;

class ConfeccaoRepository
{
    public function index($request)
    {
        $confeccao = Confeccao::latest()->paginate(25);

        return $confeccao;
    }

    public function store($request)
    {
        $request->validate([
            'id_tecido'=> 'required',              'id_colarinho'=> 'required',     'id_punho'=> 'required',         'bolso'=> 'required',            
            'monograma'=> 'required',    'biceps'=> 'required',           
            'cava'=> 'required',                   'cintura'=> 'required',          'colarinho'=> 'required',        'comp_mana_d'=> 'required',
            'comp_manga_e'=> 'required',           'comp_total'=> 'required',        'largura_costas'=> 'required',
            'largura_costas_dois'=> 'required',    'punho'=> 'required',            'quadril'=> 'required',          'torax'=> 'required',
            'produto_colarinhos_punhos_id',        'local_monograma',               'texto_monograma', 'tipo'=> 'required', 'qtd'
        ]);

        $confeccaoMedidaExistente = Confeccaomedida::where([
            ['user_id', '=', $request->user_id],
            ['colarinho', '=', $request->colarinho],
            ['largura_costas', '=', $request->largura_costas],
            ['punho', '=', $request->punho],
            ['torax', '=', $request->torax],
            ['largura_costas_dois', '=', $request->largura_costas_dois],
            ['busto', '=', $request->busto],
            ['cintura', '=', $request->cintura],
            ['cava', '=', $request->cava],
            ['altura_busto', '=', $request->altura_busto],
            ['quadril', '=', $request->quadril],
            ['comp_manga_e', '=', $request->comp_manga_e],
            ['dist_busto', '=', $request->dist_busto],
            ['comp_total', '=', $request->comp_total],
            ['comp_mana_d', '=', $request->comp_mana_d],
            ['biceps', '=', $request->biceps],
            ['ombro', '=', $request->ombro],
        ])->first();

        if(isset($confeccaoMedidaExistente)) {
            $confeccaomedida = $confeccaoMedidaExistente;
        } else {
            $confeccaomedida = Confeccaomedida::create($request->except(['id_tecido','id_colarinho','id_punho', 'id_fio', 'bolso', 'tipo', 'monograma', 'produto_colarinhos_punhos_id','local_monograma', 'texto_monograma', 'qtd' ]));
        } 


        $confeccao_detalhes = Confeccaodetalhe::create([
            'bolso' => $request->bolso,
            'monograma' => $request->monograma,
            'tipo' => $request->tipo,
            'local_monograma' => $request->local_monograma,
            'texto_monograma'=> $request->texto_monograma
        ]); 
        
        $confeccao = Confeccao::create([
            'user_id' => $request->user_id,
            'medida_id' => $confeccaomedida->id,
            'produto_tecido_id' => $request->id_tecido,
            'produto_fio_id' => $request->id_fio,
            'produto_colarinho_id' => $request->id_colarinho,
            'produto_punho_id' => $request->id_punho,
            'produto_colarinhos_punhos_id' => $request->produto_colarinhos_punhos_id,
            'confeccao_detalhe_id' => $confeccao_detalhes->id,
            'qtd' => $request->qtd
        ]);
            return $request;
        // return response()->json($confeccao, 200);
    }

    public function show($id)
    {
        $confeccao = Confeccao::findOrFail($id);

        return $confeccao;
    }

    public function update($request, $id)
    {
        $confeccao = Confeccao::findOrFail($id);
        $confeccao->update($request->all());

        return response()->json($confeccao, 200);
    }

    public function destroy($id)
    {
        Confeccao::destroy($id);

        return response()->json(null, 204);
    }

    //metodo de pagamento
    public function autorizaTransacao()
    {
        //$acquirer = new Acquirer("10004504", "769a5a598c8c4180b2dfd5b3c7e7f6f3", EnvironmentType::Sandbox);

        // Configuração da loja em modo produção
        //$store = new \Rede\Store('PV', 'TOKEN', \Rede\Environment::production());

        // Configuração da loja em modo sandbox
        $store = new \Rede\Store('10004504', '769a5a598c8c4180b2dfd5b3c7e7f6f3', \Rede\Environment::sandbox());
        //$store = new \Rede\Store('082998051', '6726f0527d8d4e57b18cc3e1693c776b', \Rede\Environment::production());

        // Transação que será autorizada
        $transaction = (new \Rede\Transaction(20.99, 'pedido' . time()))->creditCard(
            '5448280000000007',
            '235',
            '12',
            '2020',
            'John Snow'
        )->capture(false);

        // Autoriza a transação
        $transaction = (new \Rede\eRede($store))->create($transaction);

        if ($transaction->getReturnCode() == '00') {
            echo "Transação autorizada com sucesso; tid=%s\n", $transaction->getTid();
        }
    }

    public function carrinhoByUserId($user_id)
    {
        $confeccao = array();
        $confeccao['confeccao'] = Confeccao::where('user_id', $user_id)
            ->whereNull('finalizou_compra')
            #->with('confeccaomedidas')
            #->with('confeccaodetalhes')
            ->with('fio')
            ->with('tecido')
            ->with('produtoColarinhosPunhos')
            ->with('confeccaodetalhes')
            ->get();
        
        $fios_ids = [];

        $qtd = [];


        foreach ($confeccao['confeccao'] as $value) {
            if(isset($value->fio)){
                $fios_ids[] = $this->formataNumeroParaValores(ProdutoFio::selectRaw('SUM(valor) AS valor')
                    ->where('id', $value->fio->id)
                    ->first()['valor'])
                    *
                Confeccao::where('id', $value->id)->first()['qtd'];
            } else {
                $fios_ids[] = 0;
            }
        }

        $confeccao['subtotal'] = array_sum($fios_ids);
        //$confeccao['totalComFrete'] = array_sum($fios_ids) + 50;

        return $confeccao;
    }

    function formataNumeroParaValores($str_num){
        $resultado = str_replace('.', '', $str_num);
        $resultado = str_replace(',', '.', $resultado);
        return $resultado;
    }

    public function getQuantidade($id){

        $qtd = Confeccao::where('id', $id)->get();

        return $qtd;
    }

    public function reduzirQuantidade($id){
        
        $qtd = Confeccao::where('id', $id)->first()['qtd'] - 1;

        Confeccao::where('id', $id)->update(['qtd' => $qtd]);

        return 'OK';

    }

    public function aumentarQuantidade($id){
        
        $qtd = Confeccao::where('id', $id)->first()['qtd'] + 1;

        Confeccao::where('id', $id)->update(['qtd' => $qtd]);

        return 'OK';
    }

    public function colarinhoPunhoSearch($punho_id, $colarinho_id) {

        $punho = ProdutoPunho::find($punho_id);
        $colarinho = ProdutoColarinho::find($colarinho_id);

        return array(
            'colarinho' => $colarinho,
            'punho' => $punho
        );

    }
}
