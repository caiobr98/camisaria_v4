<?php

namespace App\Repositories\site;

use App\Models\Cliente\Endereco;

class EnderecoRepository
{
    public function index($request)
    {
        $endereco = Endereco::where('user_id', auth('api')->user()->id)->latest()->paginate(25);

        return $endereco;
    }

    public function store($request)
    {
        $endereco = Endereco::create($request->all());

        return response()->json($endereco, 201);
    }

    public function show($id)
    {
        $endereco = Endereco::where('id', $id)->first();

        return $endereco;
    }
    
    public function update($request, $id)
    {
        $endereco = Endereco::findOrFail($id);
        $endereco->update($request->all());
        
        return response()->json($endereco, 200);
    }
    
    public function destroy($id)
    {
        Endereco::destroy($id);
        
        return response()->json(null, 204);
    }
    
    public function cepCorreios($cep)
    {
        return json_decode(file_get_contents('https://viacep.com.br/ws/' . $cep . '/json'), true);
    }

    public function showByUserId($user_id)
    {
        $endereco = Endereco::where('user_id', $user_id)->first();

        return $endereco;
    }
}
