<?php

namespace App\Repositories\site;
use App\Models\Localimagem;

class WelcomeRepository
{
    public function areaDestaque()
    {
        return Localimagem::join('locals', 'localimagems.locals_id', '=', 'locals.id')
        ->selectRaw('max(localimagems.imagem) AS imagem, locals.id, locals.descricao, localimagems.locals_id, localimagems.created_at, locals.bairro, locals.tipo, locals.logradouro, locals.numero, locals.cep, locals.cidade')
        ->groupBy('localimagems.locals_id', 'locals.id','locals.bairro', 'locals.tipo', 'locals.logradouro', 'locals.numero', 'locals.cep', 'locals.cidade', 'localimagems.created_at', 'locals.descricao')
        ->orderBy('locals.id', 'DESC')
        ->limit(5)
        ->get();
    }
}
