<?php

namespace App\Repositories\site;

use App\Models\Minhasmedida;

class MinhasmedidaRepository
{
    public function index($request)
    {
        $medida = Minhasmedida::latest()->paginate(25);

        return $medida;
    }

    public function store($request)
    {
        return $request;
        $medida = Minhasmedida::create(
            [
                'user_id' => $request->user_id,
                'bracos_contorno' => $request->bracos_contorno,
                'bracos_comprimento' => $request->bracos_comprimento,
                'tronco_frente' => $request->tronco_frente,
                'tronco_costas' => $request->tronco_costas,
                'torax_largura' => $request->torax_largura
            ]
        );

        return response()->json($medida, 201);
    }

    public function show($id)
    {
        $medida = Minhasmedida::where('user_id', auth()->user()->id)->latest()->first();

        return $medida;
    }

    public function update($request, $id)
    {
        
        $medida = Minhasmedida::findOrFail($id);
        $medida->update($request->all());

        return response()->json($medida, 200);
    }

    public function destroy($id)
    {
        Minhasmedida::destroy($id);

        return response()->json(null, 204);
    }
}
