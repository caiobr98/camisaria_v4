<?php

namespace App\Repositories\site;

use App\Models\Confeccaodetalhe;

class ConfeccaodetalheRepository
{
    public function index($request)
    {
        $detalhe = Confeccaodetalhe::latest()->paginate(25);

        return $detalhe;
    }

    public function store($request)
    {
        $detalhe = Confeccaodetalhe::create($request->all());

        return response()->json($detalhe, 201);
    }

    public function show($id)
    {
        $detalhe = Confeccaodetalhe::findOrFail($id);

        return $detalhe;
    }

    public function update($request, $id)
    {
        $detalhe = Confeccaodetalhe::findOrFail($id);
        $detalhe->update($request->all());

        return response()->json($detalhe, 200);
    }

    public function destroy($id)
    {
        Confeccaodetalhe::destroy($id);

        return response()->json(null, 204);
    }
}
