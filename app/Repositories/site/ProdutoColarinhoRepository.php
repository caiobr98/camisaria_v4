<?php

namespace App\Repositories\site;

use App\Models\Produto\ProdutoColarinho;

class ProdutoColarinhoRepository
{
    public function index($request)
    {
        $ProdutoColarinho = ProdutoColarinho::latest()->paginate(25);

        return $ProdutoColarinho;
    }

    public function store($request)
    {
        $ProdutoColarinho = ProdutoColarinho::create($request->all());

        return response()->json($ProdutoColarinho, 201);
    }

    public function show($id)
    {
        $ProdutoColarinho = ProdutoColarinho::findOrFail($id);

        return $ProdutoColarinho;
    }

    public function update($request, $id)
    {
        $ProdutoColarinho = ProdutoColarinho::findOrFail($id);
        $ProdutoColarinho->update($request->all());

        return response()->json($ProdutoColarinho, 200);
    }

    public function destroy($id)
    {
        ProdutoColarinho::destroy($id);

        return response()->json(null, 204);
    }

    public function listaColarinhos()
    {
        return ProdutoColarinho::all();
    }
}
