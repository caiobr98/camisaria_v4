<?php

namespace App\Http\Requests\Cupom;


use Illuminate\Foundation\Http\FormRequest;
use App\Models\Cupons\Cupom;

class CupomEditarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $cupom = Cupom::find($this->id)->first();
        
        return [
            'codigo_cupom' => 'required|unique:cupons,codigo_cupom,' . intval($this->id),
            'desconto_cupom' => 'nullable',
            'tipo' => 'required|numeric',
            'frete' => 'nullable',
            'qtd' => 'nullable'
        ];
    }
}