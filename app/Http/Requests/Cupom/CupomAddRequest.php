<?php

namespace App\Http\Requests\Cupom;

use Illuminate\Foundation\Http\FormRequest;

class CupomAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo_cupom' => 'required|unique:cupons',
            'desconto_cupom' => 'nullable',
            'tipo' => 'required|numeric',
            'frete' => 'nullable',
            'qtd' => 'nullable'
        ];
    }
}