<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 10/05/2019
 * Time: 12:16
 */

namespace App\Http\Requests\ProdutoColarinho;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoColarinhoAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:255',
            'colarinho_foto' => 'nullable|file|image'
        ];
    }
}