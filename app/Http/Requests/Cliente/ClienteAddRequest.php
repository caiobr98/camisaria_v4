<?php

namespace App\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class ClienteAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users.name' => 'nullable',
            'users.email' => 'nullable|unique:users',
            'users.password' => 'nullable',
            'clientes.cpf' => 'nullable',
            'clientes.sexo' => 'nullable',
            'clientes.telefone' => 'nullable',
            'clientes.celular' => 'nullable'
        ];
    }
}
