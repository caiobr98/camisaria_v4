<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 09/05/2019
 * Time: 17:09
 */

namespace App\Http\Requests\ProdutoCaracteristica;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoCaracteristicaAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:255'
        ];
    }
}