<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 10/05/2019
 * Time: 11:29
 */

namespace App\Http\Requests\ProdutoPense;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoPenseAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:255'
        ];
    }
}