<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 10/05/2019
 * Time: 11:30
 */

namespace App\Http\Requests\ProdutoPense;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoPenseEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:255'
        ];
    }
}