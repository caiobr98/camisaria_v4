<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 14/05/2019
 * Time: 09:31
 */

namespace App\Http\Requests\ProdutoTecido;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoTecidoAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:155',
            'codigo' => 'required',
            'id_produto_cor' => 'required|numeric',
            //'id_produto_textura' => 'required',
            'id_produto_classificacao' => 'required|numeric',
            'descricao' => 'string|max:200',
            'fornecedor_id' => 'required|numeric',
            'produto_fio_id' => 'required|numeric',
            'foto' => 'nullable|file|image'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'O campo "Nome" é obrigatório',
            'codigo.required' => 'O campo Código é obrigatório',
            'id_produto_cor.required' => 'O campo Cor é obrigatório',
            // 'id_produto_textura.required' => 'O campo Textura é obrigatório',
            'id_produto_classificacao.required' => 'O campo Classificação é obrigatório',
            'produto_fio_id.required' => 'O campo Fio é obrigatório',
            'fornecedor_id.required' => 'O campo Fornecedor é obrigatório',
            'foto.required' => 'O campo Foto é obrigatório'
        ];
    }
}