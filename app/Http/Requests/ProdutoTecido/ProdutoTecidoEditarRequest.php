<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 14/05/2019
 * Time: 10:34
 */

namespace App\Http\Requests\ProdutoTecido;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoTecidoEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'nome' => 'required|string|max:155',
            'codigo' => 'required',
            'id_produto_cor' => 'required|numeric',
            'id_produto_textura' => 'required',
            'id_produto_classificacao' => 'required|numeric',
            'descricao' => 'required|string|max:200',
            'fornecedor_id' => 'required|numeric',
            'produto_fio_id' => 'required|numeric',
            'foto' => 'nullable|file|image',
            'status' => 'nullable',
        ];
    }
}