<?php

namespace App\Http\Requests\ProdutoPacote;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoPacoteEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'comprimento' => 'required',
            'largura' => 'required',
            'altura' => 'required'
        ];
    }
}