<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 09/05/2019
 * Time: 12:36
 */

namespace App\Http\Requests\Medida;


use Illuminate\Foundation\Http\FormRequest;

class MedidaAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required',
            'colarinho' => 'string|max:20',
            'largura_costas' => 'string|max:20',
            'punho' => 'string|max:20',
            'torax' => 'string|max:20',            
            'largura_costas_dois' => 'string|max:20',
            'cintura' => 'string|max:20',
            'cava' => 'string|max:20',
            'quadril' => 'string|max:20',
            'comp_manga_e' => 'string|max:20',
            'comp_total' => 'string|max:20',
            'comp_mana_d' => 'string|max:20',
            'ombro' => 'string|max:20',
            'dist_busto' => 'string|max:20',
            'busto' => 'string|max:20',
            'altura_busto' => 'string|max:20',
            'biceps' => 'string|max:20',
        ];
    }
}