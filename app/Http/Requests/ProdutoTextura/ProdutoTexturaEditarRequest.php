<?php
/**
 * Created by PhpStorm.
 * User: caio
 * Date: 2019-07-06
 * Time: 10:48
 */

namespace App\Http\Requests\ProdutoTextura;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoTexturaEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'nome' => 'required|string|max:155'
        ];
    }
}