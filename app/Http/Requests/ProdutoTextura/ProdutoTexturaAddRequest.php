<?php
/**
 * Created by PhpStorm.
 * User: caio
 * Date: 2019-08-06
 * Time: 09:40
 */

namespace App\Http\Requests\ProdutoTextura;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoTexturaAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:155'
        ];
    }
}