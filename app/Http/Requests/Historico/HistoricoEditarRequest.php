<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 09/05/2019
 * Time: 14:10
 */

namespace App\Http\Requests\Historico;


use Illuminate\Foundation\Http\FormRequest;

class HistoricoEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'codigo_tecido' => 'required',
            'fornecedor_id' => 'required',
            'data_compra' => 'required|date_format:d/m/Y',
        ];
    }
}