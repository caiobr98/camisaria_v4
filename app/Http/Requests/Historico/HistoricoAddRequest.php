<?php

namespace App\Http\Requests\Historico;


use Illuminate\Foundation\Http\FormRequest;

class HistoricoAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required',
            'codigo_tecido' => 'required',
            'fornecedor_id' => 'required',
            'data_compra' => 'required|date_format:d/m/Y',
        ];
    }
}