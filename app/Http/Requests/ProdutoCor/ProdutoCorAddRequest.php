<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-10
 * Time: 15:50
 */

namespace App\Http\Requests\ProdutoCor;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoCorAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:155'
        ];
    }
}