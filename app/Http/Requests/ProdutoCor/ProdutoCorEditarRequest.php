<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-10
 * Time: 16:25
 */

namespace App\Http\Requests\ProdutoCor;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoCorEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'nome' => 'required|string|max:155'
        ];
    }
}