<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 17:01
 */

namespace App\Http\Requests\ProdutoPunho;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoPunhoEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:200',
            'punho_foto' => 'nullable|file|image',
        ];
    }
}