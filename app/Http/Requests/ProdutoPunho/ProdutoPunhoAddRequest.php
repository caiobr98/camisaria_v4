<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 15:26
 */

namespace App\Http\Requests\ProdutoPunho;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoPunhoAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:200',
            'punho_foto' => 'required|file|image',
        ];
    }
}