<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 09/05/2019
 * Time: 17:38
 */

namespace App\Http\Requests\ClienteEndereco;


use Illuminate\Foundation\Http\FormRequest;

class ClienteEnderecoEditarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'user_id' => 'required',
            'cep' => 'nullable',
            'uf' => 'nullable',
            'cidade' => 'nullable',
            'bairro' => 'nullable',
            'rua' => 'nullable',
            'numero' => 'nullable',
            'observacoes' => 'nullable',
            'tipo' => 'required',
        ];
    }
}