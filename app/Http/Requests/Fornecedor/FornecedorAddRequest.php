<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 14/05/2019
 * Time: 09:58
 */

namespace App\Http\Requests\Fornecedor;


use Illuminate\Foundation\Http\FormRequest;

class FornecedorAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'nullable',
            'cpf' => 'nullable',
            'rg' => 'nullable',
            'email' => 'nullable',
            'telefone' => 'nullable',
            'celular' => 'nullable'
        ];
    }
}