<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-10
 * Time: 16:25
 */

namespace App\Http\Requests\ProdutoClassificacao;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoClassificacaoEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'nome' => 'required|string|max:155'
        ];
    }
}