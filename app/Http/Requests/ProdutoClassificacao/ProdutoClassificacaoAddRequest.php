<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-10
 * Time: 15:50
 */

namespace App\Http\Requests\ProdutoClassificacao;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoClassificacaoAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:155'
        ];
    }
}