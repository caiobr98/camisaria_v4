<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 11:14
 */

namespace App\Http\Requests\BaseColarinho;


use Illuminate\Foundation\Http\FormRequest;

class BaseColarinhoEditarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:200',
        ];
    }
}