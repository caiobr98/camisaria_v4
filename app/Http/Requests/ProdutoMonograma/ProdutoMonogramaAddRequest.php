<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 10/05/2019
 * Time: 09:50
 */

namespace App\Http\Requests\ProdutoMonograma;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoMonogramaAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string|max:255'
        ];
    }
}