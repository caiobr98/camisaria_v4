<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 13/05/2019
 * Time: 14:25
 */

namespace App\Http\Requests\Camiseiro;


use Illuminate\Foundation\Http\FormRequest;

class CamiseiroAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'nullable',
            'cpf' => 'nullable',
            'rg' => 'nullable',
            'email' => 'nullable',
            'telefone' => 'nullable',
            'celular' => 'nullable'
        ];
    }
}