<?php
/**
 * Created by PhpStorm.
 * User: alisonbruno
 * Date: 13/05/2019
 * Time: 14:27
 */

namespace App\Http\Requests\Camiseiro;


use Illuminate\Foundation\Http\FormRequest;

class CamiseiroEditarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'nome' => 'nullable',
            'cpf' => 'nullable',
            'rg' => 'nullable',
            'email' => 'nullable',
            'telefone' => 'nullable',
            'celular' => 'nullable'
        ];
    }
}