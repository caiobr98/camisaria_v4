<?php

namespace App\Http\Requests\Confeccao;


use Illuminate\Foundation\Http\FormRequest;

class ConfeccaoAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // 'id_tecido' => 'required|numeric',
            // 'id_colarinho' => 'required|numeric',
            // 'id_punho' => 'required|numeric',
            // 'bolso' => 'required',
            // 'monograma' => 'required',
            // 'altura_busto' => 'required',
            // 'biceps' => 'required',
            // 'busto' => 'required',
            // 'cava' => 'required',
            // 'cintura' => 'required',
            // 'colarinho' => 'required',
            // 'comp_mana_d' => 'required',
            // 'comp_manga_e' => 'required',
            // 'comp_total' => 'required',
            // 'dist_busto' => 'required',
            // 'largura_costas' => 'required',
            // 'largura_costas_dois' => 'required',
            // 'punho' => 'required',
            // 'quadril' => 'required',
            // 'torax' => 'required',
        ];
    }
}