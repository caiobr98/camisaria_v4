<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 13/05/2019
 * Time: 10:52
 */

namespace App\Http\Requests\ProdutoFio;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoFioAddRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|string|max:155',
            'valor' => 'required',
            'peso' => 'required|integer|max:10000'
        ];
    }
}