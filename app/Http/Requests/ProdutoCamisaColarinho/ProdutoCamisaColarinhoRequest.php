<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 17:01
 */

namespace App\Http\Requests\ProdutoCamisaColarinho;


use Illuminate\Foundation\Http\FormRequest;

class ProdutoCamisaColarinhoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'produto_punhos_id' => 'required|numeric',
            'produto_colarinhos_id' => 'required|numeric',
            'punho_colarinho_foto' => 'required|file|image',
            'punho_colarinho_foto_bolso' => 'required|file|image',
        ];
    }
}