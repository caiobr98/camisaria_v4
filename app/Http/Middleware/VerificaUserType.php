<?php

namespace App\Http\Middleware;

use Closure;

class VerificaUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userType = auth()->user()->type ?? 'cliente';
        if($userType == 'cliente'){
            return redirect('/');
        }
        return $next($request);
    }
}
