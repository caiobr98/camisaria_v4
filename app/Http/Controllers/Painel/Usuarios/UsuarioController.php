<?php

namespace App\Http\Controllers\Painel\Usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Cliente\Cliente;
use Notification;
use App\Notifications\PostPedido;
use App\Helpers\LogActivity;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('painel.users.listar', [
            'users' => $user
        ]);
    }

    public function listaDeletados()
    {
        $user = User::onlyTrashed()->get();
        return view('painel.users.listarDeletados', [
            'users' => $user
        ]);
    }

    public function recuperar($id)
    {
        User::where('id', $id)->restore();
        Cliente::where('user_id', $id)->restore();
        LogActivity::addToLog('Usuário recuperado. Id do usuário = ' . $id);
        return redirect()->route('usuarios-deletados');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        
        return view('painel.users.editar',[
            'user' => $user
        ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $email = User::where('email', '=', $request->email)->first();
    
        if($request->hasFile('foto')){
            if ($request->file('foto')->isValid()) {
                $imageName = time().'.'.$request->foto->getClientOriginalExtension();
                $request->foto->move(public_path('/img/users'), $imageName);
                $imagePath = '/img/users/' . $imageName;
            }

            if(!$email && $request->email != $user->email) {
                $user->update([
                    'name' => $request->nome,
                    'email' => $request->email,
                    'foto' => $imagePath
                ]);

                LogActivity::addToLog('Dados do usuário alterados. Id do usuário = ' . $id);
                    
                return back()->with('success','Dados atualizados.');
            }elseif(!isset($request->email)){
                return back()->with('error','Preencha o campo e-mail.');
            }
    
            if($user->email === $request->email) {
                $user->update([
                    'name' => $request->nome,
                    'foto' => $imagePath
                ]);

                LogActivity::addToLog('Nome e/ou foto do usuário alterado(s). Id do usuário = ' . $id);

                return back()->with('success','Dados atualizados.');
            }else{
                return back()->with('error','Erro ao atualizar.');
            }
        } else {
            if(!$email && $request->email != $user->email) {
                $user->update([
                    'name' => $request->nome,
                    'email' => $request->email,
                ]);

                LogActivity::addToLog('Nome e/ou e-mail do usuário alterado(s). Id do usuário = ' . $id);
                            
                return back()->with('success','Dados atualizados.');
            }elseif(!isset($request->email)){
                return back()->with('error','Preencha o campo e-mail.');
            }
    
            if($user->email === $request->email) {
                $user->update([
                    'name' => $request->nome,
                ]);

                LogActivity::addToLog('Nome do usuário alterado. Id do usuário = ' . $id);

                return back()->with('success','Dados atualizados.');
            }else{
                return back()->with('error','Erro ao atualizar.');
            }
        }
        
    }   

    public function editarSenha(Request $request, $id)
    {
        $user = User::find($id);

        if($request->password2 == $request->password && $request->password != null) { 
            $user->password = bcrypt($request->password);
            $user->save();

            LogActivity::addToLog('Senha de usuário alterada. Id do usuário = ' . $id);

            return back()->with('success', 'Senha alterada.'); 
        } else {
            return back()->with('error', 'Senhas não conferem .'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
