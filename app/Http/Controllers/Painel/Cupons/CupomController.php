<?php

namespace App\Http\Controllers\Painel\Cupons;

use App\Http\Requests\Cupom\CupomAddRequest;
use App\Http\Requests\Cupom\CupomEditarRequest;
use App\Models\Cupons\Cupom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class CupomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $cupom = Cupom::all();
            
        return view('painel.cupons.listagem', [
            'cupom' => $cupom
        ]);
    }

    public function frmCadastrar(){

        return view('painel.cupons.cadastro');
    }

    public function cadastrar(CupomAddRequest $request){

        $cupom = $request->validated();
        
        $cupomNovo = Cupom::create([
            'codigo_cupom' => strtoupper($cupom['codigo_cupom']),
            'desconto_cupom' => isset($cupom['desconto_cupom']) ? $cupom['desconto_cupom'] : 0,
            'tipo' => isset($cupom['tipo']) ? $cupom['tipo'] : 0,
            'frete' => $cupom['frete'],
            'qtd' => isset($cupom['qtd']) ? $cupom['qtd'] : 1,
            'status' => 0
        ]);

        LogActivity::addToLog('Cupom criado. Id do cupom = ' . $cupomNovo->id);

        return redirect()->route('cupons-listar')->with('success', 'Cupom criada com sucesso.');

    }

    public function frmEditar($id){

        $cupom = Cupom::find($id);

        return view('painel.cupons.editar', [
            'cupom' => $cupom
        ]);
    }

    public function editar($id, CupomEditarRequest $request){

        $dados = $request->validated();
        
        $cupom = Cupom::where('id', $id)->update([
            'codigo_cupom' => strtoupper($dados['codigo_cupom']),
            'desconto_cupom' => $dados['desconto_cupom'],
            'frete' => $dados['frete'],
            'tipo' => isset($dados['tipo']) ? $dados['tipo'] : 0,
            'qtd' => isset($dados['qtd']) ? $dados['qtd'] : 1,
        ]);

        if($cupom){
            LogActivity::addToLog('Cupom editado. Id do cupom = ' . $id);
            return back()->with('success', 'Cupom editada com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o cupom.');
    }

    public function detalhar($id){

        $cupom = Cupom::find($id);

        return view('painel.cupons.detalhar', [
            'cupom' => $cupom
        ]);
    }

    public function deletar($id){

        $cupom = Cupom::find($id)->delete();

        if($cupom){
            LogActivity::addToLog('Cupom deletado. Id do cupom = ' . $id);
            return redirect()->route('cupons-listar')->with('success', 'Cupom excluído com sucesso.');
        }

        return back()->with('error', 'Erro ao excluir o cupom.');
    }
}
