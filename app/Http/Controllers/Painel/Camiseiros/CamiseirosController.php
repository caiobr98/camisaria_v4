<?php

namespace App\Http\Controllers\Painel\Camiseiros;

use App\Http\Requests\Camiseiro\CamiseiroAddRequest;
use App\Http\Requests\Camiseiro\CamiseiroEditarRequest;
use App\Models\Camiseiro\Camiseiro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CamiseirosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $camiseiros = Camiseiro::all();

        return view('painel.camiseiros.listagem', [
            'camiseiros' => $camiseiros
        ]);
    }

    public function formCadastro(){
        return view('painel.camiseiros.cadastro');
    }

    public function cadastrar(CamiseiroAddRequest $request){
        $dados = $request->validated();
        $camiseiro = Camiseiro::create($dados);

        if($camiseiro->exists){
            return redirect()->route('camiseiros-listar')->with('success', 'Camiseiro criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar o camiseiro.');
    }

    public function formEditar(Camiseiro $camiseiro){
        return view('painel.camiseiros.editar', [
            'camiseiro' => $camiseiro
        ]);
    }

    public function editar(CamiseiroEditarRequest $request){
        $dados = $request->validated();

        $camiseiro = Camiseiro::where('id', $dados['id'])->update($dados);

        if($camiseiro){
            return redirect()->route('camiseiros-listar')->with('success', 'Camiseiro editado com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o camiseiro.');
    }

    public function detalhar(Camiseiro $camiseiro){
        return view('painel.camiseiros.detalhar', [
            'camiseiro' => $camiseiro
        ]);
    }

    public function deletar(Camiseiro $camiseiro){
        $c = $camiseiro->delete();

        if($c){
            return back()->with('success', 'Camiseiro excluído com sucesso.');
        }

        return back()->with('error', 'Erro ao excluir o camiseiro.');
    }
}
