<?php

namespace App\Http\Controllers\Painel\Produtos;

use App\Http\Controllers\Controller;
use App\Models\Produto\CamisaColarinhoPunho;
use App\Models\Produto\ProdutoColarinho;
use App\Models\Produto\ProdutoPunho;
use Illuminate\http\Request;
use App\Helpers\LogActivity;

class CamisaColarinhoPunhoController extends Controller
{
    public function index()
    {
        $camisas = CamisaColarinhoPunho::all();
        return view('painel.produtos.camisa_colarinho_punho.listagem',[
            'camisas' => $camisas
        ]);
    }
  
    public function create()
    {
        $punhos = ProdutoPunho::all();
        $colarinhos = ProdutoColarinho::all();

        return view('painel.produtos.camisa_colarinho_punho.cadastro', [
            'punhos' => $punhos,
            'colarinhos' => $colarinhos
        ]);
    }
  
    public function store(Request $request)
    {
        if($request->hasFile('fotoPunho') AND $request->hasFile('fotoColarinho')){
            if ($request->file('fotoPunho')->isValid() AND $request->file('fotoColarinho')->isValid()) {
                $imageName = time().'.png';
                $imageName2 = time().'_bolso.png';
                $request->fotoPunho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName);
                $request->fotoColarinho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName2);
                $imagePath = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName;
                $imagePath2 = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName2;
                
                $camisa = new CamisaColarinhoPunho;
                $camisa->produto_punhos_id             = $request->punho;
                $camisa->produto_colarinhos_id         = $request->colarinho;
                $camisa->punho_colarinho_foto          = $imagePath;
                $camisa->punho_colarinho_foto_bolso    = $imagePath2;
                $camisa->save();

                LogActivity::addToLog('Criou camisa colarinho de id = ' . $camisa->id);

                return back()->with('success', 'Camisa colarinho criado com sucesso.');
            }
        } else {
            $camisa = new CamisaColarinhoPunho;
            $camisa->produto_punhos_id             = $request->punho;
            $camisa->produto_colarinhos_id          = $request->colarinho;
            $camisa->save();

            LogActivity::addToLog('Criou camisa colarinho de id = ' . $camisa->id);

            return back()->with('success', 'Camisa colarinho criado com sucesso.');
        }    
    }

    public function show($id)
    {
        //
    }
  
    public function edit($id)
    {
        $camisa = CamisaColarinhoPunho::findOrFail($id);        
        $punho = ProdutoPunho::where('id', $camisa->produto_punhos_id)->first();
        $colarinho = ProdutoColarinho::where('id', $camisa->produto_colarinhos_id)->first();

        return view('painel.produtos.camisa_colarinho_punho.editar', [
            'camisa' => $camisa,
            'punhoList' => ProdutoPunho::all(),
            'colarinhoList' => ProdutoColarinho::all(),
            'punho' => $punho,
            'colarinho' => $colarinho
        ]);
    }
  
    public function update(Request $request, $id) 
    {
    
        $camisa = CamisaColarinhoPunho::findOrFail($id); 
        
        if($request->hasFile('fotoPunho') AND $request->hasFile('fotoColarinho')) {
            if($request->file('fotoPunho')->isValid() AND $request->file('fotoColarinho')->isValid()) {
                $imageName = time().'.'.$request->fotoPunho->getClientOriginalExtension();
                $imageName2 = time().'_bolso.' . $request->fotoColarinho->getClientOriginalExtension();
                $request->fotoPunho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName);
                $request->fotoColarinho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName2);
                $imagePath = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName;
                $imagePath2 = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName2;
                       
                $camisa->update([
                    'produto_punhos_id'             => $request->punho,
                    'produto_colarinhos_id'         => $request->colarinho,
                    'punho_colarinho_foto'          => $imagePath,
                    'punho_colarinho_foto_bolso'    => $imagePath2
                ]);
    
            }

            LogActivity::addToLog('Editou colarinho_punho de id = ' . $id);
            return back()->with('success', 'Registro atualizado com sucesso.');
        } elseif(!$request->hasFile('fotoPunho') AND !$request->hasFile('fotoColarinho')) {
            $camisa->update([
                'produto_punhos_id'         => $request->punho,
                'produto_colarinhos_id'     => $request->colarinho,
                'produto_punhos_id'         => $request->punho,
                'produto_colarinhos_id'     => $request->colarinho
            ]);

            LogActivity::addToLog('Editou colarinho_punho de id = ' . $id);
            return back()->with('success', 'Registro atualizado com sucesso.');
        }   
        
        if($request->punho == $camisa->produto_punhos_id AND $request->colarinho == $camisa->produto_colarinhos_id) {
            if($request->hasFile('fotoPunho') AND $request->hasFile('fotoColarinho')){
                if ($request->file('fotoPunho')->isValid() AND $request->file('fotoColarinho')->isValid()) {
                    $imageName = time().'.'.$request->fotoPunho->getClientOriginalExtension();
                    $imageName2 = time().'_bolso.' . $request->fotoColarinho->getClientOriginalExtension();
                    $request->fotoPunho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName);
                    $request->fotoColarinho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName2);
                    $imagePath = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName;
                    $imagePath2 = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName2;
                    
                    $camisa->update([
                        'produto_punhos_id'             => $request->punho,
                        'produto_colarinhos_id'         => $request->colarinho,
                        'punho_colarinho_foto'          => $imagePath,
                        'punho_colarinho_foto_bolso'    => $imagePath2
                    ]);
                    
                    LogActivity::addToLog('Editou colarinho_punho de id = ' . $id);
                    return back()->with('success', 'Registro atualizado com sucesso.'); 
                } 
            
            } elseif($request->hasFile('fotoPunho')){
                if ($request->file('fotoPunho')->isValid()) {
                    $imageName = time().'.'.$request->fotoPunho->getClientOriginalExtension();
                    $request->fotoPunho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName);
                    $imagePath = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName;
                    
                    $camisa->update([
                        'punho_colarinho_foto'       => $imagePath
                    ]);
                    
                    LogActivity::addToLog('Editou colarinho_punho de id = ' . $id);
                    return back()->with('success', 'Registro atualizado com sucesso.'); 
                }
            } elseif($request->hasFile('fotoColarinho')){
                if ($request->file('fotoColarinho')->isValid()) {
                    $imageName2 = time().'.'.$request->fotoColarinho->getClientOriginalExtension();
                    $request->fotoColarinho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName2);
                    $imagePath2 = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName2;
                    
                    $camisa->update([
                        'produto_punhos_id'             => $request->punho,
                        'produto_colarinhos_id'         => $request->colarinho,
                        'punho_colarinho_foto_bolso'    => $imagePath2
                    ]);

                    LogActivity::addToLog('Editou colarinho_punho de id = ' . $id);
                    return back()->with('success', 'Registro atualizado com sucesso.'); 
                }
            }
        }

        if($request->hasFile('fotoPunho')) {
            if($request->hasFile('fotoPunho')){
                if ($request->file('fotoPunho')->isValid()) {
                    $imageName = time().'.'.$request->fotoPunho->getClientOriginalExtension();
                    $request->fotoPunho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName);
                    $imagePath = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName;
                    $camisa->update([
                        'produto_punhos_id'             => $request->punho,
                        'produto_colarinhos_id'         => $request->colarinho,
                        'punho_colarinho_foto'          => $imagePath
                    ]);
                    
                    LogActivity::addToLog('Editou colarinho_punho de id = ' . $id);
                    return back()->with('success', 'Registro atualizado com sucesso.'); 
                } 
            }
        }elseif($request->hasFile('fotoColarinho')){
            if($request->hasFile('fotoColarinho')){
                if ($request->file('fotoColarinho')->isValid()) {
                    $imageName2 = time().'.'.$request->fotoColarinho->getClientOriginalExtension();
                    $request->fotoColarinho->move(public_path('media/images/galeria/moldes/camisa_colarinho_punho'), $imageName2);
                    $imagePath2 = 'media/images/galeria/moldes/camisa_colarinho_punho/' . $imageName2;
                    $camisa->update([
                        'produto_punhos_id'             => $request->punho,
                        'produto_colarinhos_id'         => $request->colarinho,
                        'punho_colarinho_foto_bolso'    => $imagePath2
                    ]);
                    
                    LogActivity::addToLog('Editou colarinho_punho de id = ' . $id);
                    return back()->with('success', 'Registro atualizado com sucesso.'); 
                } 
            }
        }
    }
  
    public function destroy($id)
    {
        CamisaColarinhoPunho::destroy($id);
        LogActivity::addToLog('Deletou camisa colarinho punho de id = ' . $id);
        return back()->with('success','Item deletado com sucesso.');
    }
}
