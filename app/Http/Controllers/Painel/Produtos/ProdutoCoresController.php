<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-10
 * Time: 14:48
 */

namespace App\Http\Controllers\Painel\Produtos;


use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoCor\ProdutoCorAddRequest;
use App\Http\Requests\ProdutoCor\ProdutoCorEditarRequest;
use App\Models\Produto\ProdutoCor;
use App\Helpers\LogActivity;

class ProdutoCoresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        return view('painel.produtos.cores.listagem', [
            'cores' => ProdutoCor::all()
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.cores.cadastro');
    }

    public function cadastrar(ProdutoCorAddRequest $request){
        $data = $request->validated();
        $r = ProdutoCor::create($data);

        if($r->exists){
            LogActivity::addToLog('Criou cor de id = ' . $r->id);
            return redirect()->route('produto-cor-listar')->with('success', 'Cor cadastrada com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar a cor');
    }

    public function formEditar(ProdutoCor $cor){
        return view('painel.produtos.cores.editar')->with([
            'cor' => $cor
        ]);
    }

    public function editar(ProdutoCorEditarRequest $request){
        $data = $request->validated();

        $r = ProdutoCor::where('id', $data['id'])->update($data);

        if($r){
            LogActivity::addToLog('Editou cor de id = ' . $data['id']);
            return redirect()->route('produto-cor-listar')->with('success', 'Cor editada com sucesso');
        }

        return back()->with('error', 'Erro ao editar a cor');
    }

    public function detalhar(ProdutoCor $cor){
        return view('painel.produtos.cores.detalhar', [
            'cor' => $cor
        ]);
    }

    public function deletar(ProdutoCor $cor){
        $r = $cor->delete();

        if($r){
            LogActivity::addToLog('Excluiu cor de id = ' . $cor->id);
            return back()->with('success', 'Cor excluída com sucesso');
        }

        return back()->with('error', 'Erro ao excluir a cor');
    }
}