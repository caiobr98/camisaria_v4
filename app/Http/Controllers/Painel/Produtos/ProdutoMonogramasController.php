<?php

namespace App\Http\Controllers\Painel\Produtos;

use App\Http\Requests\ProdutoMonograma\ProdutoMonogramaAddRequest;
use App\Http\Requests\ProdutoMonograma\ProdutoMonogramaEditarRequest;
use App\Models\Produto\ProdutoMonograma;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class ProdutoMonogramasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $monogramas = ProdutoMonograma::all();

        return view('painel.produtos.monogramas.listagem', [
            'monogramas' => $monogramas
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.monogramas.cadastro');
    }

    public function cadastrar(ProdutoMonogramaAddRequest $request){
        $dados = $request->validated();
        
        $monograma = ProdutoMonograma::create($dados);

        if($monograma->exists){
            LogActivity::addToLog('Criou monograma de id = ' . $monograma->id);
            return redirect()->route('produto-monogramas-listar')->with('success', 'Monograma criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar o monograma.');
    }

    public function formEditar(ProdutoMonograma $monograma){
        return view('painel.produtos.monogramas.editar', [
            'monograma' => $monograma
        ]);
    }

    public function editar(ProdutoMonogramaEditarRequest $request){
        $dados = $request->validated();

        $monograma = ProdutoMonograma::where('id', $dados['id'])->update($dados);

        if($monograma){
            LogActivity::addToLog('Editou monograma de id = ' . $dados['id']);
            return redirect()->route('produto-monogramas-listar')->with('success', 'Monograma editado com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o monograma.');
    }

    public function detalhar(ProdutoMonograma $monograma){
        return view('painel.produtos.monogramas.detalhar', [
            'monograma' => $monograma
        ]);
    }

    public function deletar(ProdutoMonograma $monograma){
        $m = $monograma->delete();

        if($m){
            LogActivity::addToLog('Excluiu monograma de id = ' . $monograma->id);
            return back()->with('success', 'Monograma excluído com sucesso.');
        }

        return back()->with('success', 'Erro ao excluir o monograma.');
    }
}
