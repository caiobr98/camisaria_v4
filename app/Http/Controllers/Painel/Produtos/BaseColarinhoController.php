<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 10:11
 */

namespace App\Http\Controllers\Painel\Produtos;


use App\Http\Controllers\Controller;
use App\Http\Requests\BaseColarinho\BaseColarinhoAddRequest;
use App\Http\Requests\BaseColarinho\BaseColarinhoEditarRequest;
use App\Models\Produto\ProdutoBaseColarinho;
use App\Helpers\LogActivity;

class BaseColarinhoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        return view('painel.produtos.base_colarinho.listagem', [
            'colarinhos' => ProdutoBaseColarinho::all()
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.base_colarinho.cadastro');
    }

    public function cadastrar(BaseColarinhoAddRequest $request){
        $base = ProdutoBaseColarinho::create($request->validated());

        if($base->exists){
            LogActivity::addToLog('Criou base colarinho de id = ' . $base->id);
            return redirect()->route('produto-base-colarinho-listagem')->with('success', 'Base Colarinho cadastrada com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar base colarinho');
    }

    public function formEditar(ProdutoBaseColarinho $baseColarinho){
        return view('painel.produtos.base_colarinho.editar', [
            'base' => $baseColarinho
        ]);
    }

    public function editar(BaseColarinhoEditarRequest $request){
        $data = $request->validated();
        $r = ProdutoBaseColarinho::where('id', $data['id'])->update($data);

        if($r){
            LogActivity::addToLog('Editou base colarinho de id = ' . $data['id']);
            return redirect()->route('produto-base-colarinho-listagem')->with('success', 'Base colarinho editada com sucesso');
        }

        return back()->with('error', 'Erro ao editar base de colarinho');
    }

    public function detalhar(ProdutoBaseColarinho $baseColarinho){
        return view('painel.produtos.base_colarinho.detalhar', [
            'base' => $baseColarinho
        ]);
    }

    public function deletar(ProdutoBaseColarinho $baseColarinho){
        $r = $baseColarinho->delete();

        if($r){
            LogActivity::addToLog('Excluiu base colarinho de id = ' . $baseColarinho->id);
            return back()->with('success', 'Base Colarinho deletada com sucesso');
        }

        return back()->with('error', 'Erro ao deletar base colarinho');
    }
}