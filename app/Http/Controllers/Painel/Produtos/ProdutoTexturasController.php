<?php

namespace App\Http\Controllers\Painel\Produtos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProdutoTextura\ProdutoTexturaAddRequest;
use App\Models\Produto\ProdutoTextura;
use App\Helpers\LogActivity;


class ProdutoTexturasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem()
    {
        return view('painel.produtos.texturas.listagem', [
            'texturas' => ProdutoTextura::all()
        ]);
    }

    public function formCadastro()
    {
        return view('painel.produtos.texturas.cadastro');
    }

    public function cadastrar(ProdutoTexturaAddRequest $request)
    {
        $data = $request->validated();
        $t = ProdutoTextura::create($data);

        if($t->exists){
            LogActivity::addToLog('Criou textura de id = ' . $t->id);
            return redirect()->route('produto-textura-listar')->with('success', 'Textura cadastrada com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar a textura');
    }

    public function formEditar($textura)
    {
        $prodTextura = ProdutoTextura::find($textura); 
        return view('painel.produtos.texturas.editar', ['textura' => $prodTextura]);
    }
    
    public function editar(ProdutoTexturaEditarRequest $request)
    {
        $data = $request->validated();
        
        $t = ProdutoTextura::where('id', $data['id'])->update($data);

        if($t){
            LogActivity::addToLog('Editou textura de id = ' . $data['id']);
            return redirect()->route('produto-textura-listar')->with('success', 'Textura editada com sucesso');
        }

        return back()->with('error', 'Erro ao editar a textura');
    }   

    public function detalhar(ProdutoTextura $textura){
        return view('painel.produtos.texturas.detalhar', [
            'textura' => $textura
        ]);
    }

    public function deletar(ProdutoTextura $textura)
    {
        $t = $textura->delete();
        if($t){
            LogActivity::addToLog('Excluiu textura de id = ' . $textura->id);
            return back()->with('success', 'Excluído com sucesso');
        }

        return back()->with('error', 'Erro ao excluir');
    }
}
