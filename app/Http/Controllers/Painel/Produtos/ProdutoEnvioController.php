<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 13/05/2019
 * Time: 10:39
 */

namespace App\Http\Controllers\Painel\Produtos;


use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoPacote\ProdutoPacoteEditarRequest;
use App\Models\Produto\ProdutoPacote;
use App\Helpers\LogActivity;

class ProdutoEnvioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(){
        return view('painel.produtos.pacote.show', [
            'pacote' => ProdutoPacote::all()
        ]);
    }

    public function editar(ProdutoPacoteEditarRequest $request){

        $data = $request->validated();

        $comprimento = str_replace(',','.',str_replace('.', '', $request->comprimento));
        $largura = str_replace(',','.',str_replace('.', '', $request->largura));
        $altura = str_replace(',','.',str_replace('.', '', $request->altura));

        if($comprimento < 16 || $comprimento > 105) {

            return back()->with('error', 'Comprimento deve ser menor do que 16 cm ou maior do que 105 cm');

        } else {

            if($largura < 11 || $comprimento > 105) {

                return back()->with('error', 'Largura deve ser menor do que 11 cm ou maior do que 105 cm');

            } else {

                if ($altura < 1 || $comprimento > 105) {

                    return back()->with('error', 'Altura deve ser menor do que 1 cm ou maior do que 105 cm');

                } else {

                    $r = ProdutoPacote::where('id', $request->id)->update($data);

                    if($r){
                        LogActivity::addToLog('Editou medidas de envio de id = ' . $request->id);
                        return redirect()->route('produto-pacote-show')->with('success', 'Medidas de envio editadas com sucesso');
                    }
            
                    return back()->with('error', 'Erro ao editar o fio');

                }

            }

        }

    }

}