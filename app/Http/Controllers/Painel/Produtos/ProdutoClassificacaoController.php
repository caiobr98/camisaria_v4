<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-10
 * Time: 14:48
 */

namespace App\Http\Controllers\Painel\Produtos;


use App\Http\Controllers\Controller;
use App\Models\Produto\ProdutoClassificacao;
use App\Http\Requests\ProdutoClassificacao\ProdutoClassificacaoAddRequest;
use App\Http\Requests\ProdutoClassificacao\ProdutoClassificacaoEditarRequest;
use App\Helpers\LogActivity;

class ProdutoClassificacaoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        return view('painel.produtos.classificacao.listagem', [
            'classificacaos' => ProdutoClassificacao::all()
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.classificacao.cadastro');
    }

    public function cadastrar(ProdutoClassificacaoAddRequest $request){
        $data = $request->validated();
        $r = ProdutoClassificacao::create($data);

        if($r->exists){
            LogActivity::addToLog('Criou classificação de id = ' . $r->id);
            return redirect()->route('produto-classificacao-listar')->with('success', 'Classificacao cadastrada com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar a classificacao');
    }

    public function formEditar(ProdutoClassificacao $classificacao){
        return view('painel.produtos.classificacao.editar')->with([
            'classificacao' => $classificacao
        ]);
    }

    public function editar(ProdutoClassificacaoEditarRequest $request){
        $data = $request->validated();

        $r = ProdutoClassificacao::where('id', $data['id'])->update($data);

        if($r){
            LogActivity::addToLog('Editou classificação de id = ' . $data['id']);
            return redirect()->route('produto-classificacao-listar')->with('success', 'Classificacao editada com sucesso');
        }

        return back()->with('error', 'Erro ao editar a classificacao');
    }

    public function detalhar(ProdutoClassificacao $classificacao){
        return view('painel.produtos.classificacao.detalhar', [
            'classificacao' => $classificacao
        ]);
    }

    public function deletar(ProdutoClassificacao $classificacao){
        $r = $classificacao->delete();

        if($r){
            LogActivity::addToLog('Excluiu classificação de id = ' . $classificacao->id);
            return back()->with('success', 'Classificacao excluída com sucesso');
        }

        return back()->with('error', 'Erro ao excluir a classificacao');
    }
}