<?php

namespace App\Http\Controllers\Painel\Produtos;

use App\Http\Requests\ProdutoColarinho\ProdutoColarinhoAddRequest;
use App\Http\Requests\ProdutoColarinho\ProdutoColarinhoEditarRequest;
use App\Models\Produto\ProdutoColarinho;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class ProdutoColarinhosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $colarinhos = ProdutoColarinho::all();

        return view('painel.produtos.colarinhos.listagem', [
            'colarinhos' => $colarinhos
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.colarinhos.cadastro');
    }

    public function cadastrar(ProdutoColarinhoAddRequest $request){
        $dados = $request->validated();
        
        if($request->hasFile('colarinho_foto')){
            if ($request->file('colarinho_foto')->isValid()) {
                $imageName = time().'.'.$request->colarinho_foto->getClientOriginalExtension();
                $request->colarinho_foto->move(public_path('/img/produtos/colarinhos'), $imageName);
                $dados['colarinho_foto'] = '/img/produtos/colarinhos/' . $imageName;
            }
        }

        $colarinho = ProdutoColarinho::create($dados);

        if($colarinho->exists){
            LogActivity::addToLog('Criou colarinho de id = ' . $colarinho->id);
            return redirect()->route('produto-colarinhos-listar')->with('success', 'Colarinho criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar o colarinho.');
    }

    public function formEditar(ProdutoColarinho $colarinho){
        return view('painel.produtos.colarinhos.editar', [
            'colarinho' => $colarinho
        ]);
    }

    public function editar(ProdutoColarinhoEditarRequest $request){        
        $dados = $request->validated();

        if($request->hasFile('colarinho_foto')){
            if ($request->file('colarinho_foto')->isValid()) {
                $imageName = time().'.'.$request->colarinho_foto->getClientOriginalExtension();
                $request->colarinho_foto->move(public_path('/img/produtos/colarinhos'), $imageName);
                $dados['colarinho_foto'] = '/img/produtos/colarinhos/' . $imageName;
            }
        }

        $colarinho = ProdutoColarinho::where('id', $dados['id'])->update($dados);

        if($colarinho){
            LogActivity::addToLog('Editou colarinho de id = ' . $colarinho->id);
            return redirect()->route('produto-colarinhos-listar')->with('success', 'Colarinho editado com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o colarinho.');
    }

    public function detalhar(ProdutoColarinho $colarinho){
        return view('painel.produtos.colarinhos.detalhar', [
            'colarinho' => $colarinho
        ]);
    }

    public function deletar(ProdutoColarinho $colarinho){
        $c = $colarinho->delete();

        if($c){
            LogActivity::addToLog('Excluiu colarinho de id = ' . $colarinho->id);
            return back()->with('success', 'Colarinho excluído com sucesso.');
        }

        return back()->with('success', 'Erro ao excluir o colarinho.');
    }
}
