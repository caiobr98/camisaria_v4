<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 13/05/2019
 * Time: 14:11
 */

namespace App\Http\Controllers\Painel\Produtos;


use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoTecido\ProdutoTecidoAddRequest;
use App\Http\Requests\ProdutoTecido\ProdutoTecidoEditarRequest;
use App\Models\Fornecedor\Fornecedor;
use App\Models\Produto\ProdutoCor;
use App\Models\Produto\ProdutoFio;
use App\Models\Produto\ProdutoTecido;
use App\Models\Produto\ProdutoClassificacao;
use App\Models\Produto\ProdutoTextura;
use App\Helpers\LogActivity;

class ProdutoTecidosController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        return view('painel.produtos.tecidos.listagem', [
            'tecidos' => ProdutoTecido::all()
        ]);
    }

    public function formCadastro(){
        $fornecedores = Fornecedor::all();

        return view('painel.produtos.tecidos.cadastro', [
            'fios' => ProdutoFio::all(),
            'fornecedores' => $fornecedores,
            'cores' => ProdutoCor::orderBy('nome', 'ASC')->get(),
            'classificacoes' => ProdutoClassificacao::orderBy('nome', 'ASC')->get()
        ]);
    }

    public function cadastrar(ProdutoTecidoAddRequest $request){
        $data = $request->validated();
    
        if($request->hasFile('foto')){
            if ($request->file('foto')->isValid()) {
                $imageName = time().'.'.$request->foto->getClientOriginalExtension();
                $request->foto->move(public_path('/img/produtos/tecidos'), $imageName);
                $data['foto'] = '/img/produtos/tecidos/' . $imageName;
            }
        }

        $r = ProdutoTecido::create($data);

        if($r->exists){
            LogActivity::addToLog('Criou o tecido - ' . $r->id . ' - Código - ' . $r->codigo);
            return redirect()->route('produto-tecidos-listar')->with('success', 'Tecido cadastrado com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar o tecido');
    }

    public function formEditar(ProdutoTecido $tecido){
        $fornecedores = Fornecedor::all();

        return view('painel.produtos.tecidos.editar', [
            'fios' => ProdutoFio::all(),
            'texturas' =>ProdutoTextura::all(),
            'tecido' => $tecido,
            'fornecedores' => $fornecedores,
            'cores' => ProdutoCor::orderBy('nome', 'ASC')->get(),
            'classificacoes' => ProdutoClassificacao::orderBy('nome', 'ASC')->get()
        ]);
    }

    public function editar(ProdutoTecidoEditarRequest $request){
        
        $data = $request->validated();
        if($request->hasFile('foto')){
            if ($request->file('foto')->isValid()) {
                $imageName = time().'.'.$request->foto->getClientOriginalExtension();
                $request->foto->move(public_path('/img/produtos/tecidos'), $imageName);
                $data['foto'] = '/img/produtos/tecidos/' . $imageName;
            }
        }

        if(isset($data['status'])) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
        
        $tecido = ProdutoTecido::find($data['id']);
        $tecido->fill($data);
        $r = $tecido->update();

        if($r){
            LogActivity::addToLog('Editou o tecido - ' . $tecido->id . ' - Código - ' . $tecido->codigo);
            return redirect()->route('produto-tecidos-listar')->with('success', 'Tecido editado com sucesso');
        }

        return back()->with('error', 'Erro ao editar o tecido');
    }

    public function detalhar(ProdutoTecido $tecido){
        $fornecedores = Fornecedor::all();

        return view('painel.produtos.tecidos.editar', [
            'fios' => ProdutoFio::all(),
            'tecido' => $tecido,
            'fornecedores' => $fornecedores,
            'cores' => ProdutoCor::orderBy('nome', 'ASC')->get(),
            'classificacoes' => ProdutoClassificacao::orderBy('nome', 'ASC')->get()
        ]);
    }

    public function deletar(ProdutoTecido $tecido){
        $r = $tecido->delete();

        if($r){
            LogActivity::addToLog('Excluiu o tecido - ' . $tecido->id . ' - Código - ' . $tecido->codigo);
            return back()->with('success', 'Tecido excluído com sucesso');
        }

        return back()->with('error', 'Erro ao excluir o tecido');
    }
}