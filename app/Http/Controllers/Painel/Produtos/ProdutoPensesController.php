<?php

namespace App\Http\Controllers\Painel\Produtos;

use App\Http\Requests\ProdutoPense\ProdutoPenseAddRequest;
use App\Http\Requests\ProdutoPense\ProdutoPenseEditarRequest;
use App\Models\Produto\ProdutoPense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class ProdutoPensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $penses = ProdutoPense::all();

        return view('painel.produtos.penses.listagem', [
            'penses' => $penses
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.penses.cadastro');
    }

    public function cadastrar(ProdutoPenseAddRequest $request){
        $dados = $request->validated();
        $pense = ProdutoPense::create($dados);

        if($pense->exists){
            LogActivity::addToLog('Criou pense de id = ' . $pense->id);
            return redirect()->route('produto-penses-listar')->with('success', 'Pense criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar o pense.');
    }

    public function formEditar(ProdutoPense $pense){
        return view('painel.produtos.penses.editar', [
            'pense' => $pense
        ]);
    }

    public function editar(ProdutoPenseEditarRequest $request){
        $dados = $request->validated();

        $pense = ProdutoPense::where('id', $dados['id'])->update($dados);

        if($pense){
            LogActivity::addToLog('Editou pense de id = ' . $dados['id']);
            return redirect()->route('produto-penses-listar')->with('success', 'Pense editado com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o pense.');
    }

    public function detalhar(ProdutoPense $pense){
        return view('painel.produtos.penses.detalhar', [
            'pense' => $pense
        ]);
    }

    public function deletar(ProdutoPense $pense){
        $p = $pense->delete();

        if($p){
            LogActivity::addToLog('Excluiu pense de id = ' . $pense->id);
            return back()->with('success', 'Pense excluído com sucesso.');
        }

        return back()->with('success', 'Erro ao excluir o pense.');
    }
}
