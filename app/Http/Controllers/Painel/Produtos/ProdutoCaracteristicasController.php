<?php

namespace App\Http\Controllers\Painel\Produtos;

use App\Http\Requests\ProdutoCaracteristica\ProdutoCaracteristicaAddRequest;
use App\Http\Requests\ProdutoCaracteristica\ProdutoCaracteristicaEditarRequest;
use App\Models\Produto\ProdutoCaracteristica;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class ProdutoCaracteristicasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $caracteristicas = ProdutoCaracteristica::all();

        return view('painel.produtos.caracteristicas.listagem', [
            'caracteristicas' => $caracteristicas
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.caracteristicas.cadastro');
    }

    public function cadastrar(ProdutoCaracteristicaAddRequest $request){
        $dados = $request->validated();
        $caracteristica = ProdutoCaracteristica::create($dados);

        if($caracteristica->exists){
            LogActivity::addToLog('Criou característica de id = ' . $caracteristica->id);
            return redirect()->route('produto-caracteristicas-listar')->with('success', 'Característica criada com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar a característica.');
    }

    public function formEditar(ProdutoCaracteristica $caracteristica){
        return view('painel.produtos.caracteristicas.editar', [
            'caracteristica' => $caracteristica
        ]);
    }

    public function editar(ProdutoCaracteristicaEditarRequest $request){
        $dados = $request->validated();

        $caracteristica = ProdutoCaracteristica::where('id', $dados['id'])->update($dados);

        if($caracteristica){
            LogActivity::addToLog('Criou característica de id = ' . $dados['id']);
            return redirect()->route('produto-caracteristicas-listar')->with('success', 'Característica editada com sucesso.');
        }

        return back()->with('error', 'Erro ao editar a característica.');
    }

    public function detalhar(ProdutoCaracteristica $caracteristica){
        return view('painel.produtos.caracteristicas.detalhar', [
            'caracteristica' => $caracteristica
        ]);
    }

    public function deletar(ProdutoCaracteristica $caracteristica){
        $c = $caracteristica->delete();

        if($c){
            LogActivity::addToLog('Excluiu característica de id = ' . $caracteristica->id);
            return back()->with('success', 'Característica excluída com sucesso.');
        }

        return back()->with('success', 'Erro ao excluir a característica.');
    }
}
