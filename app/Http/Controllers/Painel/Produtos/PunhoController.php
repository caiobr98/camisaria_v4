<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 11:44
 */

namespace App\Http\Controllers\Painel\Produtos;


use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoPunho\ProdutoPunhoAddRequest;
use App\Http\Requests\ProdutoPunho\ProdutoPunhoEditarRequest;
use App\Models\Produto\ProdutoPunho;
use App\Helpers\LogActivity;

class PunhoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        return view('painel.produtos.punho.listagem', [
            'punhos' => ProdutoPunho::all()
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.punho.cadastro');
    }

    public function cadastrar(ProdutoPunhoAddRequest $request){
        $data = $request->validated();
        if($request->hasFile('punho_foto')){
            if ($request->file('punho_foto')->isValid()) {
                $imageName = time().'.'.$request->punho_foto->getClientOriginalExtension();
                $request->punho_foto->move(public_path('/img/produtos/punhos'), $imageName);
                $data['punho_foto'] = '/img/produtos/punhos/' . $imageName;
            }
        }
        
        $r = ProdutoPunho::create($data);

        if($r->exists){
            LogActivity::addToLog('Criou punho de id = ' . $r->id);
            return redirect()->route('produto-punho-listagem')->with('success', 'Punho cadastrado com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar o punho');
    }

    public function formEditar(ProdutoPunho $punho){
        return view('painel.produtos.punho.editar', [
            'punho' => $punho
        ]);
    }

    public function editar(ProdutoPunhoEditarRequest $request){
        $data = $request->validated();
        if($request->hasFile('punho_foto')){
            if ($request->file('punho_foto')->isValid()) {
                $imageName = time().'.'.$request->punho_foto->getClientOriginalExtension();
                $request->punho_foto->move(public_path('/img/produtos/punhos'), $imageName);
                $data['punho_foto'] = '/img/produtos/punhos/' . $imageName;
            }
        }else{
            if($request->has('imgCurrentURL')){
                $data['punho_foto'] = $request->input('imgCurrentURL');
            }
        }

        $r = ProdutoPunho::where('id', $data['id'])->update($data);

        if($r){
            LogActivity::addToLog('Editou punho de id = ' . $data['id']);
            return redirect()->route('produto-punho-listagem')->with('success', 'Punho editado com sucesso');
        }

        return back()->with('error', 'Erro ao editar o punho');
    }

    public function detalhar(ProdutoPunho $punho){
        return view('painel.produtos.punho.detalhar', [
            'punho' => $punho
        ]);
    }

    public function deletar(ProdutoPunho $punho){
        $r = $punho->delete();

        if($r){
            LogActivity::addToLog('Excluiu punho de id = ' . $punho->id);
            return back()->with('success', 'Punho excluído com sucesso');
        }

        return back()->with('error', 'Erro ao excluir o punho');
    }
};