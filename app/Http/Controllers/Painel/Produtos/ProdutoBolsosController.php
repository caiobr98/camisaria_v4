<?php

namespace App\Http\Controllers\Painel\Produtos;

use App\Http\Requests\ProdutoBolso\ProdutoBolsoAddRequest;
use App\Http\Requests\ProdutoBolso\ProdutoBolsoEditarRequest;
use App\Models\Produto\ProdutoBolso;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class ProdutoBolsosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $bolsos = ProdutoBolso::all();

        return view('painel.produtos.bolsos.listagem', [
            'bolsos' => $bolsos
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.bolsos.cadastro');
    }

    public function cadastrar(ProdutoBolsoAddRequest $request){
        $dados = $request->validated();
        $bolso = ProdutoBolso::create($dados);

        if($bolso->exists){
            LogActivity::addToLog('Criou bolso de id = ' . $bolso->id);
            return redirect()->route('produto-bolsos-listar')->with('success', 'Bolso criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar o bolso.');
    }

    public function formEditar(ProdutoBolso $bolso){
        return view('painel.produtos.bolsos.editar', [
            'bolso' => $bolso
        ]);
    }

    public function editar(ProdutoBolsoEditarRequest $request){
        $dados = $request->validated();

        $bolso = ProdutoBolso::where('id', $dados['id'])->update($dados);

        if($bolso){
            LogActivity::addToLog('Editou bolso de id = ' . $dados['id']);
            return redirect()->route('produto-bolsos-listar')->with('success', 'Bolso editado com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o bolso.');
    }

    public function detalhar(ProdutoBolso $bolso){
        return view('painel.produtos.bolsos.detalhar', [
            'bolso' => $bolso
        ]);
    }

    public function deletar(ProdutoBolso $bolso){
        $b = $bolso->delete();

        if($b){
            LogActivity::addToLog('Excluiu bolso de id = ' . $bolso->id);
            return back()->with('success', 'Bolso excluído com sucesso.');
        }

        return back()->with('success', 'Erro ao excluir o bolso.');
    }
}
