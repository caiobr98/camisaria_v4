<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 13/05/2019
 * Time: 10:39
 */

namespace App\Http\Controllers\Painel\Produtos;


use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoFio\ProdutoFioAddRequest;
use App\Http\Requests\ProdutoFio\ProdutoFioEditarRequest;
use App\Models\Produto\ProdutoFio;
use App\Helpers\LogActivity;

class ProdutoFiosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        return view('painel.produtos.fios.listagem', [
            'fios' => ProdutoFio::all()
        ]);
    }

    public function formCadastro(){
        return view('painel.produtos.fios.cadastro');
    }

    public function cadastrar(ProdutoFioAddRequest $request){
        $data = $request->validated();

        $r = ProdutoFio::create($data);

        if($r->exists){
            LogActivity::addToLog('Criou fio de id = ' . $r->id);
            return redirect()->route('produto-fios-listar')->with('success', 'Fio cadastrado com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar o fio');
    }

    public function formEditar(ProdutoFio $fio){
        return view('painel.produtos.fios.editar', [
            'fio' => $fio
        ]);
    }

    public function editar(ProdutoFioEditarRequest $request){
        $data = $request->validated();

        $r = ProdutoFio::where('id', $data['id'])->update($data);

        if($r){
            LogActivity::addToLog('Editou fio de id = ' . $data['id']);
            return redirect()->route('produto-fios-listar')->with('success', 'Fio editado com sucesso');
        }

        return back()->with('error', 'Erro ao editar o fio');
    }

    public function detalhar(ProdutoFio $fio){
        return view('painel.produtos.fios.detalhar', [
            'fio' => $fio
        ]);
    }

    public function deletar(ProdutoFio $fio){
        $r = $fio->delete();

        if($r){
            LogActivity::addToLog('Excluiu fio de id = ' . $fio->id);
            return back()->with('success', 'Fio excluído com sucesso');
        }

        return back()->with('error', 'Erro ao excluir o fio');
    }
}