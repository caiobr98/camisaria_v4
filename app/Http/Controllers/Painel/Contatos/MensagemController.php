<?php

namespace App\Http\Controllers\Painel\Contatos;

use App\Http\Controllers\Controller;
use App\Models\Contato\Contato;
use Illuminate\http\Request;
use App\Helpers\LogActivity;

class MensagemController extends Controller
{
    public function index()
    {
        $contato = Contato::all();
        return view('painel.contatos.listar',[
            'contato' => $contato
        ]);
    }
  
    public function visualizar($id)
    {
        $contato = Contato::findOrFail($id);        
        return view('painel.contatos.visualizar', [
            'contato' => $contato
        ]);
    }
  
    public function marcar($id)
    {
        $contato = Contato::findOrFail($id);

        $contato->update([
            'visita' => date('Y-m-d G:i:s')
        ]);

        LogActivity::addToLog('Visita marcada. Id da visita = ' . $id);

        return back()->with('success', 'Visita conclúida com sucesso.');
    }

    public function desmarcar($id)
    {
        $contato = Contato::findOrFail($id);

        $contato->update([
            'visita' => NULL
        ]);

        LogActivity::addToLog('Visita desmarcada. Id da visita = ' . $id);

        return back()->with('success', 'Visita desmaracada com sucesso.');
    }
}
