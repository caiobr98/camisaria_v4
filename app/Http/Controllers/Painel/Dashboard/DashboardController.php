<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 08/05/2019
 * Time: 15:30
 */

namespace App\Http\Controllers\Painel\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\ConfeccaosPagamentos;
use App\Models\Pedido\Pedido;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Helpers\LogActivity;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::count();
        $pedidos = Pedido::select(
            'pedidos.*'
        )
            ->join('confeccaos_pagamentos', 'confeccaos_pagamentos.pedido_id', 'pedidos.id')
            ->count();
            
        $valor_total = ConfeccaosPagamentos::selectRaw('SUM(pedidos.valor_total) AS valor')
            ->join('pedidos', 'pedidos.id', 'confeccaos_pagamentos.pedido_id')
            ->first();

        $vendasAnos = ConfeccaosPagamentos::selectRaw(
                'YEAR(confeccaos_pagamentos.created_at) AS ano,
                COUNT(confeccaos_pagamentos.created_at) AS quantidade,
                SUM(pedidos.valor_total) AS lucro'
            )
            ->join('pedidos', 'pedidos.id', 'confeccaos_pagamentos.pedido_id')
            ->groupBy(DB::raw('YEAR(confeccaos_pagamentos.created_at)'))
            ->get();

        $vendasMeses = ConfeccaosPagamentos::selectRaw(
                'MONTH(confeccaos_pagamentos.created_at) AS mes,
                COUNT(confeccaos_pagamentos.created_at) AS quantidade,
                SUM(pedidos.valor_total) AS lucro'
            )
            ->join('pedidos', 'pedidos.id', 'confeccaos_pagamentos.pedido_id')
            ->whereYear('confeccaos_pagamentos.created_at', date('Y'))
            ->groupBy(DB::raw('MONTH(confeccaos_pagamentos.created_at)'))
            ->get();

        $vendasSemanas = ConfeccaosPagamentos::selectRaw(
                'MONTH(confeccaos_pagamentos.created_at) as mes,
                WEEK(confeccaos_pagamentos.created_at) AS semana,
                COUNT(confeccaos_pagamentos.created_at) AS quantidade,
                SUM(pedidos.valor_total) AS lucro'
            )
            ->join('pedidos', 'pedidos.id', 'confeccaos_pagamentos.pedido_id')
            ->whereYear('confeccaos_pagamentos.created_at', date('Y'))
            ->groupBy(DB::raw('WEEK(confeccaos_pagamentos.created_at)'))
            ->get(); 

        $vendasDias = DB::select("SELECT 
                    confeccaos_pagamentos.created_at AS dia,
                    COUNT(confeccaos_pagamentos.created_at) AS quantidade,
                    SUM(pedidos.valor_total) AS lucro
                FROM 
                    confeccaos_pagamentos
                JOIN
                    pedidos ON pedidos.id = confeccaos_pagamentos.pedido_id
                WHERE
                    YEAR(confeccaos_pagamentos.created_at) = YEAR(now())
                    AND MONTH(confeccaos_pagamentos.created_at) = MONTH(now())
                    AND DAY(confeccaos_pagamentos.created_at) BETWEEN DAY(DATE(LAST_DAY(NOW()) - 7)) AND DAY(DATE(LAST_DAY(NOW()) - 1)) 
                GROUP BY
                    DAY(confeccaos_pagamentos.created_at)"
            );
        
        return view('painel.dashboard.dashboard', [
            'user' => $users,
            'pedidos' => $pedidos,
            'valor' => $valor_total->valor,
            'vendasAnos' => $vendasAnos,
            'vendasMeses' => $vendasMeses,
            'vendasSemanas' => $vendasSemanas,
            'vendasDias' => $vendasDias
        ]);
    }
}