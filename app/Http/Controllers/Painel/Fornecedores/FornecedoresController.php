<?php

namespace App\Http\Controllers\Painel\Fornecedores;

use App\Http\Requests\Fornecedor\FornecedorAddRequest;
use App\Http\Requests\Fornecedor\FornecedorEditarRequest;
use App\Models\Fornecedor\Fornecedor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class FornecedoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $fornecedores = Fornecedor::all();

        return view('painel.fornecedores.listagem', [
            'fornecedores' => $fornecedores
        ]);
    }

    public function formCadastro(){
        return view('painel.fornecedores.cadastro');
    }

    public function cadastrar(FornecedorAddRequest $request){
        $dados = $request->validated();
        $fornecedor = Fornecedor::create($dados);

        if($fornecedor->exists){
            LogActivity::addToLog('Fornecedor criado. Id do fornecedor = ' . $fornecedor->id);
            return redirect()->route('fornecedores-listar')->with('success', 'Fornecedor criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar o fornecedor.');
    }

    public function formEditar(Fornecedor $fornecedor){
        return view('painel.fornecedores.editar', [
            'fornecedor' => $fornecedor
        ]);
    }

    public function editar(FornecedorEditarRequest $request){
        $dados = $request->validated();

        $fornecedor = Fornecedor::where('id', $dados['id'])->update($dados);

        if($fornecedor){
            LogActivity::addToLog('Fornecedor editado. Id do fornecedor = ' . $dados['id']);
            return redirect()->route('fornecedores-listar')->with('success', 'Fornecedor editado com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o fornecedor.');
    }

    public function detalhar(Fornecedor $fornecedor){
        return view('painel.fornecedores.detalhar', [
            'fornecedor' => $fornecedor
        ]);
    }

    public function deletar(Fornecedor $fornecedor){
        $f = $fornecedor->delete();

        if($f){
            LogActivity::addToLog('Fornecedor deletado. Id do fornecedor = ' . $fornecedor->id);
            return back()->with('success', 'Fornecedor excluído com sucesso.');
        }

        return back()->with('error', 'Erro ao excluir o fornecedor.');
    }
}
