<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 08/05/2019
 * Time: 15:56
 */

namespace App\Http\Controllers\Painel\Clientes;


use App\Http\Controllers\Controller;
use App\Http\Requests\Cliente\ClienteAddRequest;
use App\Http\Requests\Cliente\ClienteEditarRequest;
use App\Models\Cliente\Cliente;
use App\User;
use App\Models\Confeccaomedida;
use App\Models\Cliente\Endereco;
use App\Models\Cliente\Historico;
use App\Models\Camiseiro\Camiseiro;
use App\Models\Fornecedor\Fornecedor;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;


class ClientesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $clientes = Cliente::join('users', 'users.id', 'clientes.user_id')->get();

        return view('painel.clientes.listagem', [
            'clientes' => $clientes
        ]);
    }

    public function formCadastro(User $user){
        return view('painel.clientes.cadastro', [
            'user' => $user
        ]);
    }

    public function cadastrar(ClienteAddRequest $request){
        $userData = $request->validated();
        // dd($request);
        $user = User::create([
            'name' => $request['users.name'],
            'email' => $request['users.email'],
            'password' => bcrypt($request['users.password'])
        ])
        ->cliente()->create($userData['clientes']);

        if($user->exists){
            LogActivity::addToLog('Criou o cliente - ' . $user->id . ' - nome - ' . $user->name);
            return redirect()->route('clientes-processo-camiseiro-endereco-medidas', ['id' => $user->user_id])->with('success', 'Cliente criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar o cliente.');
    }

    public function formEditar(User $user){
        return view('painel.clientes.editar', [
            'user' => $user
        ]);
    }

    public function editar(ClienteEditarRequest $request){
        $dados = $request->validated();

        $u = User::where('id', $dados['id'])->update($request['users']);
        $c = Cliente::where('user_id', $dados['id'])->update($request['clientes']);

        if($u && $c){
            LogActivity::addToLog('Editou o cliente - ' . $dados['id'] . ' - nome - ' . $dados['users']['name']);
            return redirect()->route('clientes-processo-camiseiro-endereco-medidas', ['id' => $dados['id']])->with('success', 'Cliente Editado com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o cliente.');
    }

    public function detalhar(User $user){
        return view('painel.clientes.detalhar', [
            'user' => $user
        ]);
    }

    public function deletar(User $user){
        $user->cliente->delete();
        $u = $user->delete();

        if($u){
            LogActivity::addToLog('Deletou o cliente - ' . $user->id . ' - nome - ' . $user->name);
            return back()->with('success', 'Cliente excluído com sucesso.');
        }

        return back()->with('error', 'Erro ao excluir o cliente.');
    }

    public function processoCamiseiroEnderecoMedidas(User $user)
    {
        $camiseiros = Camiseiro::orderBy('nome', 'ASC')->get();
        $fornecedores = Fornecedor::orderBy('nome', 'ASC')->get();
        $medida = Confeccaomedida::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $endereco = Endereco::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $historico = Historico::where('user_id', $user->id)->orderBy('id', 'DESC')->first();

        return view('painel.clientes.processocamiseiroenderecomedidas.cadastro')->with([
            'camiseiros' => $camiseiros,
            'user' => $user,
            'fornecedores' => $fornecedores,
            'medida' => $medida,
            'endereco' => $endereco,
            'historico' => $historico
        ]);
    }

    public function listaDeletados()
    {
        $clientes = Cliente::onlyTrashed()->select('users.name','users.email','clientes.celular','clientes.telefone','clientes.id as cliente_id')->join('users', 'users.id', 'clientes.user_id')->get();
        return view('painel.clientes.listagemDeletados', [
            'clientes' => $clientes
        ]);
    }

    public function recuperar($id)
    {
        Cliente::where('id', $id)->restore();
        return redirect()->route('clientes-deletados');
    }
}
