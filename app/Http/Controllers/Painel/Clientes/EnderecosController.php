<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 09/05/2019
 * Time: 12:20
 */

namespace App\Http\Controllers\Painel\Clientes;


use App\Http\Controllers\Controller;
use App\Http\Requests\ClienteEndereco\ClienteEnderecoAddRequest;
use App\Http\Requests\ClienteEndereco\ClienteEnderecoEditarRequest;
use App\Models\Cliente\Endereco;
use App\User;

class EnderecosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listar(User $user){
        return view('painel.clientes.endereco.listagem', [
            'user' => $user
        ]);
    }

    public function formCadastrar(User $user){
        return view('painel.clientes.endereco.cadastro', [
            'user' => $user
        ]);
    }

    public function cadastrar(ClienteEnderecoAddRequest $request, $processoCamiseiroEnderecoMedidas = NULL){
        $endereco = Endereco::create($request->validated());

        if($endereco->exists){
            if($processoCamiseiroEnderecoMedidas) {
                return redirect()->back()->with('success', 'Endereço cadastrado com sucesso');
            }
            return back()->with('success', 'Endereço do cliente editado com sucesso');
        }

        return back()->with('error', 'Erro ao cadastrar o endereço');
    }

    public function formEditar(User $user, Endereco $endereco){
        return view('painel.clientes.endereco.editar', [
            'user' => $user,
            'endereco' => $endereco
        ]);
    }

    public function editar(ClienteEnderecoEditarRequest $request){
        $dados = $request->validated();

        $r = Endereco::where('id', $dados['id'])->update($dados);

        if($r){
            return redirect()->route('clientes-endereco', $dados['user_id'])->with('success', 'Endereço editado com sucesso');
        }

        return back()->with('error', 'Erro ao editar o usuário');
    }

    public function detalhar(User $user, Endereco $endereco){
        return view('painel.clientes.endereco.detalhar', [
            'user' => $user,
            'endereco' => $endereco
        ]);
    }

    public function deletar(Endereco $endereco){
        $e = $endereco->delete();

        if($e){
            return back()->with('success', 'Endereço deletado com sucesso');
        }

        return back()->with('error', 'Erro ao deltar endereço');
    }
}