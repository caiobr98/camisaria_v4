<?php

namespace App\Http\Controllers\Painel\Clientes;

use App\Http\Requests\Medida\MedidaAddRequest;
use App\Http\Requests\Medida\MedidaEditarRequest;
use App\Models\Cliente\Medida;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Confeccao;
use App\Models\Confeccaomedida;
use App\Helpers\LogActivity;

class MedidasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem($user_id){
        $user = User::with('medidas')
                ->find($user_id);
        $medidas = Confeccaomedida::where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get();
            
        return view('painel.clientes.medidas.listagem', [
            'user' => $user,
            'medidas' => $medidas,
        ]);
    }

    public function formCadastro($user_id){
        $user = User::find($user_id);

        return view('painel.clientes.medidas.cadastro', [
            'user' => $user
        ]);
    }

    public function cadastrar($user_id, MedidaAddRequest $request, $processoCamiseiroEnderecoMedidas = NULL){
        $medidaData = $request->validated();

        if (isset($request->id)) {
            $confeccaoMedidaExistente = Confeccaomedida::where('id',$request->id)->update($request->except(['_token', 'user_id']));
            LogActivity::addToLog('Alterou a medida para o cliente de id = ' . $user_id . '. Id da medida alterada = ' . $request->id);
            if (isset($processoCamiseiroEnderecoMedidas)) {
                return redirect()->back()->with('success', 'Medida alterada com sucesso.');
            } else {
                return redirect()->route('medidas-listar', $user_id)->with('success', 'Medida alterada com sucesso.');
            }
        } else {
            $confeccaoMedidaExistente = Confeccaomedida::where([
                ['user_id', '=', $request->user_id],
                ['colarinho', '=', $request->colarinho],
                ['largura_costas', '=', $request->largura_costas],
                ['punho', '=', $request->punho],
                ['torax', '=', $request->torax],
                ['largura_costas_dois', '=', $request->largura_costas_dois],
                ['busto', '=', $request->busto],
                ['cintura', '=', $request->cintura],
                ['cava', '=', $request->cava],
                ['altura_busto', '=', $request->altura_busto],
                ['quadril', '=', $request->quadril],
                ['comp_manga_e', '=', $request->comp_manga_e],
                ['dist_busto', '=', $request->dist_busto],
                ['comp_total', '=', $request->comp_total],
                ['comp_mana_d', '=', $request->comp_mana_d],
                ['biceps', '=', $request->biceps],
                ['ombro', '=', $request->ombro],
            ])->first();

            if(!isset($confeccaoMedidaExistente)) {
                $confeccaomedida = Confeccaomedida::create($request->except(['_token']));
                if (isset($processoCamiseiroEnderecoMedidas)) {
                    LogActivity::addToLog('Alterou a medida para o cliente de id = ' . $user_id . '. Id da medida alterada = ' . $confeccaomedida->id);
                    return redirect()->back()->with('success', 'Medida alterada com sucesso.');                    
                } else {
                    LogActivity::addToLog('Criou a medida para o cliente de id = ' . $user_id . '. Id da medida criada = ' . $confeccaomedida->id);
                    return redirect()->route('medidas-listar', $user_id)->with('success', 'Medida criada com sucesso.');
                }
            }
        }

        /*$medida = User::find($user_id)->medidas()->create($medidaData);

        if($medida->exists){
            if($processoCamiseiroEnderecoMedidas) {
                return redirect()->route('clientes-processo-camiseiro-endereco-medidas', $user_id)->with('success', 'Salvo com sucesso.');
            }

            return redirect()->route('clientes-processo-camiseiro-endereco-medidas', $user_id)->with('success', 'Medida criada com sucesso.');
        }*/

        return back()->with('error', 'Ocorreu um erro ao criar a medida.');
    }

    public function formEditar($user_id, $medida_id){
        $user = User::find($user_id);

        $medida = $user->medidas()->find($medida_id);
        $medida = Confeccaomedida::where('id', $medida_id)->first();

        return view('painel.clientes.medidas.editar', [
            'user' => $user,
            'medida' => $medida
        ]);
    }

    public function editar($user_id, $medida_id, MedidaEditarRequest $request){
        $dados = $request->validated();
        $medida = Confeccaomedida::where('id', $medida_id)->update($dados);

        if($medida){
            LogActivity::addToLog('Alterou a medida para o cliente de id = ' . $user_id . '. Id da medida alterada = ' . $medida_id);
            return back()->with('success', 'Medida editada com sucesso.');
        }

        return back()->with('error', 'Erro ao editar o medida.');
    }

    public function detalhar($user_id, $medida_id){
        $user = User::find($user_id);

        $medida = $user->medidas()->find($medida_id);

        return view('painel.clientes.medidas.detalhar', [
            'user' => $user,
            'medida' => $medida
        ]);
    }

    public function deletar($user_id, $medida_id){
        $medida = Confeccaomedida::find($medida_id)->delete();

        if($medida){
            LogActivity::addToLog('Excluiu a medida para o cliente de id = ' . $user_id . '. Id da medida excluída = ' . $medida_id);
            return redirect()->route('medidas-listar', $user_id)->with('success', 'Medida excluída com sucesso.');
        }

        return back()->with('success', 'Erro ao excluir o medida.');
    }
}
