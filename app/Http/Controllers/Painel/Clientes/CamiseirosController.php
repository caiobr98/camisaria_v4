<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-11
 * Time: 17:43
 */

namespace App\Http\Controllers\Painel\Clientes;


use App\Http\Controllers\Controller;
use App\Models\Camiseiro\Camiseiro;
use App\Models\Cliente\ClienteCamiseiro;
use App\User;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;

class CamiseirosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editarCamiseiro(User $user){
        $camiseiros = Camiseiro::orderBy('nome', 'ASC')->get();

        return view('painel.clientes.camiseiro.editar')->with([
            'camiseiros' => $camiseiros,
            'user' => $user
        ]);
    }

    public function update(Request $request){
        $data = $request->validate([
            'user_id'  => 'required',
            'camiseiro_id' => 'required'
        ]);

        $clienteCamiseiro = ClienteCamiseiro::join('users', 'users.id', 'cliente_camiseiros.user_id')
            ->updateOrCreate(
                ['user_id' => $data['user_id']],
                ['camiseiro_id' => $data['camiseiro_id']]
            );
            
        if($clienteCamiseiro->exists){
            LogActivity::addToLog('Alterou o camiseiro do cliente: ' .  $clienteCamiseiro->name);
            return back()->with('success', 'Camiseiro do cliente editado com sucesso');
        }

        return back()->with('error', 'Erro ao editar o camiseiro do cliente');
    }

}