<?php

namespace App\Http\Controllers\Painel\Clientes;

use App\Http\Requests\Historico\HistoricoAddRequest;
use App\Http\Requests\Historico\HistoricoEditarRequest;
use App\Models\Cliente\Historico;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Fornecedor\Fornecedor;
use Carbon\Carbon;
use App\Helpers\LogActivity;

class HistoricoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(User $user){
        return view('painel.clientes.historico.listagem', [
            'user' => $user
        ]);
    }

    public function formCadastrar($user_id){
        $user = User::find($user_id);
        $fornecedores = Fornecedor::orderBy('nome', 'ASC')->get();
        
        return view('painel.clientes.historico.cadastro', [
            'user' => $user,
            'fornecedores' => $fornecedores
        ]);
    }

    public function cadastrar($user_id, HistoricoAddRequest $request, $processoCamiseiroEnderecohistorico = NULL){
        $historicoData = $request->validated();
        
        $historicoData['data_compra'] = Carbon::createFromFormat('d/m/Y', $historicoData['data_compra'])->format('Y-m-d');
        
        $historico = User::find($user_id)->historico()->create($historicoData);
        
        if($historico->exists){
            LogActivity::addToLog('Historico criado para o cliente de id = ' . $user_id . '. Id do historico = ' . $historico->id);

            if($processoCamiseiroEnderecohistorico) {
                return redirect()->route('clientes-processo-camiseiro-endereco-medidas', $user_id)->with('success', 'Salvo com sucesso.');
            }
            
            return redirect()->route('clientes-processo-camiseiro-endereco-medidas', $user_id)->with('success', 'Historico criado com sucesso.');
        }

        return back()->with('error', 'Ocorreu um erro ao criar a historico.');
    }

    public function formEditar($user_id, $historico_id){
        $user = User::find($user_id);
        $fornecedores = Fornecedor::orderBy('nome', 'ASC')->get();

        $historico = $user->historico()->find($historico_id);

        return view('painel.clientes.historico.editar', [
            'user' => $user,
            'historico' => $historico,
            'fornecedores' => $fornecedores
        ]);
    }

    public function editar(Request $request){
        $historicoData = $request->except('_token');
        $historicoData['data_compra'] = Carbon::createFromFormat('d/m/Y', $historicoData['data_compra'])->format('Y-m-d');

        $r = Historico::where('id', $historicoData['id'])->update($historicoData);

        if($r){
            LogActivity::addToLog('Historico editado para o cliente de id = ' . $historicoData['user_id'] . '. Id do historico = ' . $historicoData['id']);
            return redirect()->route('historico-listar', $historicoData['user_id'])->with('success', 'Histórico editado com sucesso');
        }

        return back()->with('error', 'Erro ao editar o usuário');
    }


    public function deletar(Historico $historico){
        $e = $historico->delete();

        if($e){
            LogActivity::addToLog('Historico excluído. Id do historico = ' . $historico->id);
            return back()->with('success', 'Endereço deletado com sucesso');
        }

        return back()->with('error', 'Erro ao deltar endereço');
    }
}
