<?php

namespace App\Http\Controllers\Painel\Configuracoes;

use App\Http\Controllers\Controller;
use App\Http\Requests;
// use App\Models\LogActivity;
use App\DataTablesReceiver;
use App\User;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
class LogActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $logs = LogActivity::logActivityLists();

        // return view('painel/configuracoes.index', ['users' => User::where('type', 'admin')->get()]);
        return view('painel/configuracoes.index',compact('logs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('painel/configuracoes.log-activity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        LogActivity::create($requestData);

        return redirect('painel/configuracoes/log-activity')->with('flash_message', 'LogActivity added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $logactivitys = LogActivity::select(
            'id',
            'description',
            'subject_type',
            'properties',
            'created_at'
            )
            ->where('causer_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();
        $user = User::find($id)['name'];
            // dd($logactivitys);
        

        return view('painel/configuracoes.show', ['logactivitys' => $logactivitys, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $logactivity = LogActivity::findOrFail($id);
        $properties = json_decode($logactivity['properties'], true);

        return view('painel/configuracoes.edit', compact('logactivity', 'properties'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $logactivity = LogActivity::findOrFail($id);
        $logactivity->update($requestData);

        return redirect('painel/configuracoes/log-activity')->with('flash_message', 'LogActivity updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LogActivity::where('id', $id)->update([
            'status' => 0
        ]);

        return json_encode(1);
    }
}
