<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 15/05/2019
 * Time: 10:00
 */

namespace App\Http\Controllers\Painel\Pedido;


use App\Http\Controllers\Controller;
use App\Models\Camiseiro\Camiseiro;
use App\Models\ConfeccaosPagamentos;
use App\Models\Cupons\Cupom;
use App\Models\Pedido\Pedido;
use App\Models\Pedido\PedidoStatus;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;

class PedidoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listagem(){
        $pedidos = Pedido::select(
                'pedidos.*',
                'clientes.cpf'
            )
            //->leftJoin('confeccaos_pagamentos', 'confeccaos_pagamentos.pedido_id', 'pedidos.id')
            ->join('clientes', 'clientes.user_id', 'pedidos.user_id')
            ->get();

        return view('painel.pedidos.listagem', [
            'pedidos' => $pedidos
        ]);
    }

    public function detalhar($id){
        
        $compra = ConfeccaosPagamentos::select(
            'users.name',
            'users.email',
            'pedidos.nome',
            'pedidos.status_pagamento',
            'pedidos.id_cupom',
            'pedidos.amostra',
            'pedidos.frete',
            'confeccaos_pagamentos.pedido_id',
            'confeccaos_pagamentos.id',
            'produto_tecidos.nome As tecidos_nome',
            'produto_fios.valor As fio_valor',
            'produto_tecidos.codigo',
            'confeccao_detalhes.bolso',
            'confeccao_detalhes.monograma',
            'produto_colarinhos_punhos.punho_colarinho_foto',
            'produto_colarinhos_punhos.punho_colarinho_foto_bolso',
            'endereco_pedidos.rua',
            'endereco_pedidos.cep',
            'endereco_pedidos.numero',
            'pedidos.valor_total',
            'pedidos.status_pagamento',
            'pedidos.frete',
            'pedidos.tipo_frete',
            'produto_punhos.nome As punhos_nome',
            'produto_colarinhos.nome As colarinhos_nome',
            'confeccaomedidas.colarinho',
            'confeccaomedidas.largura_costas',
            'confeccaomedidas.punho',
            'confeccaomedidas.torax',
            'confeccaomedidas.largura_costas_dois',
            'confeccaomedidas.cintura',
            'confeccaomedidas.cava',
            'confeccaomedidas.quadril',
            'confeccaomedidas.comp_manga_e',
            'confeccaomedidas.comp_total',
            'confeccaomedidas.comp_mana_d',
            'confeccaomedidas.biceps',
            'confeccaomedidas.ombro'
        )
            ->join('pedidos', 'pedidos.id', 'confeccaos_pagamentos.pedido_id')
            ->join('confeccaos', 'confeccaos.id', 'confeccaos_pagamentos.confeccaos_id')
            ->join('endereco_pedidos', 'endereco_pedidos.id', 'pedidos.endereco_pedido_id')
            ->join('produto_colarinhos_punhos', 'produto_colarinhos_punhos.id', 'confeccaos.produto_colarinhos_punhos_id')
            ->join('confeccao_detalhes', 'confeccao_detalhes.id', 'confeccaos.confeccao_detalhe_id')
            ->join('produto_tecidos', 'produto_tecidos.id', 'confeccaos.produto_tecido_id')
            ->join('produto_fios', 'produto_fios.id', 'confeccaos.produto_fio_id')
            ->join('produto_punhos', 'produto_punhos.id', 'confeccaos.produto_punho_id')
            ->join('produto_colarinhos', 'produto_colarinhos.id', 'confeccaos.produto_colarinho_id')
            ->join('confeccaomedidas', 'confeccaomedidas.id', 'confeccaos.medida_id')
            ->join('users', 'users.id', 'confeccaos.user_id')
            ->where('confeccaos_pagamentos.pedido_id', $id)
            ->get();

            
            $cupom = null;
            
            if($compra[0]->id_cupom != null){

                $cupom = Cupom::where('id', '=', $compra[0]->id_cupom)->first();
            }
            
            return view('painel.pedidos.detalhar', [
            'compra' => $compra,
            'cupom' => $cupom
        ]);
    }

    public function formProcessar(Pedido $pedido){
        return view('painel.pedidos.processar', [
            'pedido' => $pedido,
            'camiseiros' => Camiseiro::all(),
            'status' => PedidoStatus::all()
        ]);
    }

    public function processar(Pedido $pedido, Request $request){
        $validated = $request->validate([
            'camiseiro_id' => 'required|numeric',
            'pedido_status_id'=> 'required|numeric'
        ]);

        if($pedido->update($validated)){
            LogActivity::addToLog('Pedido processado. Id do pedido = ' . $pedido->id);
            return redirect()->route('pedidos-listar')->with('success', 'Pedido processado com sucesso');
        }

        return back()->with('error', 'Erro ao processar o pedido');
    }

    public function ajaxPreco()
    {
        $preco = Pedido::select(DB::raw('valor_total'))
            ->where('YEAR(created_at)', date("Y"))
            ->orderBy('MONTH(created_at)')
            ->get();
    }

    public function cancelaTranzacao($id) 
    {
        $pedido = Pedido::find($id);
        $tid = $pedido->tid;
        $valor_total = $pedido->valor_total;
        // Configuração da loja em modo produção
        // $store = new \Rede\Store('PV', 'TOKEN', \Rede\Environment::production());
        
        // Configuração da loja em modo sandbox
        //$store = new \Rede\Store('10004504', '769a5a598c8c4180b2dfd5b3c7e7f6f3', \Rede\Environment::sandbox());

        $store = new \Rede\Store('082998051', '6726f0527d8d4e57b18cc3e1693c776b', \Rede\Environment::production());
        
        // Transação que será cancelada
        $transaction = (new \Rede\eRede($store))->cancel((new \Rede\Transaction($valor_total))->setTid($tid));
        if ($transaction->getReturnCode() == '360') {
            // printf("Transação cancelada com sucesso; tid=%s\n", $transaction->getTid());
            $pedido->status_pagamento = 'Cancelado';
            $pedido->save();
            LogActivity::addToLog('Pagamento cancelado. Id do pagamento = ' . $id);
            return back()->with('success', 'Pagamento cancelado com sucesso!');
        } else {
            return back()->with('error', 'Erro ao realizar o cancelamento do pagamento!');
        }
    }
}