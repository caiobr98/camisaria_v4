<?php

namespace App\Http\Controllers\Api\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\PagamentoRepository;

class PagamentoController extends Controller
{
    private $pagamento;

    public function __construct(PagamentoRepository $pagamentoRepository)
    {
        $this->pagamento = $pagamentoRepository;
    }
    
    public function index(Request $request)
    {
        return $this->pagamento->index($request);
    }

    
}
