<?php

namespace App\Http\Controllers\Api\site\Pedidos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\Pedidos\PedidosRepository;

class PedidosController extends Controller
{
    private $pedidos;

    public function __construct(PedidosRepository $pedidosRepository)
    {
        $this->pedidos = $pedidosRepository;
    }

    public function pedidoByUserId($id)
    {
        return $this->pedidos->pedidoByUserId($id);
    }

    public function pedidoById($userid,$id)
    {
        return $this->pedidos->pedidoById($userid, $id);
    }

    public function calcFrete(Request $request)
    {

        return $this->pedidos->calcFrete($request->cep, $request->peso, $request->comprimento, $request->largura, $request->altura, $request->tipo);

    }

    public function showMedidas(){
    
        return $this->pedidos->showMedidas();
    }

}