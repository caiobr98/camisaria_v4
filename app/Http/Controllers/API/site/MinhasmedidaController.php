<?php

namespace App\Http\Controllers\Api\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\MinhasmedidaRepository;
use App\Models\Confeccaomedida;
use App\Models\Medida;

class MinhasmedidaController extends Controller
{
    private $minhasmedida;

    public function __construct(MinhasmedidaRepository $minhasmedidaRepository)
    {
        $this->middleware('auth:api');
        $this->minhasmedida = $minhasmedidaRepository;
    }

    public function index(Request $request)
    {
        return $this->minhasmedida->index($request);
    }

    public function store(Request $request)
    {
        $minhasmedida = $this->minhasmedida->store($request);

        return response()->json($minhasmedida, 201);
    }

    public function show($id)
    {
        return $this->minhasmedida->show($id);
    }

    public function update(Request $request, $id)
    {
        $minhasmedida = $this->minhasmedida->update($request, $id);
        $minhasmedida->update($request->all());

        return response()->json($minhasmedida, 200);
    }

    public function destroy($id)
    {
        $this->minhasmedida->destroy($id);

        return response()->json(null, 204);
    }
}
