<?php

namespace App\Http\Controllers\API\site;

use App\Http\Controllers\Controller;
use App\Repositories\site\WelcomeRepository;

class WelcomeController extends Controller
{
    private $welcome;

    public function __construct(WelcomeRepository $welcomeRepository)
    {
        $this->welcome = $welcomeRepository;
    }

    public function areaDestaque()
    {
        return $this->welcome->areaDestaque();
    }
}