<?php

namespace App\Http\Controllers\Api\site\Produto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ProdutoTecidoRepository;

class ProdutoTecidoController extends Controller
{
    private $tecido;

    public function __construct(ProdutoTecidoRepository $produtoTecidoRepository)
    {
        //$this->middleware('auth');
        $this->tecido = $produtoTecidoRepository;
    }

    public function index(Request $request)
    {
        return $this->tecido->index($request);
    }

    public function indexWithFio($id)
    {
        return $this->tecido->indexWithFio($id);
    }

    public function store(Request $request)
    {
        $tecido = $this->tecido->store($request);

        return response()->json($tecido, 201);
    }

    public function show($id)
    {
        return $this->tecido->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $tecido = $this->tecido->update($request, $id);
        $tecido->update($request->all());

        return response()->json($tecido, 200);
    }

    public function destroy($id)
    {
        $this->tecido->destroy($id);

        return response()->json(null, 204);
    }

    public function produtoTecidoCorTexturaPadrao(Request $request)
    {
        return $this->tecido->produtoTecidoCorTexturaPadrao($request);
    }

    public function listaTecidos()
    {
        return $this->tecido->listaTecidos();
    }
}
