<?php

namespace App\Http\Controllers\Api\site\Produto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ProdutoFioRepository;

class ProdutoFioController extends Controller
{
    private $fio;

    public function __construct(ProdutoFioRepository $produtoFioControllerRepository)
    {
        $this->fio = $produtoFioControllerRepository;
    }

    public function index(Request $request)
    {
        return $this->fio->index($request);

    }
    public function store(Request $request)
    {
        $fio = $this->fio->store($request);

        return response()->json($fio, 201);
    }

    public function show($id)
    {
        return $this->fio->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $fio = $this->fio->update($request, $id);
        $fio->update($request->all());

        return response()->json($fio, 200);
    }

    public function destroy($id)
    {
        $this->fio->destroy($id);

        return response()->json(null, 204);
    }

    public function listaFios()
    {
        return $this->fio->listaFios();
    }
    
}