<?php

namespace App\Http\Controllers\Api\site\Produto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ProdutoPunhoRepository;

class ProdutoPunhoController extends Controller
{
    private $punho;

    public function __construct(ProdutoPunhoRepository $produtoPunhoControllerRepository)
    {
        $this->punho = $produtoPunhoControllerRepository;
    }
    
    public function index(Request $request)
    {
        return $this->punho->index($request);

    }
    public function store(Request $request)
    {
        $punho = $this->punho->store($request);

        return response()->json($punho, 201);
    }

    public function show($id)
    {
        return $this->punho->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $punho = $this->punho->update($request, $id);
        $punho->update($request->all());

        return response()->json($punho, 200);
    }

    public function destroy($id)
    {
        $this->punho->destroy($id);

        return response()->json(null, 204);
    }

    public function listaPunhos()
    {
        return $this->punho->listaPunhos();
    }
}
