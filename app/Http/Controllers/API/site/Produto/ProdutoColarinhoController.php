<?php

namespace App\Http\Controllers\Api\site\Produto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ProdutoColarinhoRepository;

class ProdutoColarinhoController extends Controller
{
    private $produtoColarinho;

    public function __construct(ProdutoColarinhoRepository $produtoColarinhoRepository)
    {
        $this->produtoColarinho = $produtoColarinhoRepository;
    }

    public function index(Request $request)
    {
        return $this->produtoColarinho->index($request);
    }

    public function store(Request $request)
    {
        $produtoColarinho = $this->produtoColarinho->store($request);

        return response()->json($produtoColarinho, 201);
    }

    public function show($id)
    {
        return $this->produtoColarinho->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $produtoColarinho = $this->produtoColarinho->update($request, $id);
        $produtoColarinho->update($request->all());

        return response()->json($produtoColarinho, 200);
    }

    public function destroy($id)
    {
        $this->produtoColarinho->destroy($id);

        return response()->json(null, 204);
    }

    public function listaColarinhos()
    {
        return $this->produtoColarinho->listaColarinhos();
    }
}
