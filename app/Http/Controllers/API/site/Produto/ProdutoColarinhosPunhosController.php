<?php

namespace App\Http\Controllers\Api\site\Produto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ProdutoColarinhosPunhosRepository;

class ProdutoColarinhosPunhosController extends Controller
{
    private $produtoColarinhoPunho;

    public function __construct(ProdutoColarinhosPunhosRepository $produtoColarinhoPunhoRepository)
    {
        $this->produtoColarinhoPunho = $produtoColarinhoPunhoRepository;
    }

    public function index(Request $request)
    {
        return $this->produtoColarinhoPunho->index($request);
    }

    public function colarinhoPunhoFoto($produto_punhos_id, $produto_colarinhos_id)
    {
        return $this->produtoColarinhoPunho->colarinhoPunhoFoto($produto_punhos_id, $produto_colarinhos_id);
    }
}