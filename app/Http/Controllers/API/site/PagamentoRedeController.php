<?php

namespace App\Http\Controllers\Api\site;

use App\Http\Controllers\Controller;
use App\Models\Cliente\EnderecoPedido;
use App\Models\Confeccao;
use App\Models\ConfeccaosPagamentos;
use App\Models\Pagamentos\Pagamento;
use App\Models\Pedido\Pedido;
use Illuminate\Http\Request;
use App\Repositories\site\MinhasmedidaRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Cupons\Cupom;

class PagamentoRedeController extends Controller
{
    public function autorizaTransacao(Request $request)
   {
        $request->validate([
            'numero' => 'required|string|max:20',
            'cep' => 'required|string|max:191',
            'rua' => 'required|string',
        ]);

       // Configuração da loja em modo produção
       //$store = new \Rede\Store('PV', 'TOKEN', \Rede\Environment::production());
       $store = new \Rede\Store('082998051', '6726f0527d8d4e57b18cc3e1693c776b', \Rede\Environment::production());
       // Configuração da loja em modo sandbox
       //$store = new \Rede\Store('10004504', '769a5a598c8c4180b2dfd5b3c7e7f6f3', \Rede\Environment::sandbox());
    //    Transação que será autorizada
       $numParcela = $request->parcela;
       $valorTotal = $request->totalComFrete;
       $cardNumber = preg_replace("/[^0-9]/", "", $request->codigoCartao); // 5448280000000007
       $securityCode = $request->cvv; //235
       $expirationMonth = substr($request->vencimento, 0, 2); //12
       $expirationYear = substr($request->vencimento, 3, 7); //2020
       $cardholderName = $request->firstName;
       
       $cupomValue = Cupom::where('id', $request->cupom)->first();
       if($request->cupom !== null && $cupomValue->status == 0) {
           if(intval($cupomValue->qtd) > 0){
               Cupom::where('id', $request->cupom)->update([
                   'qtd' => intval($cupomValue->qtd) - 1
               ]);
               
               $novoCupomValue = Cupom::where('id', $request->cupom)->first();
               
               if(intval($novoCupomValue->qtd) == 0) {
                   $novoCupomValue->status = 1;
                   $novoCupomValue->save();
               }
           }
       }

       if($request->formaPagamento == 'debito') {
            $transaction = (new \Rede\Transaction($valorTotal, time()))->debitCard(
                $cardNumber,
                $securityCode,
                $expirationMonth,
                $expirationYear,
                $cardholderName
            );

            $transaction->threeDSecure(\Rede\ThreeDSecure::DECLINE_ON_FAILURE);
            $transaction->addUrl('https://redirecturl.com/3ds/success', \Rede\Url::THREE_D_SECURE_SUCCESS);
            $transaction->addUrl('https://redirecturl.com/3ds/failure', \Rede\Url::THREE_D_SECURE_FAILURE);

            $transaction = (new \Rede\eRede($store))->create($transaction);
            if ($transaction->getReturnCode() == '220') {
                $tid = $transaction->getTid();

                $end = EnderecoPedido::create(
                    [
                        'cep'       => $request->cep,
                        'rua'       => $request->rua,
                        'numero'    => $request->numero,
                        'cidade'    => $request->cidade,
                        'bairro'    => $request->bairro,
                        'complemento' => $request->complemento
                    ]
                );

                $pedido = Pedido::create(
                    [
                        'user_id'               => $request->user_id,
                        'endereco_pedido_id'    => $end->id,
                        'tid'                   => $tid,
                        'parcela'               => $numParcela,
                        'cliente_id'            => $request->user_id,
                        'nome'                  => $request->nome,
                        'valor_total'           => $request->totalComFrete,
                        'amostra'               => $request->amostra,
                        'frete'                 => str_replace(',', '.', $request->frete),
                        'tipo_frete'            => $request->tipo_frete,
                        'id_cupom'              => $request->cupom,
                        'status_pagamento'      => 'Confirmado',
                        'pedido_status_id'      => 1,
                        'produto_tecido_id'     => $request->produto_tecido_id
                    ]
                );

                foreach ($request->itensCarrinho as $value) {
                    ConfeccaosPagamentos::create([
                        'confeccaos_id' => $value['id'],
                        'pedido_id' => $pedido->id,
                    ]);

                    Confeccao::where('id', $value['id'])->update(['finalizou_compra' => 1]);
                }
            }
            //return [$transaction, $pedido->id, $request->itensCarrinho];
            return [$transaction, $pedido->id];

        } elseif($request->formaPagamento == 'credito') {
            $transaction = (new \Rede\Transaction($valorTotal, time()))->creditCard(
                $cardNumber,
                $securityCode,
                $expirationMonth,
                $expirationYear,
                $cardholderName
            );// senão quiser capturar o pagamento ->capture(false);
            // Autoriza a transação
            
            // Configuração de parcelamento
            $transaction->setInstallments($numParcela);

            $transaction = (new \Rede\eRede($store))->create($transaction);
            if ($transaction->getReturnCode() == '00') {
                $tid = $transaction->getTid();
            
                $end = EnderecoPedido::create(
                    [
                        'cep'       => $request->cep,
                        'rua'       => $request->rua,
                        'numero'    => $request->numero,
                        'cidade'    => $request->cidade,
                        'bairro'    => $request->bairro,
                        'complemento' => $request->complemento
                    ]
                );

                $pedido = Pedido::create(
                    [
                        'user_id'               => $request->user_id,
                        'endereco_pedido_id'    => $end->id,
                        'tid'                   => $tid,
                        'parcela'               => $numParcela,
                        'cliente_id'            => $request->user_id,
                        'nome'                  => $request->nome,
                        'valor_total'           => $request->totalComFrete,
                        'amostra'               => $request->amostra,
                        'id_cupom'              => isset($request->cupom) ? $request->cupom : null,
                        'tipo_frete'            => $request->tipo_frete,
                        'frete'                 => number_format((float)$request->frete, 2, '.', ''),
                        'status_pagamento'      => 'Confirmado',
                        'pedido_status_id'      => 1,
                        'produto_tecido_id'     => $request->produto_tecido_id
                    ]
                );

                foreach ($request->itensCarrinho as $value) {
                    ConfeccaosPagamentos::create([
                        'confeccaos_id' => $value['id'],
                        'pedido_id' => $pedido->id,
                    ]);

                    Confeccao::where('id', $value['id'])->update(['finalizou_compra' => 1]);
                }
            }
            //return [$transaction, $pedido->id, $request->itensCarrinho];
            return [$transaction, $pedido->id];
        }
   }

    //método de parcelamento
    public function parcelamento(Request $request)
    {
        $numParcela = $request->parcela;
        $valorTotal = $request->valor_total;
        $cardNumber = $request->cardnumber;
        $securityCode = $request->security;
        $expirationMonth = $request->mesexpiracao;
        $expirationYear = $request->anoexpiracao;
        $cardholderName = $request->nome;
        $reference = $request->pedido;
        // Configuração da loja em modo produção
        //$store = new \Rede\Store('PV', 'TOKEN', \Rede\Environment::production());
        
        // Configuração da loja em modo sandbox
        //$store = new \Rede\Store('10004504', '769a5a598c8c4180b2dfd5b3c7e7f6f3', \Rede\Environment::sandbox());
        $store = new \Rede\Store('082998051', '6726f0527d8d4e57b18cc3e1693c776b', \Rede\Environment::production());
        
        // Transação que será autorizada
        $transaction = (new \Rede\Transaction($valorTotal, $reference . time()))->debitCard(
            $cardNumber,
            $securityCode,
            $expirationMonth,
            $expirationYear,
            $cardholderName
        );
        
        // Configuração de parcelamento
        //$transaction->setInstallments($numParcela);
        
        // Autoriza a transação
        $transaction = (new \Rede\eRede($store))->create($transaction);
        
        if ($transaction->getReturnCode() == '00') {
            $tid = $transaction->getTid();
            Pagamento::insert(['tid' => $tid]);
            return back()->with('success', "Transação autorizada com sucesso; tid=%s\n", $transaction->getTid());
        }
    }	

    //confirma tranzacao para debitar de quem pagou
    public function capturaTranzacao()
    {
        $tid = '10021910160858070237';
        // Configuração da loja em modo produção
        //$store = new \Rede\Store('PV', 'TOKEN', \Rede\Environment::production());
        
        // Configuração da loja em modo sandbox
        //$store = new \Rede\Store('10004504', '769a5a598c8c4180b2dfd5b3c7e7f6f3', \Rede\Environment::sandbox());
        $store = new \Rede\Store('082998051', '6726f0527d8d4e57b18cc3e1693c776b', \Rede\Environment::production());
        
        $transaction = (new \Rede\eRede($store))->get($tid);
        if($tid) {
            printf("O status atual da autorização é %s\n", $transaction->getAuthorization()->getStatus());
        } else{
            printf("tid não cadastrado");
        }
    }
}