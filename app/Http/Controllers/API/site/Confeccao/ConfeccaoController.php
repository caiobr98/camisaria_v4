<?php

namespace App\Http\Controllers\Api\site\Confeccao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ConfeccaoRepository;
use App\Http\Requests\Confeccao\ConfeccaoAddRequest;

class ConfeccaoController extends Controller
{
    private $confeccao;

    public function __construct(ConfeccaoRepository $confeccaoRepository)
    {
        $this->confeccao = $confeccaoRepository;
    }
    
    public function index(Request $request)
    {
        return $this->confeccao->index($request);

    }
    public function store(Request $request)
    {
        $confeccao = $this->confeccao->store($request);

        return response()->json($confeccao, 200);
    }

    public function show($id)
    {
        return $this->confeccao->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $confeccao = $this->confeccao->update($request, $id);
        $confeccao->update($request->all());

        return response()->json($confeccao, 200);
    }

    public function destroy($id)
    {
        $this->confeccao->destroy($id);

        return response()->json(null, 204);
    }

    public function autorizacao()
    {
        return $this->confeccao->autorizaTransacao();
    }

    public function carrinhoByUserId($user_id)
    {
        return $this->confeccao->carrinhoByUserId($user_id);
    }

    public function getQuantidade($id){

        return $this->confeccao->getQuantidade($id);

    }

    public function reduzirQuantidade($id){

        return $this->confeccao->reduzirQuantidade($id);
        
    }

    public function aumentarQuantidade($id){

        return $this->confeccao->aumentarQuantidade($id);
        
    }

    public function colarinhoPunhoSearch($punho_id, $colarinho_id){

        return $this->confeccao->colarinhoPunhoSearch($punho_id, $colarinho_id);

    }
}
