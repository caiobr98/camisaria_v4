<?php

namespace App\Http\Controllers\Api\site\Confeccao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ConfeccaodetalheRepository;

class ConfeccaodetalheController extends Controller
{
    private $confeccaodetalhe;

    public function __construct(ConfeccaodetalheRepository $confeccaodetalheRepository)
    {
        $this->confeccaodetalhe = $confeccaodetalheRepository;
    }

    public function index(Request $request)
    {
        return $this->confeccaodetalhe->index($request);
    }

    public function store(Request $request)
    {
        $detalhe = $this->confeccaodetalhe->store($request);

        return response()->json($detalhe, 201);
    }

    public function show($id)
    {
        return $this->confeccaodetalhe->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $detalhe = $this->confeccaodetalhe->update($request, $id);
        $detalhe->update($request->all());

        return response()->json($detalhe, 200);
    }

    public function destroy($id)
    {
        $this->confeccaodetalhe->destroy($id);

        return response()->json(null, 204);
    }
}
