<?php

namespace App\Http\Controllers\Api\site\Confeccao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ConfeccaomedidaRepository;

class ConfeccaomedidaController extends Controller
{
    private $confeccaomedidas;

    public function __construct(ConfeccaomedidaRepository $confeccaomedidaRepository)
    {
        $this->confeccaomedidas = $confeccaomedidaRepository;
    }

    public function index(Request $request)
    {
        return $this->confeccaomedidas->index($request);
    }

    public function store(Request $request)
    {
        return $request;
        $medida = $this->confeccaomedidas->store($request);

        return response()->json($medida, 201);
    }

    public function show($id)
    {
        return $this->confeccaomedidas->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $medida = $this->confeccaomedidas->update($request, $id);
        $medida->update($request->all());

        return response()->json($medida, 200);
    }

    public function destroy($id)
    {
        $this->confeccaomedidas->destroy($id);

        return response()->json(null, 204);
    }
}
