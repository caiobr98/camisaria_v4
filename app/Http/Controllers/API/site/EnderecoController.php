<?php

namespace App\Http\Controllers\Api\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\EnderecoRepository;

class EnderecoController extends Controller
{
    private $endereco;

    public function __construct(EnderecoRepository $enderecoRepository)
    {
        $this->endereco = $enderecoRepository;
    }
    
    public function index(Request $request)
    {
        return $this->endereco->index($request);

    }
    public function store(Request $request)
    {
        $this->endereco->store($request);
    }

    public function show($id)
    {
        return $this->endereco->show($id);
    }

    public function update(Request $request, $id)
    {
        $this->endereco->update($request, $id);
    }

    public function destroy($id)
    {
        $this->endereco->destroy($id);
    }
    
    public function cepCorreios($cep)
    {
        return $this->endereco->cepCorreios($cep);
    }
    
    public function showByUserId($user_id)
    {
        return $this->endereco->showByUserId($user_id);
    }
}
