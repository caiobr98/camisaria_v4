<?php

namespace App\Http\Controllers\Api\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\site\ContatoRepository;
use Auth;
use Log;

class ContatoController extends Controller
{
    private $contato;

    public function __construct(ContatoRepository $contatoRepository)
    {
        $this->contato = $contatoRepository;
    }
    
    public function index(Request $request)
    {
        return $this->contato->index($request);

    }
    public function store(Request $request)
    {
        $contato = $this->contato->store($request);

        return response()->json($contato, 201);
        // Log::debug(auth('api')->user());
    }

    public function show($id)
    {
        return $this->contato->show($id);
    }

    public function update(Request $request, $id)
    {
        
        $contato = $this->contato->update($request, $id);
        $contato->update($request->all());

        return response()->json($contato, 200);
    }

    public function destroy($id)
    {
        $this->contato->destroy($id);

        return response()->json(null, 204);
    }
}
