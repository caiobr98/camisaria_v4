<?php

namespace App\Http\Controllers\API\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use App\Repositories\admin\UserRepository;

class UserController extends Controller
{
    private $user;

    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth:api');
        $this->user = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->user->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->user->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->user->show($id);
    }

    public function updateProfile(Request $request, $id)
    {   
        return $this->user->updateProfile($request, $id);
    }

    public function profile()
    {
        return $this->user->profile();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->user->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->destroy($id);
    }
    
    public function search()
    {
        return $this->user->search();
    }

    public function updateProfileImage(Request $request, $id)
    { 
        return $this->user->updateProfileImage($request->photo, $id);
    }

    public function inactiveUsers()
    {
        return $this->user->inactiveUsers();
    }

    public function inactiveUser($id)
    {
        return $this->user->inactiveUser($id);
    }

    public function showInactiveUser($id)
    {
        return $this->user->showInactiveUser($id);
    }

    public function activeUser(Request $request, $id)
    {
        $this->user->activeUser($request, $id);
    }
}
