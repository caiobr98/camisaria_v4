<?php

namespace App\Http\Controllers\API\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\admin\cupomRepository;

class CupomApiController extends Controller
{
    private $cupom;

    public function __construct(cupomRepository $cupomRepository)
    {
        $this->middleware('auth:api');
        $this->cupom = $cupomRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function consultar(Request $request)
    {
        return $this->cupom->consultar($request);
    }

}
