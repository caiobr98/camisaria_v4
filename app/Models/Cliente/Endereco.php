<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class Endereco extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'cep',
        'uf',
        'cidade',
        'bairro',
        'complemento',
        'rua',
        'numero',
        'observacoes',
        'tipo'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
