<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-11
 * Time: 17:33
 */

namespace App\Models\Cliente;
use App\Models\BaseModel;

use Illuminate\Database\Eloquent\Model;

class ClienteCamiseiro extends BaseModel
{
    protected $fillable = [
        'user_id',
        'camiseiro_id'
    ];
}