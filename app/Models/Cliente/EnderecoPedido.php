<?php

namespace App\Models\Cliente;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnderecoPedido extends BaseModel
{
    protected $table = 'endereco_pedidos';

    use SoftDeletes;

    protected $fillable = [
        'rua',
        'cep',
        'cidade',
        'bairro',
        'complemento',
        'numero'
    ];

    public function pagamento()
    {
        return $this->hasOne('App\Models\Pagamentos\Pagamento', 'id', 'pagamento_id');
    }
}