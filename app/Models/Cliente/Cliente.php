<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 08/05/2019
 * Time: 17:01
 */

namespace App\Models\Cliente;

use App\Models\Pagamentos\Pagamento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Models\BaseModel;
class Cliente extends BaseModel
{
    use SoftDeletes, Notifiable;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function pagamento()
    {
        return $this->hasMany(Pagamento::class);
    }
}