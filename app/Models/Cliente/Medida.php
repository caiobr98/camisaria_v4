<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\BaseModel;
class Medida extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'nome',
        'colarinho',
        'torax',
        'cintura',
        'quadril',
        'comprimento_total',
        'largura_costas',
        'costas_1',
        'costas_2',
        'ombro',
        'cava',
        'comprimento_manga',
        'biceps',
        'punho_direito',
        'punho_esquerdo'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function confeccaos()
    {
        return $this->hasMany('App\Models\Confeccao');
    }
}
