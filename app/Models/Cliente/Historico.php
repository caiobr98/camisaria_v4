<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class Historico extends BaseModel
{
    use SoftDeletes;

    protected $table = 'historico_cliente';

    protected $fillable = [
        'user_id',
        'codigo_tecido',
        'fornecedor_id',
        'data_compra',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function fornecedor(){
        return $this->belongsTo('App\Models\Fornecedor\Fornecedor', 'fornecedor_id', 'id');
    }
}
