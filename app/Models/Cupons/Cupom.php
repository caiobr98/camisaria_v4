<?php

namespace App\Models\Cupons;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;
class Cupom extends BaseModel
{
    use SoftDeletes;

    protected $table = 'cupons';
}
