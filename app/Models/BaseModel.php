<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class BaseModel extends Model
{
    use LogsActivity, SoftDeletes;

    protected $guarded = [];
    
    //log mitoso
    protected static $logUnguarded = true;

    protected static $recordEvents = ['created', 'updated', 'deleted'];
}
