<?php

namespace App\Models;

class Confeccaomedida extends BaseModel
{
    protected $table = 'confeccaomedidas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
}
