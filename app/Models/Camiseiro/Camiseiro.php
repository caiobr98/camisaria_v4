<?php

namespace App\Models\Camiseiro;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;
class Camiseiro extends BaseModel
{
    use SoftDeletes;
}
