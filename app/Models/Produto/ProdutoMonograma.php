<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class ProdutoMonograma extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'nome',
        'descricao',
        'local_monograma',
        'texto_monograma'
    ];

    public function confeccaos()
    {
        return $this->hasMany('App\Models\Confeccao');
    }
}
