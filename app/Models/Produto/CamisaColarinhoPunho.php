<?php

namespace App\Models\Produto;

use App\Models\BaseModel;

class CamisaColarinhoPunho extends BaseModel
{
    protected $table = 'produto_colarinhos_punhos';

    public function produto_punho()
    {
        return $this->hasOne('App\Models\Produto\ProdutoPunho', 'id', 'produto_punhos_id');
    }

    public function produto_colarinho()
    {
        return $this->hasOne('App\Models\Produto\ProdutoColarinho', 'id', 'produto_colarinhos_id');
    }
}
