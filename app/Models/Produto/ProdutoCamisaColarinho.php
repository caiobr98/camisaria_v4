<?php

namespace App\Models\Produto;

use App\Models\BaseModel;
class ProdutoCamisaColarinho extends BaseModel
{
    protected $table = 'produto_camisa_colarinhos';

    public function confeccaos()
    {
        return $this->hasMany('App\Models\Confeccao');
    }
}