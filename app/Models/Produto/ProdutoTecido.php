<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 13/05/2019
 * Time: 14:15
 */

namespace App\Models\Produto;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutoTecido extends BaseModel
{
    protected $table = 'produto_tecidos';

    public function fio(){
        return $this->belongsTo('App\Models\Produto\ProdutoFio', 'produto_fio_id', 'id');
    }

    public function produto_cores()
    {
        return $this->belongsTo('App\Models\Produto\ProdutoCor', 'id_produto_cor');
    }
}