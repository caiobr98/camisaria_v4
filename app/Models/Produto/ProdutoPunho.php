<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 14:13
 */

namespace App\Models\Produto;
use App\Models\BaseModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutoPunho extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'nome',
        'descricao',
        'punho_foto'
    ];
}