<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class ProdutoCaracteristica extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'nome',
        'descricao',
    ];
}
