<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 10/05/2019
 * Time: 10:28
 */

namespace App\Models\Produto;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutoBaseColarinho extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'nome',
        'descricao',
    ];
}