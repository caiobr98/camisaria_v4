<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 2019-07-10
 * Time: 14:50
 */

namespace App\Models\Produto;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class ProdutoCor extends BaseModel
{
    use SoftDeletes;

    protected $table = 'produto_cores';

    protected $fillable = [
        'nome'
    ];
}