<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 13/05/2019
 * Time: 10:44
 */

namespace App\Models\Produto;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutoFio extends BaseModel
{
    use SoftDeletes;

    protected $table = 'produto_fios';
    protected $fillable = [
        'nome',
        'valor',
        'peso'
    ];

    public function setValorAttribute($value){
        $this->attributes['valor'] = str_replace(',','.',str_replace('.', '', $value));
    }

    public function getValorAttribute($valor){
        return number_format($valor, 2, ',', '.');
    }
}