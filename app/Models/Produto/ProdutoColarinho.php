<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class ProdutoColarinho extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'nome',
        'descricao',
        'colarinho_foto'
    ];

    public function confeccaos()
    {
        return $this->hasMany('App\Models\Confeccao');
    }
}
