<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class ProdutoPacote extends BaseModel
{

    protected $table = 'produto_pacotes';
    protected $fillable = [
        'comprimento',
        'largura',
        'altura'
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'update_at';
}