<?php
/**
 * Created by PhpStorm.
 * User: caio
 * Date: 2019-08-05
 * Time: 14:50
 */

namespace App\Models\Produto;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutoTextura extends BaseModel
{
    use SoftDeletes;

    protected $table = 'produto_texturas';

    protected $fillable = [
        'nome'
    ];
}