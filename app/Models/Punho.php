<?php

namespace App\Models;

class Punho extends BaseModel
{
    protected $table = 'punhos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function confeccaos()
    {
        return $this->hasMany('App\Models\Confeccao');
    }
    
}
