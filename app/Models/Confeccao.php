<?php

namespace App\Models;
class Confeccao extends BaseModel
{
    protected $table = 'confeccaos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function produto_tecidos()
    {
        return $this->belongsTo('App\Models\Produto\ProdutoTecido');
    }
    // public function medidas()
    // {
    //     return $this->belongsTo('App\Models\Cliente\Medida', 'medida_id'); esse metodo nao é mais usado pois a tabela de medidas foi inutilizada, passando agora a utilizar confeccaomedidas
    // }
    public function medidas() // confeccao medidas
    {
        return $this->belongsTo('App\Models\Confeccaomedida', 'medida_id');
    }
    public function produto_colarinhos()
    {
        return $this->belongsTo('App\Models\Produto\ProdutoColarinho');
    }
    public function produto_punhos()
    {
        return $this->belongsTo('App\Models\Produto\ProdutoPunho', 'id');
    }
    public function produto_monogramas()
    {
        return $this->belongsTo('App\Models\Produto\ProdutoMonograma');
    }
    
    public function confeccaomedidas()
    {
        return $this->hasOne('App\Models\Confeccaomedida', 'id', 'medida_id');
    }

    public function confeccaodetalhes()
    {
        return $this->hasOne('App\Models\Confeccaodetalhe', 'id', 'confeccao_detalhe_id');
    }

    public function tecido()
    {
        return $this->hasOne('App\Models\Produto\ProdutoTecido', 'id', 'produto_tecido_id');
    }

    public function fio()
    {
        return $this->hasOne('App\Models\Produto\ProdutoFio', 'id', 'produto_fio_id');
    }

    public function produtoColarinhosPunhos()
    {
        return $this->hasOne('App\Models\Produto\ProdutoColarinhosPunhos', 'id', 'produto_colarinhos_punhos_id');
    }
}
