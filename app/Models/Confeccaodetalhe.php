<?php

namespace App\Models;

class Confeccaodetalhe extends BaseModel
{
    protected $table = 'confeccao_detalhes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
}
