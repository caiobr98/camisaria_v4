<?php

namespace App\Models\Fornecedor;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedor extends BaseModel
{
    use SoftDeletes;

    protected $table = 'fornecedores';

    protected $fillable = [
        'nome',
        'cpf',
        'rg',
        'email',
        'telefone',
        'celular'
    ];
}
