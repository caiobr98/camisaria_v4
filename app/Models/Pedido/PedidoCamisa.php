<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 22/05/2019
 * Time: 17:45
 */

namespace App\Models\Pedido;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoCamisa extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'produto_tecido_id',
        'produto_punho_id',
        'produto_pense_id',
        'produto_monograma_id',
        'produto_fio_id',
        'produto_colarinho_id',
        'produto_caracteristica_id',
        'produto_bolso_id',
        'produto_base_colarinho_id',
        'pedido_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function caracteristica(){
        return $this->hasOne('App\Models\Produto\ProdutoCaracteristica', 'id', 'produto_caracteristica_id');
    }

    public function monograma(){
        return $this->hasOne('App\Models\Produto\ProdutoMonograma', 'id', 'produto_monograma_id');
    }

    public function baseColarinho(){
        return $this->hasOne('App\Models\Produto\ProdutoBaseColarinho', 'id', 'produto_base_colarinho_id');
    }

    public function bolso(){
        return $this->hasOne('App\Models\Produto\ProdutoBolso', 'id', 'produto_bolso_id');
    }

    public function pense(){
        return $this->hasOne('App\Models\Produto\ProdutoPense', 'id', 'produto_pense_id');
    }

    public function punho(){
        return $this->hasOne('App\Models\Produto\ProdutoPunho', 'id', 'produto_punho_id');
    }

    public function colarinho(){
        return $this->hasOne('App\Models\Produto\ProdutoColarinho', 'id', 'produto_colarinho_id');
    }

    public function tecido(){
        return $this->hasOne('App\Models\Produto\ProdutoTecido', 'id', 'produto_tecido_id');
    }
}