<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 23/05/2019
 * Time: 17:38
 */

namespace App\Models\Pedido;


use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
class PedidoStatus extends BaseModel
{

    const PENDENTE = 1;
    const CONFIRMADO = 2;

    protected $fillable = [
        'id',
        'nome',
    ];
}