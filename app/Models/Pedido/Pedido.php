<?php
/**
 * Created by PhpStorm.
 * User: maxwillian
 * Date: 15/05/2019
 * Time: 17:33
 */

namespace App\Models\Pedido;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'endereco_pedido_id',
        'nome',
        'tid',             
        'parcela',               
        'valor_total',     
        'status_pagamento',
        'amostra',
        'frete',
        'tipo_frete',
        'id_cupom',
        'pedido_status_id',
        'produto_tecido_id',
        'camiseiro_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function endereco(){
        return $this->hasOne('App\Models\Cliente\EnderecoPedido', 'id', 'endereco_pedido_id');
    }

    public function camiseiro(){
        return $this->hasOne('App\Models\Camiseiro\Camiseiro');
    }

    public function camisas(){
        return $this->hasMany('App\Models\Pedido\PedidoCamisa');
    }

    public function status(){
        return $this->hasOne('App\Models\Pedido\PedidoStatus', 'id', 'pedido_status_id');
    }

    public function produto_tecido(){
        return $this->hasOne('App\Models\Produto\ProdutoTecido', 'id', 'produto_tecido_id');
    }
}