<?php

namespace App\Models;

class Minhasmedida extends BaseModel
{
    protected $table = 'minhasmedidas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function users()
    {
        return $this->hasOne('App\User');
    }
    
}
