<?php

namespace App\Models;

class Contato extends BaseModel
{
    protected $table = 'contatos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
}
