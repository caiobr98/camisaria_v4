<?php

namespace App\Observers;

use App\Mail\PedidoConfirmado;
use App\Models\Pedido\Pedido;
use App\Models\Pedido\PedidoStatus;
use Illuminate\Support\Facades\Mail;
use Log;
Use App\User;
use App\Models\ConfeccaosPagamentos;
class PedidoObserver
{
    /**
     * Handle the pedido "created" event.
     *
     * @param  \App\Models\Pedido\Pedido  $pedido
     * @return void
     */
    public function created(Pedido $pedido)
    {
        //
    }

    /**
     * Handle the pedido "updated" event.
     *
     * @param  \App\Models\Pedido\Pedido  $pedido
     * @return void
     */
    public function updated(Pedido $pedido)
    {
        $user = User::where('id', $pedido->user_id)->get();
        
        if($pedido->pedido_status_id == PedidoStatus::CONFIRMADO){
            $array = [];
            Mail::send('emails.pedido_confirmado', ['pedido' => $pedido],  function($message) use ($user) {
                $message->to($user[0]->email);
                $message->subject('Confirmação de pedido');
            });
            // Mail::to($pedido->user)->send(new PedidoConfirmado($pedido));
        }
    }

    /**
     * Handle the pedido "deleted" event.
     *
     * @param  \App\Models\Pedido\Pedido  $pedido
     * @return void
     */
    public function deleted(Pedido $pedido)
    {
        //
    }

    /**
     * Handle the pedido "restored" event.
     *
     * @param  \App\Models\Pedido\Pedido  $pedido
     * @return void
     */
    public function restored(Pedido $pedido)
    {
        //
    }

    /**
     * Handle the pedido "force deleted" event.
     *
     * @param  \App\Models\Pedido\Pedido  $pedido
     * @return void
     */
    public function forceDeleted(Pedido $pedido)
    {
        //
    }
}
