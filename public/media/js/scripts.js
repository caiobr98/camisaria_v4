$(document).ready(function() {

  AOS.init({
    disable: 'mobile',
    offset: 0,
    duration: 500,
    easing: 'linear',
    delay: 0
  });

  $('.carousel').carousel();

  $('.collapse').collapse();
  $('#pergunta-01').addClass('collapse show');
  $('.collapse').on('hidden.bs.collapse', toggleIcon);
  $('.collapse').on('shown.bs.collapse', toggleIcon);

  $('.navbar-toggle').click(function(){
    if($('.navbar-toggle i').hasClass('fa-bars')) {
      $('.navbar-toggle i').removeClass('fa-bars').addClass('fa-close');
    } else {
      $('.navbar-toggle i').removeClass('fa-close').addClass('fa-bars');
    }
    $('.navbar-collapse').toggleClass('menu-mobile');
    $('.navbar-toggle').toggleClass('indexcity');
  });


	$(window).scroll(function() {
		if ($(this).scrollTop() > 590) {
			$('header').addClass('encolhe');
		} else {
		  $('header').removeClass('encolhe');
		}
  });
  

  /* Carrossel */
  var owlSobre = $('.owl-sobre');
  owlSobre.owlCarousel({
    items:1,
    loop:true,
    margin:0,
    autoplay:true,
    nav:false,
    dots:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        680:{
            items:1
        },
        800:{
            items:1
        },
        991:{
            items:1
        },
        1024:{
            items:1
        },
        1920:{
            items:1
        }
    }
  });

  var owl = $('.owl-trio');
  owl.owlCarousel({
    items:3,
    loop:true,
    margin:10,
    autoplay:true,
    nav:false,
    dots:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        680:{
            items:1
        },
        800:{
            items:2
        },
        991:{
            items:2
        },
        1024:{
            items:3
        },
        1920:{
            items:3
        }
    }
  });

  /* Dismiss */
  $('.alert').alert();

  var trigger = $('.hamburger'),
  overlay = $('.overlay'),
  isClosed = false;

  trigger.click(function () {
    hamburger_cross();
  });

  function hamburger_cross() {

    if (isClosed == true) {
      overlay.hide();
      trigger.removeClass('is-open');
      trigger.addClass('is-closed');
      isClosed = false;
    } else {
      overlay.show();
      trigger.removeClass('is-closed');
      trigger.addClass('is-open');
      isClosed = true;
    }
  }

  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
        $('html').toggleClass('overflow');
  });


  $('#sidebar-wrapper').find('li').click(function(e) {  
    e.PreventDefault();
    $('.hamburger').click();
  });

  $('[data-toggle="tooltip"]').tooltip();

  var isMobile = false; //initiate as false
  // device detection
  if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
      isMobile = true;
      $('#localizacao').append('<iframe src="https://embed.waze.com/pt-BR/iframe?zoom=17&lat=-23.51787&lon=-46.17686&pin=1&desc=1" id="mapa"></iframe>');
  } else {
      $('#localizacao').append('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.4068082149465!2d-46.179044084454404!3d-23.51786646587388!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cdd83c3aef74a1%3A0xb498e67bede2ad4f!2sSuperteia+-+Desenvolvimento+e+Solu%C3%A7%C3%B5es+para+Internet!5e0!3m2!1spt-BR!2sbr!4v1529595867817" id="mapa" frameborder="0" allowfullscreen></iframe>');
  }

  $('.ancora').off();
  $('.ancora').click(function() {
    if(isClosed == true) { $('.hamburger').click(); }
    $('html, body').animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 1000);
    return false;
  });

  $( "#btnCalcularFrete" ).click(function(event) {
    event.preventDefault();
    $("html, body").animate({ scrollTop: $($("#priceBlock")).offset().top-200 }, 1000);
  });

  $( "#add_cupom" ).click(function(event) {
    event.preventDefault();
    $("html, body").animate({ scrollTop: $($("#priceBlock")).offset().top-160 }, 1000);
  });

  $( '#btnFinalizar' ).click(function(event) {
    event.preventDefault();
    $("html, body").animate({ scrollTop: $($("#carrinho")).offset().top }, 1000);

  });

  $('#frmCadastro').validator();
  $("#frmContato").submit(function(e){
    e.preventDefault();
    var formData = new FormData(this);

    $("#btnEnviarContato").attr('disabled',true);

    $.ajax({
        url: '../enviar_contato.php',
        type: 'POST',
        data: formData,
        dataType: 'JSON',
        cache: false,
        contentType: false,
        processData: false,
        success: function(result){
        if(result.msg == 1){
			  $(".form-control").val('');
          $("#msgSucessoContato").fadeIn(500).focus().fadeOut(10000);
        }else{
          $("#msgErroContato").fadeIn(500).focus().fadeOut(10000);
        }
          $("#btnEnviarContato").attr('disabled',false);
        }
    });
  });


  /* Parallax */
  $window = $(window);
  $('.parallax').each(function(){
      var $scroll = $(this);
      $(window).scroll(function() {
          var yPos = -($window.scrollTop() / $scroll.data('speed'))*2;
          var coords = '50%' + yPos + 'px';
          $scroll.css({ backgroundPosition: coords });
      });
  });


  /* Count */
  $('.num').each(function () {
    $(this).counterUp({
      delay: 2,
      time: 30
    });
  });


  /* Tabs */
  function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
  }
  function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
  }

  //Initialize tooltips
   $('.nav-tabs > li a[title]').tooltip();

  $(".next-step").click(function (e) {
    var $active = $('.wizard .nav-tabs li.active');
    $active.removeClass('active');
    $active.next().removeClass('disabled').addClass('active');
    nextTab($active);
  });

  $(".prev-step").click(function (e) {
    var $active = $('.wizard .nav-tabs li.active');
    $active.removeClass('active');
    $active.prev().addClass('active');
    prevTab($active);
  });

  //Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
      var $target = $(e.target);
      if ($target.parent().hasClass('disabled')) {
          return false;
      }
  });


  $('.btn-upload').on('click', function() {
    $('.arquivo').trigger('click');
  });


  $('.arquivo').on('change', function() {
    var fileName = $(this)[0].files[0].name;
    $('#file').val(fileName);
  });


  /* Máscara */
  $('.telefone').mask('(99) 9999-9999');
  $('.telefone').focusout(function(){
      var phone, element;
      element = $(this);
      element.unmask();
      phone = element.val().replace(/\D/g, '');
      if(phone.length > 10) {
          element.mask('(99) 99999-999?9');
      } else {
          element.mask('(99) 9999-9999?9');
      }
  }).trigger('focusout');


  //$('.medida').mask('99,99');
  $('#medida-colarinho').mask('99,99');


  //Galeria
  $('#ri-grid').gridrotator({
    rows: 1,
    columns: 5,
    w1024: {
    	columns: 4,
      rows: 1
    },
    w768: {
    	columns: 3,
      rows: 1
    },
    w480: {
    	columns: 2,
      rows: 1
    },
    w320: {
    	columns: 2,
      rows: 1
    },
    step: 'random',
    maxStep: 1,
    preventClick	: true,
    animType: 'random',
    animSpeed: 1000,
    animEasingOut	: 'linear',
    animEasingIn	: 'linear',
    interval		: 6000,
    slideshow		: true,
    onhover		: true,
    nochange		: []
  });

    /* Confecção */
      var $container = $('.confeccaoContainer');
      $container.isotope({
          filter: '*',
          animationOptions: {
              duration: 750,
              easing: 'linear',
              queue: false
          }
      });

      $('.confeccaoFilter a').click(function(){
          $('.confeccaoFilter .current').removeClass('current');
          $(this).parent('li').addClass('current');

          var selector = $(this).attr('data-filter');
          $container.isotope({
              filter: selector,
              animationOptions: {
                  duration: 750,
                  easing: 'linear',
                  queue: false
              }
           });
           return false;

      });

    $('.drop-nav a').click(function() {
      $('.sub-nav').removeClass('show');
      $('.sub-nav.'+ $(this).data('submenu')).addClass('show');
    });

    $('.form-control').focus(function() {
      $(this).parent().find('label').addClass('focus');
    });

    $('.form-control').blur(function() {
      if($(this).lenght < 0) {
        $(this).parent().find('label').removeClass('focus');
      }
    });

    var logo = $('.logo').clone();
    $(window).resize(function() {
      if( $(window).width() < 991 ) {
        $('.navbar').attr('id', 'sidebar-wrapper');
        $('.navbar-toggle').show();
        $('.navbar-wrapper').prepend(logo);
      }
    });
    $(document).resize(function() {
      if( $(window).width() < 991 ) {
        $('.navbar').attr('id', 'sidebar-wrapper');
        $('.navbar-toggle').show();
        $('.navbar-wrapper').prepend(logo);
      }
    });

    if( $(window).width() < 991 ) {
      $('.navbar').attr('id', 'sidebar-wrapper');
      $('.navbar-toggle').show();
      $('.navbar-wrapper').prepend(logo);
      $('.dropdown').find('a').click(function() {
        $(this).parents('li').toggleClass('show');
      });
    }

});

  function toggleIcon(e) {
    $(e.target).prev('.card-header').find(".fa").toggleClass('fa-plus fa-minus');
  }

  //Camiseta fixa
  /* removido por solicitação 
  var contScrol = 0;
  $(window).scroll(function() {

    if ($(this).scrollTop() > 290 && $(this).scrollTop() < 840 && $(document).height() > 1100) { 
      
      if(contScrol == 0){
        $('#confeccao').fadeOut('slow');
        $('#infoConfig').fadeOut('slow');
        $('#confeccao').css({
          'position': 'fixed', 
          'margin-top': '0px',
          'top': '150px',
          'left': '330px'
        });

        $('#infoConfig').css({
          'position': 'fixed', 
          'top': '550px',
          'right': '0',
          'left': '-34%'
        });
        $('#confeccao').fadeIn('slow');
        $('#infoConfig').fadeIn('slow');
        contScrol = 1;
      }
  
    } else if($(this).scrollTop() >= 840){
      if(contScrol == 1){
        $('#confeccao').fadeOut('slow');
        $('#infoConfig').fadeOut('slow');
        
        $('#confeccao').css({
          'position': 'static',
          'top': '0',
          'margin-top': '540px'
        });
        $('#infoConfig').css({
          'position': 'static',
          'top': '0',
          'left': '0'
        });

        $('#confeccao').fadeIn('slow');
        $('#infoConfig').fadeIn('slow');
        contScrol = 0;
      }
    } else {
      $('#confeccao').css({ 'position': 'static',
      'margin-top': '0px', });
      $('#infoConfig').css({
        'position': 'static'
      });
      contScrol = 0;
    }
  });

  */