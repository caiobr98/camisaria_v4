$(document).ready(function(){
    $('.cpf').mask('000.000.000-00', {reverse: true});

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.telefone').mask(SPMaskBehavior, spOptions);

    $('.cep').mask('00000-000');

    $('[data-toggle="tooltip"]').tooltip();

    $('.money').mask('000.000.000.000.000,00', {reverse: true});
    $('#cep').mask('00000000', {reverse: true});

    $('.datapicker').datepicker({
        format: 'dd/mm/yyyy',                
        language: 'pt-BR',
    });

    $('.money').parents('form').on('submit', function(){
        $('.money').mask('000000000000000,00', {reverse: true});
    });
});