<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// admin
    Route::apiResources(['user' => 'API\admin\UserController']);
    
    Route::group(['prefix' => 'users'], function () {
        Route::get('inactive', 'API\admin\UserController@inactiveUsers');
        Route::get('inactive/{id}', 'API\admin\UserController@showInactiveUser');
        Route::put('active/{id}', 'API\admin\UserController@activeUser');
        Route::put('inactive/{id}', 'API\admin\UserController@inactiveUser');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'API\admin\UserController@profile');
        Route::put('/{id}', 'API\admin\UserController@updateProfile');
        Route::post('updateProfileImage/{id}', 'API\admin\UserController@updateProfileImage');
    });

    Route::get('findUser', 'API\admin\UserController@search');

    // site 
    Route::group(['prefix' => 'site'], function () {
        // pagina index
        Route::get('welcome', 'API\site\WelcomeController@areaDestaque');
    });
    
    
Route::apiResources(['minhasmedida' => 'Api\site\MinhasmedidaController']);

Route::apiResources(['tecido' => 'Api\site\Produto\ProdutoTecidoController']);
Route::get('tecido/fio/{id}', 'Api\site\Produto\ProdutoTecidoController@indexWithFio');
Route::get('tecido-lista', 'Api\site\Produto\ProdutoTecidoController@listaTecidos');
Route::post('tecidocortexturapadrao', 'API\site\Produto\ProdutoTecidoController@produtoTecidoCorTexturaPadrao');
Route::apiResources(['colarinho' => 'Api\site\Produto\ProdutoColarinhoController']);
Route::get('colarinho-lista', 'Api\site\Produto\ProdutoColarinhoController@listaColarinhos');
Route::apiResources(['punho' => 'Api\site\Produto\ProdutoPunhoController']);
Route::get('punho-lista', 'Api\site\Produto\ProdutoPunhoController@listaPunhos');

Route::apiResources(['fio' => 'API\site\Produto\ProdutoFioController']);

Route::apiResources(['contato' => 'Api\site\ContatoController']);

Route::apiResources(['confeccao' => 'Api\site\Confeccao\ConfeccaoController']);
Route::get('confeccao/carrinhobyuserid/{user_id}', 'Api\site\Confeccao\ConfeccaoController@carrinhoByUserId');
Route::get('colarinho/punho/search/{punho_id}/{colarinho_id}', 'Api\site\Confeccao\ConfeccaoController@colarinhoPunhoSearch');

Route::apiResources(['pagamento' => 'Api\site\Confeccao\ConfeccaoController']);
Route::post('/pagamentos', 'API\site\PagamentoRedeController@autorizaTransacao');
Route::post('/pagamentos/debito', 'API\site\PagamentoRedeController@autorizaTransacaoDebito');

Route::apiResources(['confeccaomedida' => 'Api\site\Confeccao\ConfeccaomedidaController']);
Route::apiResources(['confeccaodetalhe' => 'Api\site\Confeccao\ConfeccaodetalheController']);

Route::apiResources(['endereco' => 'Api\site\EnderecoController']);
Route::get('endereco/cepcorreios/{cep}', 'API\site\EnderecoController@cepCorreios');
Route::get('enderecobyuserid/{user_id}', 'API\site\EnderecoController@showByUserId');

Route::get('pagamento/cepcorreios/{cep}', 'API\site\EnderecoController@cepCorreios');

Route::get('produtoscolarinhospunhos', 'API\site\Produto\ProdutoColarinhosPunhosController@index');
Route::get('produtoscolarinhospunhos/{produto_punhos_id}/{produto_colarinhos_id}', 'API\site\Produto\ProdutoColarinhosPunhosController@colarinhoPunhoFoto');

Route::get('pedidosbyuserid/{id}', 'API\site\Pedidos\PedidosController@pedidoByUserId');
Route::get('pedidobyid/{userid}/{id}', 'API\site\Pedidos\PedidosController@pedidobyid');

Route::get('medidas/pacote', 'API\site\Pedidos\PedidosController@showMedidas');
Route::post('calcFrete', 'API\site\Pedidos\PedidosController@calcFrete');

Route::get('quantidadeCamisa/{id}', 'Api\site\Confeccao\ConfeccaoController@getQuantidade');
Route::get('quantidadeCamisa/reduzir/{id}', 'Api\site\Confeccao\ConfeccaoController@reduzirQuantidade');
Route::get('quantidadeCamisa/aumentar/{id}', 'Api\site\Confeccao\ConfeccaoController@aumentarQuantidade');
Route::post('cupom/consulta', 'API\admin\CupomApiController@consultar');
