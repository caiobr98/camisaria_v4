<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Broadcast::routes();


Route::get('/', function() {
    return view('home');
})->name('home-public');

Route::get('/home', function() {
    return view('home');
})->name('home-public');

/*Route::get('/', function() {
    return view('welcome');
});*/

Route::get('/deslogar', function () {
    return view('deslogar');
});


Auth::routes(['verify'=> true]);

//Route::get('/admin', 'Painel\Dashboard\DashboardController@index')->name('home');
// Route::get('/admin', function () {
//     return view('admin');
// }); deixa guardado pra depois

//facebook login
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::group(['prefix' => 'admin/painel', 'middleware' => 'verificausertype'], function(){
    Route::get('/', 'Painel\Dashboard\DashboardController@index')->name('home');
    Route::get('/dashboard', 'Painel\Dashboard\DashboardController@index')->name('dashboard');

    Route::get('/configuracoes-log', 'Painel\Configuracoes\LogActivityController@index')->name('lista-index');
    Route::get('/configuracoes-show/{id}', 'Painel\Configuracoes\LogActivityController@show')->name('show-log');
    Route::get('/configuracoes-edit/{id}', 'Painel\Configuracoes\LogActivityController@edit')->name('edit-log');

    Route::get('/clientes', 'Painel\Clientes\ClientesController@listagem')->name('clientes-listar');
    Route::get('/clientes/cadastrar', 'Painel\Clientes\ClientesController@formCadastro')->name('clientes-cadastrar');
    Route::post('/clientes/cadastrar', 'Painel\Clientes\ClientesController@cadastrar')->name('clientes-cadastrar');
    Route::get('/clientes/editar/{user}', 'Painel\Clientes\ClientesController@formEditar')->name('clientes-editar-form');
    Route::post('/clientes/editar', 'Painel\Clientes\ClientesController@editar')->name('clientes-editar');
    Route::get('/clientes/detalhar/{user}', 'Painel\Clientes\ClientesController@detalhar')->name('clientes-detalhar');
    Route::get('/clientes/deletar/{user}', 'Painel\Clientes\ClientesController@deletar')->name('clientes-deletar');

    Route::get('/clientes/{user_id}/medidas',                       'Painel\Clientes\MedidasController@listagem')->name('medidas-listar');
    Route::get('/clientes/{user_id}/medidas/cadastrar',             'Painel\Clientes\MedidasController@formCadastro')->name('medidas-cadastrar');
    Route::post('/clientes/{user_id}/medidas/cadastrar/{processoCamiseiroEnderecoMedidas?}', 'Painel\Clientes\MedidasController@cadastrar')->name('medidas-cadastrar');
    Route::get('/clientes/{user_id}/medidas/editar/{medida_id}',    'Painel\Clientes\MedidasController@formEditar')->name('medidas-editar');
    Route::post('/clientes/{user_id}/medidas/editar/{medida_id}',   'Painel\Clientes\MedidasController@editar')->name('medidas-editar');
    Route::get('/clientes/{user_id}/medidas/detalhar/{medida_id}',  'Painel\Clientes\MedidasController@detalhar')->name('medidas-detalhar');
    Route::get('/clientes/{user_id}/medidas/deletar/{medida_id}',   'Painel\Clientes\MedidasController@deletar')->name('medidas-deletar');

    Route::get('/clientes/historico/{user}', 'Painel\Clientes\HistoricoController@listagem')->name('historico-listar');
    Route::get('/clientes/historico/{user}/cadastrar', 'Painel\Clientes\HistoricoController@formCadastrar')->name('clientes-historico-form-cadastrar');
    Route::post('/clientes/{user_id}/historico/cadastrar/{processoCamiseiroEnderecoMedidas?}', 'Painel\Clientes\HistoricoController@cadastrar')->name('historico-cadastrar');
    Route::get('/clientes/historico/{user}/editar/{historico}', 'Painel\Clientes\HistoricoController@formEditar')->name('clientes-historico-form-editar');
    Route::post('/clientes/historico/editar', 'Painel\Clientes\HistoricoController@editar')->name('clientes-historico-editar');
    Route::get('/clientes/historico/deletar/{historico}', 'Painel\Clientes\HistoricoController@deletar')->name('clientes-historico-deletar');
    
    Route::get('/clientes/endereco/{user}', 'Painel\Clientes\EnderecosController@listar')->name('clientes-endereco');
    Route::get('/clientes/endereco/{user}/cadastrar', 'Painel\Clientes\EnderecosController@formCadastrar')->name('clientes-endereco-form-cadastrar');
    Route::post('/clientes/endereco/cadastrar/{processoCamiseiroEnderecoMedidas?}', 'Painel\Clientes\EnderecosController@cadastrar')->name('clientes-endereco-cadastrar');
    Route::get('/clientes/endereco/{user}/editar/{endereco}', 'Painel\Clientes\EnderecosController@formEditar')->name('clientes-endereco-form-editar');
    Route::post('/clientes/endereco/editar/', 'Painel\Clientes\EnderecosController@editar')->name('clientes-endereco-editar');
    Route::get('/clientes/endereco/{user}/detalhar/{endereco}', 'Painel\Clientes\EnderecosController@detalhar')->name('clientes-endereco-form-detalhar');
    Route::get('/clientes/endereco/deletar/{endereco}', 'Painel\Clientes\EnderecosController@deletar')->name('clientes-endereco-deletar');

    Route::get('/clientes/camiseiro/{user}', 'Painel\Clientes\CamiseirosController@editarCamiseiro')->name('clientes-camiseiro');
    Route::post('/clientes/camiseiro/{user}', 'Painel\Clientes\CamiseirosController@update')->name('clientes-camiseiro');

    Route::get('/clientes/camiseiro-endereco-medidas/{user}', 'Painel\Clientes\ClientesController@processoCamiseiroEnderecoMedidas')->name('clientes-processo-camiseiro-endereco-medidas');

    Route::get('/produtos/caracteristicas',                             'Painel\Produtos\ProdutoCaracteristicasController@listagem')->name('produto-caracteristicas-listar');
    Route::get('/produtos/caracteristicas/cadastrar',                   'Painel\Produtos\ProdutoCaracteristicasController@formCadastro')->name('produto-caracteristicas-cadastrar');
    Route::post('/produtos/caracteristicas/cadastrar',                  'Painel\Produtos\ProdutoCaracteristicasController@cadastrar')->name('produto-caracteristicas-cadastrar');
    Route::get('/produtos/caracteristicas/editar/{caracteristica}',     'Painel\Produtos\ProdutoCaracteristicasController@formEditar')->name('produto-caracteristicas-editar');
    Route::post('/produtos/caracteristicas/editar/{caracteristica}',    'Painel\Produtos\ProdutoCaracteristicasController@editar')->name('produto-caracteristicas-editar');
    Route::get('/produtos/caracteristicas/detalhar/{caracteristica}',   'Painel\Produtos\ProdutoCaracteristicasController@detalhar')->name('produto-caracteristicas-detalhar');
    Route::get('/produtos/caracteristicas/deletar/{caracteristica}',    'Painel\Produtos\ProdutoCaracteristicasController@deletar')->name('produto-caracteristicas-deletar');

    Route::get('/produtos/monogramas',                        'Painel\Produtos\ProdutoMonogramasController@listagem')->name('produto-monogramas-listar');
    Route::get('/produtos/monogramas/cadastrar',              'Painel\Produtos\ProdutoMonogramasController@formCadastro')->name('produto-monogramas-cadastrar');
    Route::post('/produtos/monogramas/cadastrar',             'Painel\Produtos\ProdutoMonogramasController@cadastrar')->name('produto-monogramas-cadastrar');
    Route::get('/produtos/monogramas/editar/{monograma}',     'Painel\Produtos\ProdutoMonogramasController@formEditar')->name('produto-monogramas-editar');
    Route::post('/produtos/monogramas/editar/{monograma}',    'Painel\Produtos\ProdutoMonogramasController@editar')->name('produto-monogramas-editar');
    Route::get('/produtos/monogramas/detalhar/{monograma}',   'Painel\Produtos\ProdutoMonogramasController@detalhar')->name('produto-monogramas-detalhar');
    Route::get('/produtos/monogramas/deletar/{monograma}',    'Painel\Produtos\ProdutoMonogramasController@deletar')->name('produto-monogramas-deletar');

    Route::get('/produtos/bolsos',                    'Painel\Produtos\ProdutoBolsosController@listagem')->name('produto-bolsos-listar');
    Route::get('/produtos/bolsos/cadastrar',          'Painel\Produtos\ProdutoBolsosController@formCadastro')->name('produto-bolsos-cadastrar');
    Route::post('/produtos/bolsos/cadastrar',         'Painel\Produtos\ProdutoBolsosController@cadastrar')->name('produto-bolsos-cadastrar');
    Route::get('/produtos/bolsos/editar/{bolso}',     'Painel\Produtos\ProdutoBolsosController@formEditar')->name('produto-bolsos-editar');
    Route::post('/produtos/bolsos/editar/{bolso}',    'Painel\Produtos\ProdutoBolsosController@editar')->name('produto-bolsos-editar');
    Route::get('/produtos/bolsos/detalhar/{bolso}',   'Painel\Produtos\ProdutoBolsosController@detalhar')->name('produto-bolsos-detalhar');
    Route::get('/produtos/bolsos/deletar/{bolso}',    'Painel\Produtos\ProdutoBolsosController@deletar')->name('produto-bolsos-deletar');

    Route::get('/produtos/base-colarinho', 'Painel\Produtos\BaseColarinhoController@listagem')->name('produto-base-colarinho-listagem');
    Route::get('/produtos/base-colarinho/cadastrar', 'Painel\Produtos\BaseColarinhoController@formCadastro')->name('produto-base-colarinho-cadastrar');
    Route::post('/produtos/base-colarinho/cadastrar', 'Painel\Produtos\BaseColarinhoController@cadastrar')->name('produto-base-colarinho-cadastrar');
    Route::get('/produtos/base-colarinho/editar/{baseColarinho}', 'Painel\Produtos\BaseColarinhoController@formEditar')->name('produto-base-colarinho-editar');
    Route::post('/produtos/base-colarinho/editar/{baseColarinho}', 'Painel\Produtos\BaseColarinhoController@editar')->name('produto-base-colarinho-editar');
    Route::get('/produtos/base-colarinho/detalhar/{baseColarinho}', 'Painel\Produtos\BaseColarinhoController@detalhar')->name('produto-base-colarinho-detalhar');
    Route::get('/produtos/base-colarinho/deletar/{baseColarinho}', 'Painel\Produtos\BaseColarinhoController@deletar')->name('produto-base-colarinho-deletar');

    Route::get('/produtos/punho', 'Painel\Produtos\PunhoController@listagem')->name('produto-punho-listagem');
    Route::get('/produtos/punho/cadastrar', 'Painel\Produtos\PunhoController@formCadastro')->name('produto-punho-cadastrar');
    Route::post('/produtos/punho/cadastrar', 'Painel\Produtos\PunhoController@cadastrar')->name('produto-punho-cadastrar');
    Route::get('/produtos/punho/editar/{punho}', 'Painel\Produtos\PunhoController@formEditar')->name('produto-punho-editar');
    Route::post('/produtos/punho/editar/{punho}', 'Painel\Produtos\PunhoController@editar')->name('produto-punho-editar');
    Route::get('/produtos/punho/detalhar/{punho}', 'Painel\Produtos\PunhoController@detalhar')->name('produto-punho-detalhar');
    Route::get('/produtos/punho/deletar/{punho}', 'Painel\Produtos\PunhoController@deletar')->name('produto-punho-deletar');

    Route::get('/produtos/penses',                    'Painel\Produtos\ProdutoPensesController@listagem')->name('produto-penses-listar');
    Route::get('/produtos/penses/cadastrar',          'Painel\Produtos\ProdutoPensesController@formCadastro')->name('produto-penses-cadastrar');
    Route::post('/produtos/penses/cadastrar',         'Painel\Produtos\ProdutoPensesController@cadastrar')->name('produto-penses-cadastrar');
    Route::get('/produtos/penses/editar/{pense}',     'Painel\Produtos\ProdutoPensesController@formEditar')->name('produto-penses-editar');
    Route::post('/produtos/penses/editar/{pense}',    'Painel\Produtos\ProdutoPensesController@editar')->name('produto-penses-editar');
    Route::get('/produtos/penses/detalhar/{pense}',   'Painel\Produtos\ProdutoPensesController@detalhar')->name('produto-penses-detalhar');
    Route::get('/produtos/penses/deletar/{pense}',    'Painel\Produtos\ProdutoPensesController@deletar')->name('produto-penses-deletar');

    Route::get('/produtos/colarinhos',                      'Painel\Produtos\ProdutoColarinhosController@listagem')->name('produto-colarinhos-listar');
    Route::get('/produtos/colarinhos/cadastrar',            'Painel\Produtos\ProdutoColarinhosController@formCadastro')->name('produto-colarinhos-cadastrar');
    Route::post('/produtos/colarinhos/cadastrar',           'Painel\Produtos\ProdutoColarinhosController@cadastrar')->name('produto-colarinhos-cadastrar');
    Route::get('/produtos/colarinhos/editar/{colarinho}',   'Painel\Produtos\ProdutoColarinhosController@formEditar')->name('produto-colarinhos-editar');
    Route::post('/produtos/colarinhos/editar/{colarinho}',  'Painel\Produtos\ProdutoColarinhosController@editar')->name('produto-colarinhos-editar');
    Route::get('/produtos/colarinhos/detalhar/{colarinho}', 'Painel\Produtos\ProdutoColarinhosController@detalhar')->name('produto-colarinhos-detalhar');
    Route::get('/produtos/colarinhos/deletar/{colarinho}',  'Painel\Produtos\ProdutoColarinhosController@deletar')->name('produto-colarinhos-deletar');


    Route::get('/pacote/medidas',                      'Painel\Produtos\ProdutoEnvioController@show')->name('produto-pacote-show');
    Route::post('/pacote/medidas/editar/{pacote}',      'Painel\Produtos\ProdutoEnvioController@editar')->name('produto-pacote-editar');

    Route::get('/produtos/fio',                      'Painel\Produtos\ProdutoFiosController@listagem')->name('produto-fios-listar');
    Route::get('/produtos/fio/cadastrar',            'Painel\Produtos\ProdutoFiosController@formCadastro')->name('produto-fios-cadastrar');
    Route::post('/produtos/fio/cadastrar',           'Painel\Produtos\ProdutoFiosController@cadastrar')->name('produto-fios-cadastrar');
    Route::get('/produtos/fio/editar/{fio}',   'Painel\Produtos\ProdutoFiosController@formEditar')->name('produto-fios-editar');
    Route::post('/produtos/fio/editar/{fio}',  'Painel\Produtos\ProdutoFiosController@editar')->name('produto-fios-editar');
    Route::get('/produtos/fio/detalhar/{fio}', 'Painel\Produtos\ProdutoFiosController@detalhar')->name('produto-fios-detalhar');
    Route::get('/produtos/fio/deletar/{fio}',  'Painel\Produtos\ProdutoFiosController@deletar')->name('produto-fios-deletar');

    Route::get('/camiseiros',                       'Painel\Camiseiros\CamiseirosController@listagem')->name('camiseiros-listar');
    Route::get('/camiseiros/cadastrar',             'Painel\Camiseiros\CamiseirosController@formCadastro')->name('camiseiros-cadastrar');
    Route::post('/camiseiros/cadastrar',            'Painel\Camiseiros\CamiseirosController@cadastrar')->name('camiseiros-cadastrar');
    Route::get('/camiseiros/editar/{camiseiro}',    'Painel\Camiseiros\CamiseirosController@formEditar')->name('camiseiros-editar');
    Route::post('/camiseiros/editar/{camiseiro}',   'Painel\Camiseiros\CamiseirosController@editar')->name('camiseiros-editar');
    Route::get('/camiseiros/detalhar/{camiseiro}',  'Painel\Camiseiros\CamiseirosController@detalhar')->name('camiseiros-detalhar');
    Route::get('/camiseiros/deletar/{camiseiro}',   'Painel\Camiseiros\CamiseirosController@deletar')->name('camiseiros-deletar');

    Route::get('/produtos/tecidos',                      'Painel\Produtos\ProdutoTecidosController@listagem')->name('produto-tecidos-listar');
    Route::get('/produtos/tecidos/cadastrar',            'Painel\Produtos\ProdutoTecidosController@formCadastro')->name('produto-tecidos-cadastrar');
    Route::post('/produtos/tecidos/cadastrar',           'Painel\Produtos\ProdutoTecidosController@cadastrar')->name('produto-tecidos-cadastrar-post');
    Route::get('/produtos/tecidos/editar/{tecido}',   'Painel\Produtos\ProdutoTecidosController@formEditar')->name('produto-tecidos-editar');
    Route::post('/produtos/tecidos/editar/{tecido}',  'Painel\Produtos\ProdutoTecidosController@editar')->name('produto-tecidos-editar');
    Route::get('/produtos/tecidos/detalhar/{tecido}', 'Painel\Produtos\ProdutoTecidosController@detalhar')->name('produto-tecidos-detalhar');
    Route::get('/produtos/tecidos/deletar/{tecido}',  'Painel\Produtos\ProdutoTecidosController@deletar')->name('produto-tecidos-deletar');

    Route::get('/produtos/cor',                      'Painel\Produtos\ProdutoCoresController@listagem')->name('produto-cor-listar');
    Route::get('/produtos/cor/cadastrar',            'Painel\Produtos\ProdutoCoresController@formCadastro')->name('produto-cor-cadastrar');
    Route::post('/produtos/cor/cadastrar',           'Painel\Produtos\ProdutoCoresController@cadastrar')->name('produto-cor-cadastrar');
    Route::get('/produtos/cor/editar/{cor}',   'Painel\Produtos\ProdutoCoresController@formEditar')->name('produto-cor-editar');
    Route::post('/produtos/cor/editar/{cor}',  'Painel\Produtos\ProdutoCoresController@editar')->name('produto-cor-editar');
    Route::get('/produtos/cor/detalhar/{cor}', 'Painel\Produtos\ProdutoCoresController@detalhar')->name('produto-cor-detalhar');
    Route::get('/produtos/cor/deletar/{cor}',  'Painel\Produtos\ProdutoCoresController@deletar')->name('produto-cor-deletar');

    Route::get('/produtos/textura',                      'Painel\Produtos\ProdutoTexturasController@listagem')->name('produto-textura-listar');
    Route::get('/produtos/textura/cadastrar',            'Painel\Produtos\ProdutoTexturasController@formCadastro')->name('produto-textura-cadastrar');
    Route::post('/produtos/textura/cadastrar',           'Painel\Produtos\ProdutoTexturasController@cadastrar')->name('produto-textura-cadastrar');
    Route::get('/produtos/textura/editar/{textura}',   'Painel\Produtos\ProdutoTexturasController@formEditar')->name('produto-textura-editar');
    Route::post('/produtos/textura/editar/{textura}',  'Painel\Produtos\ProdutoTexturasController@editar')->name('produto-textura-editar');
    Route::get('/produtos/textura/detalhar/{textura}', 'Painel\Produtos\ProdutoTexturasController@detalhar')->name('produto-textura-detalhar');
    Route::get('/produtos/textura/deletar/{textura}',  'Painel\Produtos\ProdutoTexturasController@deletar')->name('produto-textura-deletar');    

    Route::get('/produtos/classificacao',                      'Painel\Produtos\ProdutoClassificacaoController@listagem')->name('produto-classificacao-listar');
    Route::get('/produtos/classificacao/cadastrar',            'Painel\Produtos\ProdutoClassificacaoController@formCadastro')->name('produto-classificacao-cadastrar');
    Route::post('/produtos/classificacao/cadastrar',           'Painel\Produtos\ProdutoClassificacaoController@cadastrar')->name('produto-classificacao-cadastrar');
    Route::get('/produtos/classificacao/editar/{classificacao}',   'Painel\Produtos\ProdutoClassificacaoController@formEditar')->name('produto-classificacao-editar');
    Route::post('/produtos/classificacao/editar/{classificacao}',  'Painel\Produtos\ProdutoClassificacaoController@editar')->name('produto-classificacao-editar');
    Route::get('/produtos/classificacao/detalhar/{classificacao}', 'Painel\Produtos\ProdutoClassificacaoController@detalhar')->name('produto-classificacao-detalhar');
    Route::get('/produtos/classificacao/deletar/{classificacao}',  'Painel\Produtos\ProdutoClassificacaoController@deletar')->name('produto-classificacao-deletar');

    Route::resource('/camisa-colarinho',  'Painel\Produtos\CamisaColarinhoPunhoController');    

    Route::get('/fornecedores',                       'Painel\Fornecedores\FornecedoresController@listagem')->name('fornecedores-listar');
    Route::get('/fornecedores/cadastrar',             'Painel\Fornecedores\FornecedoresController@formCadastro')->name('fornecedores-cadastrar');
    Route::post('/fornecedores/cadastrar',            'Painel\Fornecedores\FornecedoresController@cadastrar')->name('fornecedores-cadastrar');
    Route::get('/fornecedores/editar/{fornecedor}',    'Painel\Fornecedores\FornecedoresController@formEditar')->name('fornecedores-editar');
    Route::post('/fornecedores/editar/{fornecedor}',   'Painel\Fornecedores\FornecedoresController@editar')->name('fornecedores-editar');
    Route::get('/fornecedores/detalhar/{fornecedor}',  'Painel\Fornecedores\FornecedoresController@detalhar')->name('fornecedores-detalhar');
    Route::get('/fornecedores/deletar/{fornecedor}',   'Painel\Fornecedores\FornecedoresController@deletar')->name('fornecedores-deletar');

    Route::get('/pedidos', 'Painel\Pedido\PedidoController@listagem')->name('pedidos-listar');
    Route::get('/pedidos/detalhar/{pedido}', 'Painel\Pedido\PedidoController@detalhar')->name('pedidos-detalhar');
    Route::get('/pedidos/detalhar/{pedido}/processar', 'Painel\Pedido\PedidoController@formProcessar')->name('pedidos-processar');
    Route::post('/pedidos/detalhar/{pedido}/processar', 'Painel\Pedido\PedidoController@processar')->name('pedidos-processar');

    Route::post('ajaxpreco', 'Painel\Pedido\PedidoController@ajaxPreco')->name('ajax-preco');

    Route::resource('usuario','Painel\Usuarios\UsuarioController');
    Route::post('usuario/senha/{usuario}', 'Painel\Usuarios\UsuarioController@editarSenha')->name('editar-senha');
    Route::get('/usuarios-deletados','Painel\Usuarios\UsuarioController@listaDeletados')->name('usuarios-deletados');
    Route::get('/usuarios-deletados/recuperar/{id}','Painel\Usuarios\UsuarioController@recuperar')->name('usuario-recuperar');

    Route::get('/clientes-deletados','Painel\Clientes\ClientesController@listaDeletados')->name('clientes-deletados');
    Route::get('/clientes-deletados/recuperar/{id}','Painel\Clientes\ClientesController@recuperar')->name('cliente-recuperar');

    // Route::get('/pagamentos', 'Painel\Pagamentos\PagamentoController@listagem')->name('pagamentos-listar');
    // Route::get('/teste', 'Painel\Pagamentos\PagamentoController@listagem1')->name('teste-listar');

    //autoriza pagamento
    // Route::get('/pagamento', 'Painel\Pagamentos\PagamentoController@autorizaTransacao')->name('pagamento');
    //captura o pagamento pelo tid
    // Route::get('/pagamento/captura', 'Painel\Pagamentos\PagamentoController@capturaTranzacao');
    //consulta por tid
    // Route::get('/pagamento/consulta/{tid}', 'Painel\Pagamentos\PagamentoController@consultaTranzacao');
    //cancela o pagamento
    Route::get('/pagamento/cancelar/{id}', 'Painel\Pedido\PedidoController@cancelaTranzacao')->name('cancelar-pagamento');

    //contato
    Route::get('/contato', 'Painel\Contatos\MensagemController@index')->name('contato-lista');
    Route::get('/contato/visualizar/{id}', 'Painel\Contatos\MensagemController@visualizar')->name('contato-visualizar');
    Route::get('/contato/marcar/{id}', 'Painel\Contatos\MensagemController@marcar')->name('contato-marcar');
    Route::get('/contato/desmarcar/{id}', 'Painel\Contatos\MensagemController@desmarcar')->name('contato-desmarcar');

    Route::get('/cupom', 'Painel\Cupons\CupomController@index')->name('cupons-listar');
    Route::get('/cupom/cadastrar', 'Painel\Cupons\CupomController@frmCadastrar')->name('cupons-frm-cadastrar');
    Route::post('/cupom/cadastrar', 'Painel\Cupons\CupomController@cadastrar')->name('cupons-cadastrar');
    Route::get('/cupom/visualizar/{id}', 'Painel\Cupons\CupomController@detalhar')->name('cupons-detalhar');
    Route::get('/cupom/editar/{id}', 'Painel\Cupons\CupomController@frmEditar')->name('cupons-frm-editar');
    Route::post('/cupom/editar/{id}', 'Painel\Cupons\CupomController@editar')->name('cupons-editar');
    Route::get('/cupom/deletar/{id}', 'Painel\Cupons\CupomController@deletar')->name('cupons-deletar');
});


Route::get('deletarmedida/{medida_id}', 'API\site\Confeccao\ConfeccaomedidaController@destroy');

Route::get('{path}', function () {
    return view('home');
 })->where('path', '(.*)');